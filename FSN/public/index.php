<?php

$root = dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR;
// $root = dirname(__FILE__).DIRECTORY_SEPARATOR;

require $root.'library/PHPExcel.php' ;
require $root.'library/Yab/Loader.php';

try {

	$loader = Yab_Loader::getInstance();

	$loader->addPath($root.'application');
	
  try {
               
                if (isset($_SERVER['WINDIR'])){
                    
                    $loader->configure($root.'config\config.local.ini');
					
                }else{
                    
                    $loader->configure($root.'config/config.ini');
                }
               
	} catch(Yab_Exception $e) {

	  var_dump($e);
	  die;
		//die('FSN est en cours de maintenance et sera r&eacute;tabli d&#039;ici quelques minutes. <br />Merci de votre compr&eacute;hension<br /><br />'.PHP_EOL.PHP_EOL.'<!-- '.$e->getMessage().' -->');

	}
        
	$loader->startMvc();


} catch(Yab_Exception $e) {

	echo $e;

}

// effacement d'une page pour afficher une autre

function div_condition_fouille_saisie()
{
	alert('in_coucou_1');
	document.getElementById('formulaire').style.display = "none";
	document.getElementById('div_condition_fouille').style.display = "block";
	document.getElementById('but_condition_fouille').style.display = "none"; 
	document.getElementById('but_retour_menu').style.display = "block"; 
	/* document.getElementById('other').style.display = "block"; */

}

function div_condition_fouille_menu()
{
	alert('in_coucou_2');
	document.getElementById('formulaire').style.display = "block";
	document.getElementById('div_condition_fouille').style.display = "none"; 
	document.getElementById('but_condition_fouille').style.display = "block";
	/* document.getElementById('but_retour_menu').style.display = "none"; */
	/* document.getElementById('other').style.display = "none"; */

}

function div_commentaire_saisie()
{
	alert('in_coucou_3');
	document.getElementById('formulaire').style.display = "none";
	document.getElementById('div_commentaire').style.display = "block";
	document.getElementById('but_commentaire').style.display = "none"; 
	document.getElementById('but_retour_menu').style.display = "block"; 
	/* document.getElementById('other').style.display = "block"; */

}

function div_commentaire_menu()
{
	alert('in_coucou_4');
	document.getElementById('formulaire').style.display = "block";
	document.getElementById('div_commentaire').style.display = "none";
	document.getElementById('but_commentaire').style.display = "block";
	document.getElementById('but_retour_menu').style.display = "none";
	/* document.getElementById('other').style.display = "none"; */
}


Object.size = function(obj) {
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
};

// PARTIE EN COMMENTAIRE DANS L'ATTENTE D'UNE FUTUR REACTIVATION SI LA GESTION DES TABLES DE LIENS ASSOCIEES EST MISE EN PLACE
//Change le contenu des listes attachées à celle dont le nom est fourni en paramètre (nomListe)
//en fonction de l'id sélectionné (ou du tableau des id sélectionnés le cas échéant)
//nomListe : nom de liste dont la valeur a changé
//tabValSel : liste des id (attribut value) sélectionnées dans la liste "nomListe"
//tabListesSel : tableau associatif qui comprendra, pour chaque liste déroulante ayant au moins un élément sélectionné, la ou les id choisis
function changeAttachedSelect(nomListe, tabValSel)
{
	/*var tabListesSel = new Object();	// Tableau ne contenant que les listes déroulantes effectivement sélectionnées
//alert("nomListe="+nomListe+" - tabValSel.length="+Object.size(tabValSel));
	if (tabValSel[nomListe] == null || tabValSel[nomListe] == "")
	{
		// On déselectionne la liste : on la supprime de la liste des listes sélectionnées
		if ( tabValSel.hasOwnProperty(nomListe) )
		{
			delete tabValSel[nomListe];
		}

		$("#div_corps_2").hide();
	}
	else
	{*/
		// Si la liste concernée est "classe" et que l'élément choisi est "P - Poterie de terre", on rend visible
		// le bloc avec les informations complémentaires
		if (nomListe == "classe")
		{
			if ($('#classe option[value=' + tabValSel[nomListe][0] + ']').text().indexOf("P") >= 0)
			{
				$("#div_corps_2").show();
			}
			else
			{
				$("#div_corps_2").hide();
			}
		}	
	/*}

	// Parcours du "tableau associatif" tabListesSel, ce qui permettra de connaître les listes déjà initialisées (où un choix a été fait)
	// et celles qu'il est nécessaire de recharger en fonction des options choisies dans 1 ou 2 autres listes
	var nomListesARecharger = "Classe#NatureMatiere#Fonction#";	// On a ici toutes les listes. Le but du code qui suit est d'effacer dans cette chaîne le nom des listes déjà sélectionnées
	for (key in tabValSel)
	{
		// key = nom de la liste sélectionnée = clé du tableau associatif tabListesSel
		if ( tabValSel.hasOwnProperty(key) )
		{
			var valSel = tabValSel[key];	// Code de la liste sélectionnée
			if( valSel != undefined && valSel != null )
			{
				listeSelect = key.charAt(0).toUpperCase() + key.substring(1);	// Mise en majuscule de la 1ère lettre du nom de la liste car dans le formulaire HTML, les noms des listes sont toutes en minuscule
				nomListesARecharger = nomListesARecharger.replace(listeSelect + "#", "");	// On 

				tabListesSel[key] = tabValSel[key].join(", ");
			}
		}
	}

	// Nb de listes sélectionnées
	nbListesSel = Object.size(tabListesSel);
	
	if (nbListesSel <= 2)
	{
		// 1 liste est sélectionnée. Il faut recharger les 2 listes restantes en tenant compte du filtre de celle qui est sélectionnée
		// (ex : Classe est sélectionnée, NatureMatiere et Fonctions rechargent les choix qui sont en relation avec la Classe)
		// 2 listes sont sélectionnées. Il faut recharger la liste restante en tenant compte des 2 sélectionnées
		var tabListesARecharger = nomListesARecharger.split("#");	// transformation de la chaîne des listes à recharger en tableau
		//alert("nomListesARecharger="+nomListesARecharger);
		for (var i=0; i < tabListesARecharger.length - 1; i++)
		{
			var listeARecharger = tabListesARecharger[i];
			var donnees = 'listeARecharger=' + listeARecharger;
			if (nbListesSel > 0)
			{
				var jsonTabListeSel = JSON.stringify(tabListesSel);	// Sérialisation JSON du tableau des listes sélectionnées avec les valeurs des codes choisis
				donnees = donnees + '&listesSelectionnees=' + jsonTabListeSel;	// Ensemble des données à envoyer à la page PHP de requête via AJAX
			}
			//alert("data="+donnees);
			$.ajax({
					url : 'er-requete',
					type : 'POST',
					data : donnees,
					datatype : 'text',
					success : function(retour, status){
						// Recupération du résultat de la requête et rechargement de la ou des listes restantes
						// Forme <nom liste à recharger>=[{"<id>":"valeur id","<description>","valeur description","<visible>","0/1"}, {...}, ..., {...}]
						//alert("retour="+retour);
						var tabResultat = (retour.split("||"))[0].split("=");
						listeARecharger = tabResultat[0];
						var tabListeResultat = JSON.parse(tabResultat[1]);
						//alert("retour="+retour+" - tabListeResultat.length="+tabListeResultat.length);
						reloadSelect(listeARecharger, tabListeResultat);
					},
					error : function(retour, status, error){
					},
					complete : function(retour, status){
					}
				});
		}
	}
	else
	{
		// 3 listes sélectionnées : on ne fait rien de plus
		return;
	}*/
}

//Rechargement de la liste passée en paramètre avec le tableau tabValText fourni
function reloadSelect(nomListe, tabValText)
{
	if (tabValText == undefined || tabValText == null)
	{
		return;
	}

	// Le nom de la liste commence toujours par une lettre en minuscule
	nomListe = nomListe.charAt(0).toLowerCase() + nomListe.substring(1);
	
	// On va effectuer le rechargement sur les clones des listes concernées. Comme la liste natureMatiere
	// est un select MULTIPLE, il n'est pas possible de cacher ses éléments, contrairement à une liste simple
	// Tous les colones sont des listes simples, même celui de natureMatiere
	// L'étape suivante sera de copier les seules options visibles des listes clones dans les listes respectives 
	// Classe, NatureMatiere et Fonction
	// Enfin, il y'aura parcours de chaque liste pour sélectionner les options qui doivent l'être (grâce au tableau tabValSel)

	// On vide la liste destination
	$( "#" + nomListe + " option").each(function(){
		this.remove();
	});
	
//alert(nomListe+" - tabValText.length=" +tabValText.length+" - tabIdSel["+nomListe+"].length="+tabIdSel[nomListe].length);
	// On y rajoute l'option "" de la liste clone
	$("#" + nomListe).append($("#" + nomListe + "Clone option[value='']").clone());
	
	// On ajoute dans la liste destination que les seules options visibles dans la table tabValText
	for (var i=0; i < tabValText.length; i++)
	{
		if (tabValText[i].visible > 0)
		{
			$("#" + nomListe).append('<option value="' + tabValText[i].id + '">' + tabValText[i].description + '</option>');
		}
	}

	// On sélectionne dans la liste destination les éléments désignés par leur id dans la table tabIdSel[nomListe]
	if (tabIdSel[nomListe] != null)
	{
		//alert("tabIdSel["+nomListe+"].length="+tabIdSel[nomListe].length);
		$("#" + nomListe + "Clone option").each(function(){
			//alert(this.value+" - "+tabIdSel[nomListe].join(","));
			if ( jQuery.inArray( this.value, tabIdSel[nomListe] ) >= 0 )
			{
				//alert(this.value + " se trouve dans tabIdSel["+nomListe+"]");
				// L'id de cet option fait bien partie des options à sélectionner
				if ($("#" + nomListe + " option[value='" + this.value + "']").length == 0)
				{
					// On clone cette option faisant partie de la liste $("#" + nomListe + "Clone") dans la liste destination puis on la sélectionne
					//alert("-> on clone l'option "+this.value+" dans la liste destination"); 
					$("#" + nomListe).append( $(this).clone() );
				}
				
				// Si cet option existe déjà dans la liste destination, on la sélectionne
				//alert("-> on selectionne l'option "+this.value+" dans la liste destination"); 
				$("#" + nomListe + " option[value='" + this.value + "']").attr("selected", "selected");
			}
		});
	}
}

//Validation et envoi du formulaire
function valideFormulaire() {

	//document.write("<br/>debut validation formulaire");
	// On positionne l'indicateur "inclueElementPoterie" selon que la zone de saisie
	// des éléments poterie est cachée ou non (à prendre en compte ou non)
	$("#inclueElementPoterie").val(0);
	$('#div_corps_2:visible > #inclueElementPoterie').val(1);

	//document.write("<br/>fin validation formulaire 1");
	test();

	//alert('valeur : ' +formulaire);
	
	// Validation du formulaire
	if (yav.performCheck("formulaire", rules, "inline"))
	{
		test();
		//document.write("<br/>debut validation yav");
		// On bloque les boutons "Valider" et "Reset"
		$('.input_valider').attr('disabled', true);
		$('.input_annuler').attr('disabled', true);
		
		// On effectue jusqu'à 600 tentatives pour envoyer le formulaire, soit 300 secondes
		soumissionOK = true;
		submitFormulaire(600);
		//document.write("fin validation yav");
	}
	//document.write("<br/>fin validation formulaire 2");
}

// Remise à 0 de tous les champs du formulaire
function resetFormulaire() {
	$("#formulaire").each(function() {
		this.reset();
	});
}

// Règles de gestion pour la validation du formulaire côté client en utilisant YAV
var rules=new Array();
var idRule = 0;
rules[idRule++]='date_enregistrement:<?php echo Message("IHM_FormulaireElementsRecueillis_1"); ?>|mask';
rules[idRule++]='date_enregistrement:<?php echo Message("IHM_FormulaireElementsRecueillis_1"); ?>|date';
rules[idRule++]='date_enregistrement:<?php echo Message("IHM_FormulaireElementsRecueillis_1"); ?>|mask|date';
//rules[idRule++]='datation_debut:<?php echo Message("IHM_FormulaireElementsRecueillis_3"); ?>|date';
//rules[idRule++]='datation_debut:<?php echo Message("IHM_FormulaireElementsRecueillis_3"); ?>|mask|date';
//rules[idRule++]='datation_fin:<?php echo Message("IHM_FormulaireElementsRecueillis_4"); ?>|date';
//rules[idRule++]='datation_fin:<?php echo Message("IHM_FormulaireElementsRecueillis_4"); ?>|mask|date';
//rules[idRule++]='datation_debut:<?php echo Message("IHM_FormulaireElementsRecueillis_3"); ?>|date_le|$datationFinUtilisation:<?php echo Message("IHM_FormulaireElementsRecueillis_4"); ?>';
rules[idRule++]='intervenant:<?php echo Message("IHM_FormulaireElementsRecueillis_2"); ?>|mask';
rules[idRule++]='etat:<?php echo Message("IHM_FormulaireElementsRecueillis_7"); ?>|mask';
rules[idRule++]='statut:<?php echo Message("IHM_FormulaireElementsRecueillis_8"); ?>|mask';
rules[idRule++]='quantite:<?php echo Message("IHM_FormulaireElementsRecueillis_12"); ?>|double';
rules[idRule++]='poids:<?php echo Message("IHM_FormulaireElementsRecueillis_13"); ?>|double';
rules[idRule++]='longueur:<?php echo Message("IHM_FormulaireElementsRecueillis_14"); ?>|double';
rules[idRule++]='largeur:<?php echo Message("IHM_FormulaireElementsRecueillis_15"); ?>|double';
rules[idRule++]='hauteur:<?php echo Message("IHM_FormulaireElementsRecueillis_16"); ?>|double';
rules[idRule++]='diametre:<?php echo Message("IHM_FormulaireElementsRecueillis_17"); ?>|double';
rules[idRule++]='incertitude:<?php echo Message("IHM_FormulaireElementsRecueillis_32"); ?>|double';
rules[idRule++]='proportionConserve:<?php echo Message("IHM_FormulaireElementsRecueillis_34"); ?>|double';
rules[idRule++]='quantite|mask|0123456789.';
rules[idRule++]='poids|mask|0123456789.';
rules[idRule++]='longueur|mask|0123456789.';
rules[idRule++]='largeur|mask|0123456789.';
rules[idRule++]='hauteur|mask|0123456789.';
rules[idRule++]='diametre|mask|0123456789.';
rules[idRule++]='incertitude|mask|0123456789.';
rules[idRule++]='proportionConserve|mask|0123456789.';
var idRulePreCond = idRule;
rules[idRule++]='inclueElementPoterie|equal|1|pre-condition';
rules[idRule++]='caractere_pate|mask|post-condition';
rules[idRule++]=idRulePreCond+'|implies|'+(idRule-2)+'|<?php echo Message("fonctions_ValideValeur_2").": ".Message("IHM_FormulaireElementsRecueillis_22"); ?>';
rules[idRule++]='production|mask|post-condition';
rules[idRule++]=idRulePreCond+'|implies|'+(idRule-2)+'|<?php echo Message("fonctions_ValideValeur_2").": ".Message("IHM_FormulaireElementsRecueillis_23"); ?>';
rules[idRule++]='forme|mask|post-condition';
rules[idRule++]=idRulePreCond+'|implies|'+(idRule-2)+'|<?php echo Message("fonctions_ValideValeur_2").": ".Message("IHM_FormulaireElementsRecueillis_24"); ?>';
/*rules[idRule++]='decor|mask|post-condition';
rules[idRule++]=idRulePreCond+'|implies|'+(idRule-2)+'|<?php echo Message("fonctions_ValideValeur_2").": ".Message("IHM_FormulaireElementsRecueillis_25"); ?>';
rules[idRule++]='traces_usage|mask|post-condition';
rules[idRule++]=idRulePreCond+'|implies|'+(idRule-2)+'|<?php echo Message("fonctions_ValideValeur_2").": ".Message("IHM_FormulaireElementsRecueillis_26"); ?>';*/
rules[idRule++]='proportionConserve|mask|post-condition';
rules[idRule++]=idRulePreCond+'|implies|'+(idRule-2)+'|<?php echo Message("fonctions_ValideValeur_2").": ".Message("IHM_FormulaireElementsRecueillis_34"); ?>';
rules[idRule++]='classification|mask|post-condition';
rules[idRule++]=idRulePreCond+'|implies|'+(idRule-2)+'|<?php echo Message("fonctions_ValideValeur_2").": ".Message("IHM_FormulaireElementsRecueillis_28"); ?>';

// Variables pour la gestion des listes associées (Classe, NatureMatiere, Fonction)
// Tableau associatif stockant les valeurs de base de chaque liste
var tabIdSel = new Object();		// Tableau contenant, pour chaque liste déroulante, la liste des id sélectionnées ou à sélectionner
var nbListesSel = 0;				// Nb de listes effectivement sélectionnées
/** 
 * script javascript de gestion des intervenants coté client.
 * Description		:	La ligne s'affiche après validation d'un participant dans la zone "Liste des participations" en dernière ligne.
 * 						Elle sera validée lors de l'exécution du formulaire.
 * 						La possibilité de supprimer cette ligne existe, comme pour les autres.
 * 
 * Créé le			: 05/12/2014
 * Information		: FSN v1.5
 * Modification		: Reprise du code provenant des us conçu par ANG.
 * 						Ajout des tests sur les dates.
 * 						Gestion des messages en cas d'anomalie lors des tests.
 * 						Suppression de l'appel à datepicker (mais conservation du code comme modèle).
 * 						Suppression du select (mais conservation du code comme modèle).
 * 
 * @author nobody
 */

// Ajouter une participation
function ajouterParticipation()
{	
	var Cell;
	var id_intervenant 		= document.getElementById("select_intervenant").value;
	var text_intervenant 	= document.getElementById("select_intervenant").options[document.getElementById("select_intervenant").selectedIndex].text;
	var partDebut 			= document.getElementById("datedebut_part").value;
	var oaDebut 			= document.getElementById("datedebut").value;
	var partFin 			= document.getElementById("datefin_part").value;
	var oaFin 				= document.getElementById("datefin").value;
	var role_intervenant 	= document.getElementById("role_intervenant").value;
	var roleSelectionne 	= document.getElementById("role_intervenant").selectedIndex;
	var selectRole 			= document.getElementById("role_intervenant").options[document.getElementById("role_intervenant").selectedIndex].text;
	var tableau 			= document.getElementById('tableParticipation');
	
	
	
	var regSeparateur			= new RegExp('[/]+','g');
	
	var oaDebutItem				= oaDebut.split(regSeparateur);
	var oaDebutCompare			= '';
	if( oaDebutItem == null){
		alert('Aucun élémént trouvé ! ...');
    } else {
       var	oaDebutCompare	= oaDebutCompare + oaDebutItem[2];
       var	oaDebutCompare	= oaDebutCompare + oaDebutItem[1];
       var	oaDebutCompare	= oaDebutCompare + oaDebutItem[0];
    }
	
	var oaFinItem				= oaFin.split(regSeparateur);
	var oaFinCompare			= '';
	if(oaFinItem == null){
		alert('Aucun élémént trouvé ! ...');
    } else {
       var	oaFinCompare	= oaFinCompare + oaFinItem[2];
       var	oaFinCompare	= oaFinCompare + oaFinItem[1];
       var	oaFinCompare	= oaFinCompare + oaFinItem[0];
    }
	
	
	var partDebutItem				= partDebut.split(regSeparateur);
	var partDebutCompare			= '';
	if( partDebutItem == null){
		alert('Aucun élémént trouvé ! ...');
    } else {
       var	partDebutCompare	= partDebutCompare + partDebutItem[2];
       var	partDebutCompare	= partDebutCompare + partDebutItem[1];
       var	partDebutCompare	= partDebutCompare + partDebutItem[0];
    }
	
	var partFinItem				= partFin.split(regSeparateur);
	var partFinCompare			= '';
	if(partFinItem == null){
		alert('Aucun élémént trouvé ! ...');
    } else {
       var	partFinCompare	= partFinCompare + partFinItem[2];
       var	partFinCompare	= partFinCompare + partFinItem[1];
       var	partFinCompare	= partFinCompare + partFinItem[0];
    }
	
	var ctlAlert	= true;
	if(!text_intervenant)
	{
		alert("Erreur : Intervenant non renseigné, obligatoire");
		var ctlAlert	= false;
	}
	if(!selectRole)
	{
		alert("Erreur : Rôle non renseigné, obligatoire");
		var ctlAlert	= false;
	}
	if(!partDebutCompare)
	{
		alert("Erreur : Date de début non renseignée, obligatoire");
		var ctlAlert	= false;
	}
	if(!partFinCompare)
	{
		alert("Erreur : Date de fin non renseignée, obligatoire");
		var ctlAlert	= false;
	}
	if(partDebutCompare > partFinCompare)
	{
		alert("Erreur : La date de début de participation " + partDebut  + " supérieure à la date de fin de participation " + partFin);
		var ctlAlert	= false;
	}
	if(oaDebutCompare > partDebutCompare)
	{
		alert("Erreur : La date de début de participation " + partDebut + " antérieure à la date de début d'opération archéologique " + oaDebut);
		var ctlAlert	= false;
	}
	if(partFinCompare < oaDebutCompare)
	{	
		alert("Erreur : La date de fin de participation " + partFin + " antérieure à la date de début d'opération archéologique " + oaDebut);
		var ctlAlert	= false;
	}	
	if(partFinCompare > oaFinCompare)
	{
		alert("Erreur : La date de fin de participation " + partFin + " postérieure à la date de fin d'opération archéologique " + oaFin);
		var ctlAlert	= false;
	}
	
	if(ctlAlert == true)
	{
		var optsVal = new Array();
		var optsTxt = new Array();
		$('option', '#role_intervenant').each(function(){
			optsVal.push($(this).val());
		})
		$('option', '#role_intervenant').each(function(){
			optsTxt.push($(this).text());
		})
	
		var ligne = tableau.insertRow(-1); 	
		var nb_lignes 	= tableau.rows;
		var longueur 	= nb_lignes.length;
		
		// récupérer la plus grande valeur de l'indice du tableau des interventions.
		//	==> utile pour ne pas donner plusieurs fois le même id aux lignes de la table.
		var indice = getIndice();
		indice++;
			
		Cell = ligne.insertCell(0);
		Cell.style.width 		= "0px";
		Cell.style.padding 		= "0px";
		var inputIdInterv 		= document.createElement("input");		  
		inputIdInterv.type		= "hidden";
		inputIdInterv.id		= "id_intervenant"+indice;
		inputIdInterv.name		= "idInter[]";
		inputIdInterv.value		= id_intervenant;
		inputIdInterv.setAttribute("readOnly","true");
		Cell.appendChild(inputIdInterv);
		
		Cell = ligne.insertCell(1);
		var inputTextInterv = document.createElement("input");
		inputTextInterv.style.width = "90%";
		inputTextInterv.style.height = "15px";
		inputTextInterv.type="text";
		inputTextInterv.className = "color inputColor input_style";
		inputTextInterv.id="text_intervenant_"+indice;
		inputTextInterv.name="intervText[]";
		inputTextInterv.value=text_intervenant;
		inputTextInterv.setAttribute("readOnly","true");
		Cell.appendChild(inputTextInterv);
	
		Cell = ligne.insertCell(2);
		Cell.style.width 		= "4%";
		Cell.style.paddingLeft 	= "15px";
		var spanDebut 			= document.createElement("span");
		spanDebut.className 	= "";
		spanDebut.innerHTML 	= "Début";
		Cell.appendChild(spanDebut);
		
		Cell = ligne.insertCell(3);
		Cell.style.width 		= "15%";
		Cell.style.paddingLeft 	= "15px";
		var inputDateDebut 		= document.createElement("input");	
		inputDateDebut.style.width = "60%";
		inputDateDebut.style.height = "15px";
		inputDateDebut.type		= "date2";
		inputDateDebut.id		= "datedebut_liste_part_"+indice;
		inputDateDebut.name		= "dateDebut[]";
		inputDateDebut.value	= partDebut;
		inputDateDebut.className= "color date_style_us_itv_tab";
		inputDateDebut.setAttribute("readOnly","true");
		Cell.appendChild(inputDateDebut);
		// on appelle la fonction datepicker après avoir créer la cellule, sinon ça ne fonctionne pas
		/*$(inputDateDebut).datepicker({
			buttonImage: "public/images/calendar.png",
			showOn: "button",
			buttonImageOnly: true,
			});*/
		
		Cell = ligne.insertCell(4)
		Cell.style.width = "4%";
		var spanFin 			= document.createElement("span");
		spanFin.className 		= "";
		spanFin.innerHTML 		= "Fin";
		Cell.appendChild(spanFin);
		
		Cell = ligne.insertCell(5); 
		Cell.style.width 		= "15%";
		var inputDateFin 		= document.createElement("input");
		inputDateFin.style.width= "60%";
		inputDateFin.style.height = "15px";
		inputDateFin.type		= "date2";
		inputDateFin.id			= "datefin_liste_part_"+indice;
		inputDateFin.name		= "dateFin[]";
		inputDateFin.value		= partFin;
		inputDateFin.className 	= "color date_style_us_itv_tab";
		inputDateFin.setAttribute("readOnly","true");
		Cell.appendChild(inputDateFin);
		// on appelle la fonction datepicker après avoir créer la cellule, sinon ça ne fonctionne pas
		/*$(inputDateFin).datepicker({
			buttonImage: "public/images/calendar.png",
			showOn: "button",
			buttonImageOnly: true,
			});*/
	
		Cell 					= ligne.insertCell(6);
		Cell.style.width 		= "4%";
		var spanRole 			= document.createElement("span");
		spanRole.className 		= "";
		spanRole.innerHTML 		= "Rôle";
		Cell.appendChild(spanRole);
			
		Cell 					= ligne.insertCell(7);
		Cell.style.display		= "none";
		var inputRoleId 		= document.createElement("input");
		inputRoleId.style.display	= "none";
		inputRoleId.type		= "hidden";
		inputRoleId.id			= "role_intervenant_" + indice;
		inputRoleId.name		= "idRole[]";
		inputRoleId.value		= role_intervenant;
		inputRoleId.setAttribute("readOnly","true");
		Cell.appendChild(inputRoleId);
		
		
		/*
		 * A conserver comme modèle d'affichage d'un select
		 */
		Cell = ligne.insertCell(8);
		Cell.style.width = "20%";
		var selectRole = document.createElement("select");
		selectRole.style.width = "90%";
		//selectRole.style.height = "20px";
		selectRole.id="selectTypeRole_" + indice;
		selectRole.name="selectTypeRole[]";
		selectRole.className = "color select_style_us_itv_tab";
	    
		for (var key = 0; key < optsVal.length; key++)
		{
			var element = document.createElement("option");
		    element.value = optsVal[key];
		    element.text = optsTxt[key];
		    selectRole.appendChild(element);
		}
		Cell.appendChild(selectRole);
		
		// réafficher le libellé sélectionné de la liste role
		document.getElementById("selectTypeRole_" + indice).selectedIndex=roleSelectionne;
		/*
		 * Fin de la partie à conserver
		 */
		
		/*Cell 					= ligne.insertCell(8);
		Cell.style.width 		= "20%";
		var inputRole 			= document.createElement("input");
		inputRole.style.width 	= "90%";
		inputRole.style.height 	= "20px";
		inputRole.id			= "roleSelectionne" + longueur;
		inputRole.name			= "selectRole[]";
		inputRole.value			= selectRole;
		inputRole.setAttribute("readOnly","true");
		inputRole.className 	= "color inputColor input_style";
		Cell.appendChild(inputRole);*/
		
		Cell = ligne.insertCell(9);
		var btnSupp = document.createElement("input");
		btnSupp.type = "button";
		btnSupp.value = "";
		btnSupp.className = "btn-supprimer-us-itv display-Inline-block vertical-align";
		btnSupp.onclick = function(){suppression(ligne.rowIndex)};
		Cell.appendChild(btnSupp);
	
		
	
		//réafficher le premier intervenant de la liste intervenant
		document.getElementById('select_intervenant').selectedIndex=0;
		// réafficher le premier libellé de la liste type
		document.getElementById('role_intervenant').selectedIndex=0;
		
		
		// Initialiser les dates début et fin
		document.getElementById('datedebut_part').value= "";
		document.getElementById('datefin_part').value= "";
		
	}
}

function suppression(ligne)
{
  document.getElementById('tableParticipation').deleteRow(ligne);
}

function vider_tabInterv()
{
	var tableau = document.getElementById('tableParticipation');
	var nb_lignes = tableau.rows;
	var nb_lig = nb_lignes.length;
	
	for (var i = (nb_lig-1); i >= 0; i--)
	{
		document.getElementById('tableParticipation').deleteRow(i);  
	}

	// réafficher le premier intervenant de la liste intervenant
	document.getElementById('select_intervenant').selectedIndex=0;
	// réafficher le premier libellé de la liste type
	document.getElementById('role_participation').selectedIndex=0;
}
//nke - ang intervention f

// ang : Méthode permettant de récupérer la plus grande valeur de l'indice du tableau des participations.
//		 Retourne ==> 	-1 si le tableau est vide,
//						ou l'indice du dernier élément du tableau.
function getIndice()
{
	var indice = -1;
	
	var nomArray = document.getElementsByName("intervText[]");
	
	for(var i = 0 ; i < nomArray.length ; i++)
	{
	    var idInter = (document.getElementById(nomArray[i].id).id).split('_');
	    
	    //Récupérer la valeur de l'indice dans le dernier poste du tableau idInter : exemple d'id => "text_intervenant_2", on veut récupérer le 2 
	    
	    if (indice < (idInter[(idInter.length)-1])*1)
	    {
	    	indice = idInter[(idInter.length)-1]*1;
	    }   
	}
	return indice;
}
	
// Fonction pour recharger dynamiquement les listes précision production liées à la production
function productionAjax(){
	var xhr = getXMLHttpRequest();
	// On défini ce qu'on va faire quand on aura la réponse
	xhr.onreadystatechange = function()
	{
		// On ne fait quelque chose que si on a tout reçu et que le serveur est ok
		if(xhr.readyState == 4 && xhr.status == 200)
		{
			leselect = xhr.responseText;
			// On se sert de innerHTML pour rajouter les options a la liste
			document.getElementById('precisionprod').innerHTML = leselect;
		}
	}
	// traitement par post
	xhr.open("POST","er-ajax",true);
	// ne pas oublier ça pour le post
	xhr.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
	// ne pas oublier de poster les arguments
	// ici, l'id production
	sel = document.getElementById('production');
	idprod = sel.options[sel.selectedIndex].value;
	xhr.send("idProd="+idprod);
	//alert(idprod);
}
function ajaxReference(statut, id){
	var xhr = getXMLHttpRequest();
	xhr.onreadystatechange = function()
	{
		if(xhr.readyState == 4 && xhr.status == 200)
		{
			selectRef = xhr.responseText;
			

			if(statut == "nom")
			{
				document.getElementById('nomReference').innerHTML = selectRef;
			}
			else
				document.getElementById('listeRef').innerHTML = selectRef;

			/*On évalue le javascript contenu dans les dom*/
			var scripts = document.getElementById('listeRef').getElementsByTagName('script');
			for(var i=0; i < scripts.length;i++)
			{
				/*Sous IE il faut faire un execScript pour que les fonctions soient définie en globale*/
				if (window.execScript)
				{
					/*On replace les éventuels com' html car IE n'aime pas ça*/
					window.execScript(scripts[i].text.replace('<!--',''));
				}
				/*Sous les autres navigateurs on fait un window.eval*/
				else
				{
					window.eval(scripts[i].text);
				}
			}
		}
	}
	xhr.open("POST","reference-ajax",true);
	xhr.setRequestHeader('Content-Type','application/x-www-form-urlencoded');

	var reference = document.getElementById("reference");
	var nameRef = reference.options[reference.selectedIndex].value;
	
	if(statut == "liste")
	{
		xhr.send("nameRef=" + nameRef + "&statut=" + statut);
	}

	if(statut == "insert")
	{
		var libele = document.getElementById("libeleCreer").value;
		var visible = document.getElementById("visibleCreer").checked;
		xhr.send("nameRef=" + nameRef + "&libele=" + libele + "&visible=" + visible + "&statut=" + statut);
		//document.getElementById("libeleCreer").value = '';
		document.getElementById("visibleCreer").value = '';
	}

	if(statut == 0)
	{
		var visible = document.getElementById("visible_"+id).checked;
		var label = document.getElementById("libelle_"+id).value;
		var statut = "update";
		xhr.send("nameRef=" + nameRef  + "&libelle=" + label + "&visible=" + visible + "&id=" + id + "&statut=" + statut);
	}

	if(statut == 1)
	{
		if(confirmationSuppression())
		{
			xhr.send("nameRef=" + nameRef + "&id=" + id + "&statut=delete");
		}
	}

	if(statut == "nom")
	{
		xhr.send("nameRef=" + nameRef  + "&statut=" + statut);
	}
}

function confirmationSuppression() {
	var answer = confirm("Veuillez confirmer la suppression !")
	if (answer){
		return true;
	}
}

function affichageTableau() 
{ 
	var reference = document.getElementById("reference");
	var nameRef = reference.options[reference.selectedIndex].value;
	if(nameRef != 0)
	{
		document.getElementById("affichage").style.display="block"; 
	}
	else
	{
		document.getElementById("affichage").style.display="none"; 
	}
}
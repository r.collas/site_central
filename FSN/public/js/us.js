// Gestion du calcule de l'identifiant
function calculer_identifiant(event) {
	var id = event.target.id ;
	var input_identification = $("#identification") ;
	var identifiant = $("#in-" + id).val() ;
	input_identification.val(identifiant) ; 
}

/**
 * Lister les US appartenant au site de fouille sélectionné : Utilisation de AJAX.
 * XMLHTTPRequest.
 */
/* Création de la variable globale qui contiendra l'objet XHR */
var requete = null;
/**
 * Fonction privée qui va créer un objet XHR.
 * Cette fonction initialisera la valeur dans la variable globale définie
 * ci-dessus.
 */
function creerRequete()
{
    try
    {
        /* On tente de créer un objet XmlHTTPRequest */
        requete = new XMLHttpRequest();
    }
    catch (microsoft)
    {
        /* Microsoft utilisant une autre technique, on essays de créer un objet ActiveX */
        try
        {
            requete = new ActiveXObject('Msxml2.XMLHTTP');
        }
        catch(autremicrosoft)
        {
            /* La première méthode a échoué, on en teste une seconde */
            try
            {
                requete = new ActiveXObject('Microsoft.XMLHTTP');
            }
            catch(echec)
            {
                /* À ce stade, aucune méthode ne fonctionne... mettez donc votre navigateur à jour ;) */
                requete = null;
            }
        }
    }
    if(requete == null)
    {
        alert('Impossible de créer l\'objet requête,\nVotre navigateur ne semble pas supporter les object XMLHttpRequest.');
    }
}
/**
 * Fonction privée qui va mettre à jour l'affichage de la page.
 */
function actualiser()
{
    var listeUS = requete.responseText;
    var blocListe = document.getElementById('blocLiens');
    blocListe.innerHTML = listeUS;
}

/**
 * Fonction publique appelée par la page affichée.
 * Cette fonction va initialiser la création de l'objet XHR puis appeler
 * le code serveur afin de récupérer les données à modifier dans la page.
 */
function lister_US(idSF,idUS)
{
    /* Si il n'y a pas d'US associées au site de fouille, on fait disparaître la seconde liste au cas où elle serait affichée */
    if(idSF == "")
    {
        document.getElementById('blocLiens').innerHTML = '';
    }
    else
    {
        /* À cet endroit précis, on peut faire apparaître un message d'attente */
        var blocListe = document.getElementById('blocLiens');
        blocListe.innerHTML = "Traitement en cours, veuillez patienter...";
        /* On crée l'objet XHR */
        creerRequete();
        /* Définition du fichier de traitement */
        var url = 'us-ajax?idSF='+ idSF + '&idUS='+ idUS;
        //var url = 'ajax/listeUSrattachees.php?idSF='+ idSF;
        /* Envoi de la requête à la page de traitement */
        requete.open('GET', url, true);
        /* On surveille le changement d'état de la requête qui va passer successivement de 1 à 4 */
        requete.onreadystatechange = function()
        {
            /* Lorsque l'état est à 4 */
            if(requete.readyState == 4)
            {
                /* Si on a un statut à 200 */
                if(requete.status == 200)
                {
                    /* Mise à jour de l'affichage, on appelle la fonction appropriée */
                    actualiser();
                }
            }
        };
        requete.send(null);
    }
}


// Ajouter une intervention
function ajouterIntervention()
{
	var Cell;
	var id_intervenant = document.getElementById("select_intervenant").value;
	var text_intervenant = document.getElementById("select_intervenant").options[document.getElementById("select_intervenant").selectedIndex].text;
	var dateDebut = document.getElementById("datedebut_inter").value;
	var dateFin = document.getElementById("datefin_inter").value;
	var type_intervention = document.getElementById("type_intervention").value;
	var type_inter_selectedIndex = document.getElementById("type_intervention").selectedIndex;
	var selectTypeInter = document.getElementById("type_intervention").options[document.getElementById("type_intervention").selectedIndex].text;
	var tableau = document.getElementById('tableIntervention');
	
	if (text_intervenant == "")
	{
		alert("Vous devez sélectionner un intervenant");
	}
	else
	{
		var optsVal = new Array();
		var optsTxt = new Array();
		$('option', '#type_intervention').each(function(){
			optsVal.push($(this).val());
		})
		$('option', '#type_intervention').each(function(){
			optsTxt.push($(this).text());
		})
		
		var ligne = tableau.insertRow(-1); 
			
		var nb_lignes = tableau.rows;
		var longueur = nb_lignes.length;
		
		Cell = ligne.insertCell(0);
		Cell.style.width = "0px";
		Cell.style.padding = "0px";
		var inputIdInterv = document.createElement("input");		  
		inputIdInterv.type="hidden";
		inputIdInterv.id="id_intervenant"+longueur;
		inputIdInterv.name="idInter[]";
		inputIdInterv.value=id_intervenant;
		inputIdInterv.setAttribute("readOnly","true");
		Cell.appendChild(inputIdInterv);
	
		Cell = ligne.insertCell(1);  
		var inputTextInterv = document.createElement("input");		  
		inputTextInterv.type="text";
		inputTextInterv.className = "color inputColor input_style";
		inputTextInterv.id="text_intervenant"+longueur;
		inputTextInterv.name="intervText[]";
		inputTextInterv.value=text_intervenant;
		inputTextInterv.setAttribute("readOnly","true");
		Cell.appendChild(inputTextInterv);
		
		
		Cell = ligne.insertCell(2);
		Cell.style.width = "5%";
		Cell.style.paddingLeft = "15px";
		var spanDebut = document.createElement("span");
		spanDebut.className = "";
		spanDebut.innerHTML = "Début";
		Cell.appendChild(spanDebut);
		
		Cell = ligne.insertCell(3);
		Cell.style.width = "20%";
		var inputDateDebut = document.createElement("input");	
		inputDateDebut.style.width = "50%";
		inputDateDebut.type="date2";
		inputDateDebut.id="datedebut_liste_inter_"+longueur;
		inputDateDebut.name="dateDebut[]";
		inputDateDebut.value=dateDebut;
		inputDateDebut.className = "color date_style_us_itv_tab";	
		Cell.appendChild(inputDateDebut);
		// on appelle la fonction datepicker après avoir créer la cellule, sinon ça ne fonctionne pas
		$(inputDateDebut).datepicker({
			buttonImage: "public/images/calendar.png",
			showOn: "button",
			buttonImageOnly: true,
			});
		
		Cell = ligne.insertCell(4)
		Cell.style.width = "5%";
		var spanFin = document.createElement("span");
		spanFin.className = "";
		spanFin.innerHTML = "Fin";
		Cell.appendChild(spanFin);
		
		Cell = ligne.insertCell(5); 
		Cell.style.width = "20%";
		var inputDateFin = document.createElement("input");
		inputDateFin.style.width = "50%";
		inputDateFin.type="date2";
		inputDateFin.id="datefin_liste_inter_"+longueur;
		inputDateFin.name="dateFin[]";
		inputDateFin.value=dateFin;
		inputDateFin.className = "color date_style_us_itv_tab";
		Cell.appendChild(inputDateFin);
		// on appelle la fonction datepicker après avoir créer la cellule, sinon ça ne fonctionne pas
		$(inputDateFin).datepicker({
			buttonImage: "public/images/calendar.png",
			showOn: "button",
			buttonImageOnly: true,
			});
		
		Cell = ligne.insertCell(6);
		Cell.style.width = "5%";
		var spanType = document.createElement("span");
		spanType.className = "";
		spanType.innerHTML = "Type";
		Cell.appendChild(spanType);
			
		Cell = ligne.insertCell(7);
		Cell.style.width = "10px";
		Cell.style.padding = "0px";
		var inputTypeId = document.createElement("input");
		inputTypeId.style.width = "0px";
		inputTypeId.style.padding = "0px";
		inputTypeId.type="hidden";
		inputTypeId.id="type_intervention" + longueur;
		inputTypeId.name="typeIntervention[]";
		inputTypeId.value=type_intervention;
		inputTypeId.setAttribute("readOnly","true");
		Cell.appendChild(inputTypeId);
		
		Cell = ligne.insertCell(8);
		Cell.style.width = "20%";
		var selectType = document.createElement("select");
		selectType.style.width = "90%";
		selectType.id="selectTypeInter_" + longueur;
		selectType.name="selectTypeInter[]";
		selectType.className = "color select_style_us_itv_tab";
		
		for (var key = 0; key < optsVal.length; key++)
		{
			var element = document.createElement("option");
		    element.value = optsVal[key];
		    element.text = optsTxt[key];
		    selectType.appendChild(element);   
		}
		Cell.appendChild(selectType);
		
		// réafficher le libellé sélectionné de la liste type
		document.getElementById("selectTypeInter_" + longueur).selectedIndex = type_inter_selectedIndex;
			  
		Cell = ligne.insertCell(9);
		var btnSupp = document.createElement("input");
		btnSupp.type = "button";
		btnSupp.value = "";
		btnSupp.className = "btn-supprimer-us-itv display-Inline-block vertical-align";
		btnSupp.onclick = function(){suppression(ligne.rowIndex)};
		Cell.appendChild(btnSupp);
	}
	
	//réafficher le premier intervenant de la liste intervenant
	document.getElementById('select_intervenant').selectedIndex=0;
	// réafficher le premier libellé de la liste type
	document.getElementById('type_intervention').selectedIndex=0;
	
	
	// Initialiser les dates début et fin
	document.getElementById('datedebut_inter').value= "";
	document.getElementById('datefin_inter').value= "";  
}

function suppression(ligne)
{
  document.getElementById('tableIntervention').deleteRow(ligne);
}

function vider_tabInterv()
{
	var tableau = document.getElementById('tableIntervention');
	var nb_lignes = tableau.rows;
	var nb_lig = nb_lignes.length;
	
	for (var i = (nb_lig-1); i >= 0; i--)
	{
		document.getElementById('tableIntervention').deleteRow(i);  
	}

	// réafficher le premier intervenant de la liste intervenant
	document.getElementById('select_intervenant').selectedIndex=0;
	// réafficher le premier libellé de la liste type
	document.getElementById('type_intervention').selectedIndex=0;
}


	
$(document).ready( function () {
    $('#gestionDataTable').DataTable( {
        // chargement du fichier de langue FR
    	"oLanguage": { "sUrl": "js/dataTable/plugins/fr_FR.js" },

		// Définition des collones ou le tri jj/mm/aaaa sera appliqué (voir fichier date-uk.js)
		"aoColumns": [
						null,
    	                { "sType": "uk_date" },
    	                { "sType": "uk_date" },
    	                null,
    	                null,
    	                null,
    	                null
    	            ],
    	columnDefs: [{ type: 'natural', targets: 0 }]
    } );
} );

$(document).ready( function () {
    $('#tblFouilles').DataTable( {
        // chargement du fichier de langue FR
    	"oLanguage": { "sUrl": "js/dataTable/plugins/fr_FR.js" },
    	columnDefs: [{ type: 'natural', targets: 0 }]
    } );
} );
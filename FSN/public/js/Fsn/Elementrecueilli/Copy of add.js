    <?php
            $js_array = json_encode($precision_opt);
    echo "var opt_prod_precisions = ". $js_array . ";\n";
    ?>
    var del_nat_mat = [];       // les natures mati�res d�j� identifi� qui sont � supprimer
    var add_nat_mats = [];      // les natures mati�res � ajouter
    var list_nat_mat = [        // les �l�ments diponibles dans la liste autocomplete
    <?php foreach($naturematieres as $key => $nm){ ?>
        {
            id: <?php echo $nm->get('id') ; ?>,
            label: "<?php echo $nm->get('naturematiere') ; ?>"
        },
    <?php } ?>
    ];
    function supp_vignette(vignette){
        var index = add_nat_mats.indexOf(parseInt(vignette.attr("idnature")));
        if (index > -1) {
            add_nat_mats.splice(index, 1);
        }
        $('#option-nature-'+vignette.attr("idnature")).remove();
        vignette.remove();
    }
    function removeNatureAutocom(idnature){
        var index = 0;
        for(var n in list_nat_mat){
            if(list_nat_mat[n].id == idnature) {
                list_nat_mat.splice(index, 1);
                $("#autocomp-nat-mat").autocomplete("option","source",list_nat_mat);
                return index; // l'�l�ment a �t� supprim�
            }
            index++;
        }
        return -1; // l'�l�ment � supprimer n'est pas dans la liste
    }
    function addNatureAutocom(idnature,label){
        list_nat_mat.push({id: parseInt(idnature), label: label});
    }
    $(function(){
        $("#panel-caract-poterie").hide();
        $("#autocomp-nat-mat").focus(function (event) {
            console.log('focus - autocom');
            $("#autocomp-nat-mat").autocomplete({
                autoFocus : true,
                source: list_nat_mat,
                minLength: 1,
                appendTo: $('#div_resultat'),
                select: function( event, ui ) {
                    add_nat_mats.push(ui.item.id);
                    $("#panel-nature-matiere .panel-body").append("<div class='vignette' id='vignette-"+ui.item.id
                            +"' idnature='"+ui.item.id+"'><span class='label-vignette'>"+ui.item.label
                            +"</span><span class='del-vignette'>X</span></div>");
                    removeNatureAutocom(ui.item.id);
                    $("#vignette-"+ui.item.id).click(function(){
                        supp_vignette($(this))
                        addNatureAutocom($(this).attr('idnature'),$('.label-vignette',$(this)).text())
                    });
                    $('#autocomp-nat-mat').val("");
                    $("#naturematiere_principal").append("<option id='option-nature-"+ui.item.id+"'>"+ui.item.label+"</option>")
                    event.preventDefault();
                }
            });
        });
        $('.del-vignette').click(function(){
            supp_vignette($(this).parent())
            del_nat_mat.puch($(this).parent().attr('idnature'));
        })
        $("#classe_id").change(function(){
            if($(this).val() == '1993' )
                $("#panel-caract-poterie").show("Fold");
            else
                $("#panel-caract-poterie").hide("Fold");
        })
        $("#production_id").change(function(){
            var opts = ""
            var prod_id = parseInt($(this).val());
            for(var opt in opt_prod_precisions[prod_id]){
                opts += "<option value='"+opt_prod_precisions[prod_id][opt].id+"'>"+opt_prod_precisions[prod_id][opt].precision+"</option>";
            }
            $("#precisionproduction_id").html(opts);
        })

    });
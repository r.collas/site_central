function notify(Type, msgTitle, msg, position, fade)
{
	if (fade === undefined) fade = true;
	notif({
		  type: Type,
		  msg: "<b>" + msgTitle + "</b> " + msg,
		  position: position,
		  fade: fade
		});
}

function notifyUpdate()
{
	notif({
		  type: "info",
		  msg: "<b>Information : </b>la mise à jour est automatique",
		  position: "right",
		  multiline: 0,
		  timeout: 5000,
		  width: 400,
		  fade: false
		});
}
var titleState = ["Non","Oui","Indeterminé"]

$(function(){	
    $(".tribox").each(function(){
        
    	if($(this).val() == "0") {

            $(this).prop("checked",false)
    	} else {
            $(this).prop("checked", true)
            if($(this).val() != "1"){
                $(this).prop("indeterminate",true)
                $(this).val("null")
            }
        }
        
        $(this).attr('title',titleState[($(this).prop('checked')?1:0)+($(this).prop('indeterminate')?1:0)])
    })


    $(".tribox").click(function(){
    
    	if($(this).prop("checked")){
    		$(this).prop("indeterminate",true)
            $(this).val("null")
        }
        else if($(this).val() == "null"){
            $(this).prop("indeterminate",false)
            $(this).prop("checked",true)
            $(this).val(1)
        }
        $(this).attr('title',titleState[($(this).prop('checked')?1:0)+($(this).prop('indeterminate')?1:0)])
    })
})

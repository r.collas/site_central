go :-
  go. 
% superposition(US_ANTERIEUR,US_POSTERIEUR) 
superposition('CYG.1092','CYG.1041'). 
superposition('CYG.1052','CYG.1041'). 
superposition('CYG.1052','CYG.1053'). 
superposition('CYG.1029','CYG.1028'). 
superposition('CYG.1059','CYG.1041'). 
superposition('CYG.1077','CYG.1078'). 
superposition('CYG.1022','CYG.1035'). 
superposition('CYG.1075','CYG.1076'). 
superposition('CYG.1075','CYG.1069'). 
superposition('CYG.1045','CYG.1027'). 
superposition('CYG.1070','CYG.1059'). 
superposition('CYG.1081','CYG.1082'). 
superposition('CYG.1087','CYG.1088'). 
superposition('CYG.1030','CYG.1031'). 
superposition('CYG.1030','CYG.1032'). 
superposition('CYG.1056','CYG.1041'). 
superposition('CYG.1034','CYG.1033'). 
superposition('CYG.1072','CYG.1071'). 
superposition('CYG.1093','CYG.1094'). 
superposition('CYG.1084','CYG.1083'). 
superposition('CYG.1028','CYG.1022'). 
superposition('CYG.1041','CYG.1054'). 
superposition('CYG.1094','CYG.1095'). 
superposition('CYG.1079','CYG.1080'). 
superposition('CYG.1060','CYG.1061'). 
superposition('CYG.1058','CYG.1041'). 
superposition('CYG.1003','CYG.1004'). 
superposition('CYG.1085','CYG.1086'). 
superposition('CYG.1057','CYG.1041'). 
superposition('CYG.1062','CYG.1063'). 
superposition('CYG.1026','CYG.1022'). 
superposition('CYG.1002','CYG.1003'). 
superposition('CYG.1027','CYG.1001'). 
superposition('CYG.1027','CYG.1022'). 
superposition('CYG.1055','CYG.1052'). 
superposition('CYG.1055','CYG.1070'). 
superposition('CYG.1066','CYG.1067'). 
superposition('CYG.1048','CYG.1044'). 
superposition('CYG.1048','CYG.1022'). 
superposition('CYG.1048','CYG.1049'). 
superposition('CYG.1046','CYG.1047'). 
superposition('CYG.1074','CYG.1073'). 
superposition('CYG.1032','CYG.1031'). 
superposition('CYG.1024','CYG.1022'). 
superposition('CYG.1024','CYG.1025'). 
superposition('CYG.1035','CYG.1036'). 
superposition('CYG.1065','CYG.1041'). 
superposition('CYG.1065','CYG.1068'). 
% synchronisation(US_LEFT,US_RIGHT) 
synchronisation('CYG.1022','CYG.1001'). 
synchronisation('CYG.1008','CYG.1007'). 
synchronisation('CYG.1023','CYG.1001'). 
synchronisation('CYG.1064','CYG.1092'). 
synchronisation('CYG.1096','CYG.1093'). 
synchronisation('CYG.1097','CYG.1094'). 
% superposition(A,P)
% synchronisation(S1,S2)

synchro(X,Y) :-
	synchronisation(X,Y).
  
synchro(X,Y) :-
	synchronisation(X,Z),
	synchro(Z,Y).

anterieur(X,Y) :-
	superposition(X,Y).
  
anterieur(X,Y) :-
	superposition(X,Z),
	anterieur(Z,Y).
  
anterieur(X,Y) :-
	synchro(X,Z),
	anterieur(Z,Y).

posterieur(X,Y) :-
	anterieur(Y,X).

incoherenceTemporelle(X,Y) :-
	anterieur(X,Y),
	posterieur(X,Y).

% Recherche des niveaux anteroposterite
% initialisation de la recherche du niveau initial
niveauPosteriorite(1,X) :-
  not(posterieur(X,_)).

% Recherche des niveaux successifs
niveauPosteriorite(N,X) :-
  superposition(Y,X),
  niveauPosteriorite(N-1,Y).
  
% Initialisation du niveau initial
niveauSynchro(1,X) :-
  synchro(X,_).

% Recherche des niveaux successifs
niveauSynchro(N,X) :-
  synchronisation(X,Y),
  niveauSynchro(N,Y).
  
  

findAnterieur(X) :-
	findall(Y, anterieur(X,Y), RESULT),
  write(RESULT).

findPosterieur(X) :-
	findall(Y, posterieur(X,Y), RESULT),
  write(RESULT).

findAllRacine(X) :-
  findall(X, niveauPosteriorite(1,X), RESULT),
  write(RESULT).
    
?- findAllRacine(_).

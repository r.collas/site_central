% superposition(A,P)
% synchronisation(S1,S2)

synchro(X,Y) :-
	synchronisation(X,Y).
  
synchro(X,Y) :-
	synchronisation(X,Z),
	synchro(Z,Y).

anterieur(X,Y) :-
	superposition(X,Y).
  
anterieur(X,Y) :-
	superposition(X,Z),
	anterieur(Z,Y).
  
anterieur(X,Y) :-
	synchro(X,Z),
	anterieur(Z,Y).

posterieur(X,Y) :-
	anterieur(Y,X).

incoherenceTemporelle(X,Y) :-
	anterieur(X,Y),
	posterieur(X,Y).

findAnterieur(X) :-
	findall(Y, anterieur(X,Y), RESULT),
  write(RESULT).

findPosterieur(X) :-
	findall(Y, posterieur(X,Y), RESULT),
  write(RESULT).
  
init:-
  init. 
% synchronisation(US_LEFT,US_RIGHT) 
synchronisation('CYG.1022','CYG.1001'). 
synchronisation('CYG.1008','CYG.1007'). 
synchronisation('CYG.1023','CYG.1001'). 
synchronisation('CYG.1064','CYG.1092'). 
synchronisation('CYG.1096','CYG.1093'). 
synchronisation('CYG.1097','CYG.1094'). 
% superposition(A,P)
% synchronisation(S1,S2)

synchro(X,Y) :-
	synchronisation(X,Y).
  
synchro(X,Y) :-
	synchronisation(X,Z),
	synchro(Z,Y).

anterieur(X,Y) :-
	superposition(X,Y).
  
anterieur(X,Y) :-
	superposition(X,Z),
	anterieur(Z,Y).
  
anterieur(X,Y) :-
	synchro(X,Z),
	anterieur(Z,Y).

posterieur(X,Y) :-
	anterieur(Y,X).

incoherenceTemporelle(X,Y) :-
	anterieur(X,Y),
	posterieur(X,Y).

findAnterieur(IDENTIFICATION) :-
	findall(anterieur(IDENTIFICATION,Y), RESULT).
  write(RESULT).
  
?- findAnterieur('CYG.1055').

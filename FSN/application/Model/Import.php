<?php
use Trismegiste\Prolog\QueryCompiler;
class Model_Import extends Yab_Db_Table {

	public $table;
	public $table_tmp;
	public $name_model;
	public $erreur;
	public $session;
	public $last_field;
	protected $_name;


	public function setTable($table){
		$this->table = $table;
	}

	public function setTableTmp($table_tmp){
		$this->table_tmp = $table_tmp;
	}

	public function setNameMdel($name_model){
		$this->name_model = $name_model;
	}

	/*
	 * @return string : retourne le nom de la table temporaire
	 */
	public function getTableTmp(){
		return $this->table_tmp;
	}

	/*
	 * Liste tout les enregistrement de la table temporaire
	 * @return Object Yab_Db_Statment
	 */
	public function listeImport(){
		$sql = "SELECT * FROM $this->table_tmp ";

		$db = $this->getAdapter();
		$result =$db->prepare($sql);
		return $result;
	}

	/*
	 * Genère le nom de la table temporaire et le set dans $this->table_tmp
	 * @param $loginuser : String : Login de l'utilisateur
	 * @return void;
	 */
	public function createNameTableTmp($loginUser){
		$table_tmp = "import_tmp_".$this->table."_".$loginUser;
		$this->setTableTmp($table_tmp);
		$this->_name=$this->table_tmp;
		$this->session->set ( 'table_tmp', $this->table_tmp );
	}

	/*
	 * Supprime la table temporaire $this->table_tmp;
	 * @return object Yab_Db_Table;
	 */
	public function dropTableTmp(){
		$sql = "drop table if exists $this->table_tmp";
		return $db = $this->getAdapter()->query($sql);
	}

	/*
	 * Créer la table temporaire en fonction de la scructure de la table $this->table
	 * Tous les champs sont de type VARCHAR(255)
	 * Ajout d'un champ tmp_id PRIMARY AUTO_INCREMENT
	 * Ajout d'un champ erreur VARCHAR(255)
	 *
	 */
	public function createTableTmp(){
		$db = $this->getAdapter();

		//récupération de la structure de la table $this->table au travers de son modèle $this->name_model
		$obj = new $this->name_model();
		$coluns = $obj->getColumns();

		$sql="drop table if exists $this->table_tmp";
		$db->query($sql);

		$sql="create table $this->table_tmp (";
		foreach($coluns as $name_colun => $colun_info){
			$sql.="$name_colun VARCHAR(255) DEFAULT NULL,";
			$array[]=$name_colun;
		}

		$sql.="toto";
		$sql = str_replace(",toto", ") ENGINE = MEMORY", $sql);

		$this->last_field=end($array);

		return $db->query($sql);

	}

	/*
	 * Ajoute un champ tmp_id PRIMARY KEY AUTO_INCREMENT à la tbale temporaire $this->table_tmp
	 */
	private function addFieldsId(){
		$db = $this->getAdapter();
		$sql="ALTER TABLE " . $this->table_tmp. "
          ADD tmp_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY";
		return $db->query($sql);
	}

	/*
	 * Ajoute un champ tmp_erreur VARCHAR(255) à la table temporaire $this->table_tmp;
	 */
	private function addFieldErreur(){
		$db = $this->getAdapter();
		$sql = " ALTER TABLE " . $this->table_tmp . "
          ADD  tmp_erreur VARCHAR(10000) DEFAULT \"\" ";
		return $db->query($sql);
	}
	/*
	 * Import un fichier CSV dans la table temporaire $this->table_tmp;
	 * @param $file string : chemin jusqu'au fichier CSV
	 */
	public function importCSVTmp($file,$ligneIngore=0){
		$db = $this->getAdapter();

		//Vérification de l'existance du fichier
		if (!file_exists($file)){
			$this->erreur = "Le fichier n'existe pas ou est introuvable";
			return false;
		}
		//vérification de l'extension du fichier
		if (pathinfo($file,PATHINFO_EXTENSION) != "csv"){
			$this->erreur = "Le fichier n'est pas au format CSV";
			return false;
		}


		$this->createTableTmp();

		$this->addFieldsId();
		$this->addFieldErreur();

		$f = file($file);
		$fichier = fopen($file,"w+");
		foreach ($f as $key => $ligne){
			fwrite($fichier,addcslashes(utf8_encode($ligne),"\"\,\'"));
		}

		fclose($fichier);

		$sql="LOAD DATA
				INFILE '".addslashes($file)."'
				INTO TABLE ".$this->table_tmp."
				FIELDS
             	 	TERMINATED BY ';'
				LINES TERMINATED BY '\\n'
		";


		if ($ligneIngore > 0)
			$sql.=" IGNORE $ligneIngore LINES ";

		$db->query($sql);

		$this->deleteChar("\n", $this->last_field);
		$this->deleteChar("\r", $this->last_field);

		return true;
	}


	public function _checkInt($field_name,$size){
		$result = $db->prepare('
          UPDATE ' . $this->table_tmp . ' AS tmp
          SET tmp_erreur = CONCAT(tmp_erreur, Le champ '. $field_name .' doit avoir une taille de '.$size.' caractères \n)
          WHERE cmt.env_monitors NOT IN ("PROD", "QUALIF")
      ');
	}

	/*
	 * Génère un UUID et remplace la valeur du champ $field_nam de la table $table_tmp
	 */
	public function _setUUID($field_name, $table_tmp=null){
		$db = $this->getAdapter();
		if(is_null($table_tmp))
			$table_tmp=$this->table_tmp;

		$sql = 'UPDATE ' .$table_tmp . ' SET ' . $field_name . ' = UUID()';

		$db->query($sql);

		$sql=" ALTER TABLE  `".$table_tmp."` ADD UNIQUE (
				`".$field_name."`
				)";
		$db->query($sql);
	}

	/*
	 * Verifie que le chamop $field_name de la table temporaire soit une date valide au format "YYYY-MM-DD"
	 * @param $field_name : String nom du champ
	 * @param [$table_tmp] : String nom de la table temporaire
	 */
	public function _checkDate($field_name, $table_tmp=null){
		$db = $this->getAdapter();
		if(is_null($table_tmp))
			$table_tmp=$this->table_tmp;

		$sql='UPDATE ' . $this->table_tmp . '
			SET tmp_erreur = CONCAT(tmp_erreur," Le champ '.$field_name.' doit être une date au format AAAA-MM-DD \n")
			WHERE (ISNULL(STR_TO_DATE('.$field_name.',"%Y-%m-%d")) = 1) AND (' .$field_name . ' IS NOT NULL OR ' . $field_name . ' <> "")';

		$db->query($sql);
	}

	/*
	 * Verifie que le chamop $field_name de la table temporaire soit une année valide au format "YYYY"
	 * @param $field_name : String nom du champ
	 * @param [$table_tmp] : String nom de la table temporaire
	 */
	public function _checkAnnee($field_name, $table_tmp=null){
		$db = $this->getAdapter();
		if(is_null($table_tmp))
			$table_tmp=$this->table_tmp;

		$sql='UPDATE ' . $this->table_tmp . '
			SET tmp_erreur = CONCAT(tmp_erreur," Le champ '.$field_name.' doit être une année au format AAAA \n")
			WHERE (ISNULL(STR_TO_DATE('.$field_name.',"%Y")) = 1) AND (' .$field_name . ' IS NOT NULL OR ' . $field_name . ' <> "")';

		$db->query($sql);
	}

	/*
	 * Verifie que le chamop $field_name de la table temporaire soit un mois valide (entre 1-12)
	 * @param $field_name : String nom du champ
	 * @param [$table_tmp] : String nom de la table temporaire
	 */
	public function _checkMois($field_name, $table_tmp=null){
		$db = $this->getAdapter();
		if(is_null($table_tmp))
			$table_tmp=$this->table_tmp;

		$sql='UPDATE ' . $this->table_tmp . '
			SET tmp_erreur = CONCAT(tmp_erreur," Le champ '.$field_name.' doit être un mois au format MM \n")
			WHERE (ISNULL(STR_TO_DATE('.$field_name.',"%m")) = 1) AND (' .$field_name . ' IS NOT NULL OR ' . $field_name . ' <> "")';

		$db->query($sql);
	}

	/*
	 * Verifie que le chamop $field_name de la table temporaire soit un jours valide (entre 1 et 31)
	 * @param $field_name : String nom du champ
	 * @param [$table_tmp] : String nom de la table temporaire
	 */
	public function _checkJour($field_name, $table_tmp=null){
		$db = $this->getAdapter();
		if(is_null($table_tmp))
			$table_tmp=$this->table_tmp;

		$sql='UPDATE ' . $this->table_tmp . '
			SET tmp_erreur = CONCAT(tmp_erreur," Le champ '.$field_name.' doit être un jour au format JJ \n")
			WHERE (ISNULL(STR_TO_DATE('.$field_name.',"%d")) LIKE 1) ';

		$db->query($sql);
	}

	/*
	 * Verifie que le chamop $field_name de la table temporaire soit un numéric (nombre entier positif ou négatif)
	 * @param $field_name : String nom du champ
	 * @param [$table_tmp] : String nom de la table temporaire
	 */
	public function _checkNumeric($field_name,$table_tmp=null){
			$db = $this->getAdapter();
			if(is_null($table_tmp))
				$table_tmp=$this->table_tmp;

			$sql='UPDATE ' . $this->table_tmp . '
			SET tmp_erreur = CONCAT(tmp_erreur," Le champ '.$field_name.' doit être un numérique \n")
			WHERE (TRIM('.$field_name.') NOT REGEXP (\'^[-0-9]*[0-9]$\')) AND '.$field_name.' <> "" ';

			$db->query($sql);
	}

	/*
	 * Renvoie TRUE si la tabletemporaire contient des erreur ou FALSE si elle n'en contient pas
	 * @return booléen TRUE or FALSE
	 */
	public function erreur(){
		$db = $this->getAdapter();
		$sql = 'SELECT * from '.$this->table_tmp.' where tmp_erreur <> "" ';

		$sql=$db->prepare($sql);
		$sql->execute();
		$result=$sql->count();

		if ($result > 0)
			return true;
		else
			return false;
	}

	/*
	 * Verifie que le chamop $field_name de la table temporaire soit un nombre flotant
	 * @param $field_name : String nom du champ
	 * @param [$table_tmp] : String nom de la table temporaire
	 */
	public function _checkFloat($field_name, $table_tmp=null){
		$db = $this->getAdapter();
		if(is_null($table_tmp))
			$table_tmp=$this->table_tmp;

		$sql='UPDATE ' . $this->table_tmp . '
			SET tmp_erreur = CONCAT(tmp_erreur," Le champ '.$field_name.' doit être un numérique \n")
			WHERE (TRIM('.$field_name.') NOT REGEXP (\'^[-+]?[0-9]*\.?[0-9]+$\')) AND '. $field_name.'!= ""';
		$db->query($sql);
	}

	/*
	 * Verifie que le chamop $field_name de la table temporaire n'est pas la valeur NULL ou "";
	 * @param $field_name : String nom du champ
	 * @param [$table_tmp] : String nom de la table temporaire
	 */
	public function _checkNotNull($field_name,$table_tmp=null){
		$db = $this->getAdapter();
		if(is_null($table_tmp))
			$table_tmp=$this->table_tmp;

		$sql='UPDATE ' .$table_tmp . '
			SET tmp_erreur = CONCAT(tmp_erreur," Le champ '.$field_name.' ne doit pas être NULL \n")
			WHERE (ISNULL(TRIM('.$field_name.'))=1 ) ';
		$db->query($sql);
	}

	private function _checkEntiteAdmin($field_name,$table_reference,$field_reference,$table_tmp=null){
		$db = $this->getAdapter();
		if(is_null($table_tmp))
			$table_tmp=$this->table_tmp;

		$sql='UPDATE ' . $this->table_tmp . '
			SET tmp_erreur = CONCAT(tmp_erreur," La valeur du champ '.$field_name.' est introuvable dans la table de référence '.$table_reference.' \n")
			WHERE '.$field_name.'
					IN (SELECT '. $field_reference .' from '.$table_reference.')  ';
		$db->query($sql);
	}

	private function _checkNomabregeSitefouille($field_name,$table_reference,$field_reference,$table_tmp=null){
		$db = $this->getAdapter();
		if(is_null($table_tmp))
			$table_tmp=$this->table_tmp;

		$sql='UPDATE ' . $this->table_tmp . '
			SET tmp_erreur = CONCAT(tmp_erreur," La valeur du champ '.$field_name.' est introuvable dans la table de référence '.$table_reference.' \n")
			WHERE '.$field_name.'
					NOT IN (SELECT '. $field_reference .' from '.$table_reference.')  ';

		$db->query($sql);
	}

	/*
	 * Verifie que le chamop $field_name de la table temporaire corresponde
	 * à une valeur du champ $field_reference de la table $table_reference
	 * @param $field_name : String nom du champ
	 * @param $table_reference : String nom de la table étrangère
	 * @param $field_rerefence : String nom du champ dans la table $table_reference
	 * @param [$table_tmp] : String nom de la table temporaire
	 */
	public function _checkStringExistEtranger($field_name,$table_reference,$field_reference,$table_tmp=null){
		$db = $this->getAdapter();
		if(is_null($table_tmp))
			$table_tmp=$this->table_tmp;

		$sql='UPDATE ' . $this->table_tmp . '
			SET tmp_erreur = CONCAT(tmp_erreur," La valeur du champ '.$field_name.' est introuvable dans la table de référence '.$table_reference.' ")
			WHERE 1 = 1
			AND '.$field_name.' <> "" 
			AND NOT EXISTS (
			 SELECT 1 from '.$table_reference.' 
    			 WHERE translate('.$field_reference.', "áàâäãåçéèêëíìîïñóòôöõúùûüýÿ -.",'. '"aaaaaaceeeeiiiinooooouuuuyy____") 
    			     = 
    			     translate('.$table_tmp.'.'.$field_name.', "áàâäãåçéèêëíìîïñóòôöõúùûüýÿ -.",'. '"aaaaaaceeeeiiiinooooouuuuyy____") 
            ) ';

		$db->query($sql);
	}

	/*
	 * Remplace la valeur du champ $field_name de la table temporaire $table_tmp par l'id contenu dans le champ $field_foreign_primary de la table $table_reference ou $field_name == $field_reference
	 *
	 */
	public function _replaceByAnId($field_name,$field_reference, $field_foreign_primary, $table_reference, $contexte = null, $table_tmp=null){
		$db = $this->getAdapter();
		if(is_null($table_tmp))
			$table_tmp=$this->table_tmp;
                if($table_reference == 'fsn_categorie'){
                    $visible = ' AND visible = 1';
                }  else {
                    $visible = '';
                }

                if($contexte){
                    $contexte_where = ' AND categorie_type = "'.$contexte.'"';
                }
                else {
                    $contexte_where = '' ;
                }
		$sql='UPDATE ' . $this->table_tmp . '
			SET '.$field_name.' = (SELECT '.$field_foreign_primary.' FROM '.$table_reference.'
					WHERE translate('.$table_tmp.'.'.$field_name.', "áàâäãåçéèêëíìîïñóòôöõúùûüýÿ -./",'. '"aaaaaaceeeeiiiinooooouuuuyy____")
							= translate('.$table_reference.'.'.$field_reference.', "áàâäãåçéèêëíìîïñóòôöõúùûüýÿ -./",'. '"aaaaaaceeeeiiiinooooouuuuyy____")'
                        .$contexte_where
                        .$visible.')';

		$db->query($sql);
	}

	public function _replaceByAnIdConteneur($field_name,$field_reference, $field_foreign_primary, $table_reference, $entiteadmin, $table_tmp=null){
		$db = $this->getAdapter();
		if(is_null($table_tmp))
			$table_tmp=$this->table_tmp;

		$sql='UPDATE ' . $this->table_tmp . '
			SET '.$field_name.' = (SELECT '.$field_foreign_primary.' FROM '.$table_reference.'
					WHERE translate('.$table_tmp.'.'.$field_name.', "áàâäãåçéèêëíìîïñóòôöõúùûüýÿ -./",'. '"aaaaaaceeeeiiiinooooouuuuyy____")
							= translate('.$table_reference.'.'.$field_reference.', "áàâäãåçéèêëíìîïñóòôöõúùûüýÿ -./",'. '"aaaaaaceeeeiiiinooooouuuuyy____")
					AND entiteadmin_id='. $entiteadmin .'
					)';

		$db->query($sql);
	}
	/*
	 * Supprime le champ $field de la table $table_tmp
	 */
	public function _delField($field, $table_tmp=null){
		$db = $this->getAdapter();
		if(is_null($table_tmp))
			$table_tmp=$this->table_tmp;

		$sql = "ALTER TABLE " . $table_tmp . " DROP " .$field;
		return $db->query($sql);
	}

//        public function _getErPoterie($field, $table_tmp=null){
//		$db = $this->getAdapter();
//		if(is_null($table_tmp))
//			$table_tmp=$this->table_tmp;
//
//		$sql = "SELECT id FROM " . $table_tmp." WHERE " ;
//		return $db->query($sql);
//	}

	/*
	 * Insert le contenue de la table $table_tmp dans la table $this->table
	 */
	public function insertVraiTable($table_tmp=null){
		$db = $this->getAdapter();
		
		if(is_null($table_tmp))
			$table_tmp=$this->table_tmp;

		$sql="INSERT INTO $this->table SELECT * FROM $this->table_tmp ";
		return $db->query($sql);
	}

	/*
	 * Remplace la valeur du champ $field_reference par l'ID de l'enregistrement de la table $table_reference où $field_name == $field_reference
	 *
	 */
	public function _updateForeignKey($field_name,$field_id,$field_reference,$table_reference,$table_tmp=null){
		$db = $this->getAdapter();
		if(is_null($table_tmp))
			$table_tmp=$this->table_tmp;

		$sql = "UPDATE $table_tmp as tmp
		SET $field_name as field =
			(SELECT $field_id as id
				FROM $table_reference as table
				WHERE translate(LOWER('tmp.field'),'áàâäãåçéèêëíìîïñóòôöõúùûüýÿ -./', 'aaaaaaceeeeiiiinooooouuuuyy____')
					= translate(LOWER('tmp.field'),'áàâäãåçéèêëíìîïñóòôöõúùûüýÿ -./', 'aaaaaaceeeeiiiinooooouuuuyy____')
		)";

	}



	/*
	 * Vérifie que la valeur du champ $field est "oui" ou "non", insensible à la casse;
	 */
	public function _bool($field,$table_tmp=null){
		$db = $this->getAdapter();
		if(is_null($table_tmp))
			$table_tmp=$this->table_tmp;

		$sql =" UPDATE " . $table_tmp . " SET tmp_erreur = CONCAT(tmp_erreur,\" La valeur du champ '.$field.' doit oui ou non \n\") WHERE
		translate(LOWER(".$field."),'áàâäãåçéèêëíìîïñóòôöõúùûüýÿ -./', 'aaaaaaceeeeiiiinooooouuuuyy____') !=
				(translate(LOWER('oui'),'áàâäãåçéèêëíìîïñóòôöõúùûüýÿ -./', 'aaaaaaceeeeiiiinooooouuuuyy____')
				or translate(LOWER('non'),'áàâäãåçéèêëíìîïñóòôöõúùûüýÿ -./', 'aaaaaaceeeeiiiinooooouuuuyy____'))	";
		$db->query($sql);

	}

	/*
	 * Remplace la valeur de $field par 1 s'il vaut "oui" (insensible à la casse) ou par 0 s'il vaut "non" insensible à la casse
	 */
	public function _replacebool($field,$table_tmp=null){
		$db = $this->getAdapter();
		if(is_null($table_tmp))
			$table_tmp=$this->table_tmp;
		$sql =" UPDATE " . $table_tmp . " SET " . $field . " = 1 WHERE
				translate(LOWER('".$field."'),'áàâäãåçéèêëíìîïñóòôöõúùûüýÿ -./', 'aaaaaaceeeeiiiinooooouuuuyy____') =
				translate(LOWER('oui'),'áàâäãåçéèêëíìîïñóòôöõúùûüýÿ -./', 'aaaaaaceeeeiiiinooooouuuuyy____')	";
		$db->query($sql);

		$sql =" UPDATE " . $table_tmp . " SET " . $field . " = 0 WHERE
				translate(LOWER('".$field."'),'áàâäãåçéèêëíìîïñóòôöõúùûüýÿ -./', 'aaaaaaceeeeiiiinooooouuuuyy____') =
				translate(LOWER('non'),'áàâäãåçéèêëíìîïñóòôöõúùûüýÿ -./', 'aaaaaaceeeeiiiinooooouuuuyy____')	";
		$db->query($sql);
	}

	public function _checkLength($field,$size,$table_tmp=null){
		if(is_null($table_tmp))
			$table_tmp=$this->table_tmp;
		$db = $this->getAdapter();
		$sql =" UPDATE " . $table_tmp . "SET tmp_error = CONCAT(tmp_error,\"La valeur du champ " . $field . " doit etre de maximum " . $size . " caractères \n\") WHERE
				LENGTH(".$field.") > " . $size ;
		$db->query($sql);
	}

	public function _checkStringConteneur($field_name,$table_reference,$field_reference,$entiteadmin, $table_tmp=null){
		$db = $this->getAdapter();
		if(is_null($table_tmp))
			$table_tmp=$this->table_tmp;

		$sql='UPDATE ' . $this->table_tmp . '
			SET tmp_erreur = CONCAT(tmp_erreur," La valeur du champ '.$field_name.' est introuvable dans la table de référence '.$table_reference.' \n")
			WHERE
					translate('.$field_name.', "áàâäãåçéèêëíìîïñóòôöõúùûüýÿ -./",'. '"aaaaaaceeeeiiiinooooouuuuyy____") '.
						'NOT IN (SELECT translate('.$field_reference.', "áàâäãåçéèêëíìîïñóòôöõúùûüýÿ -./",'. '"aaaaaaceeeeiiiinooooouuuuyy____") from '.$table_reference.' WHERE entiteadmin_id = ' . $entiteadmin . ')  ';

		$db->query($sql);
	}

	public function deleteChar($char,$field,$table_tmp=null){
		if(is_null($table_tmp))
			$table_tmp=$this->table_tmp;
		$db = $this->getAdapter();

		$sql='UPDATE ' . $this->table_tmp . '
			SET '.$field.' = REPLACE('.$field.',"'.$char.'","")';

		$db->query($sql);

	}

	public function exportCSVImport($table_tmp=null){
		if(is_null($table_tmp))
			$table_tmp=$this->table_tmp;
		$db = $this->getAdapter();
		$sql ="SELECT * FROM $table_tmp";
		$result = $db->prepare($sql);

		$string="";

		foreach ($result as $row){
			foreach ($row as $key => $value)
				$string.='"'.$key.'";';

			$string.="tyty";
			$string = str_replace(";tyty", "\n", $string);
			break;
		}

		foreach ($result as $row){
			foreach ($row as $key => $value)
				$string.='"' .$value . '";';

			$string.="tyty";
			$string = str_replace(";tyty", "\n", $string);
		}
		return $string;
	}



}

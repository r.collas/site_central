<?php

class Model_Trait_Inter extends Yab_Db_Table {

	protected $_name = 'trait_inter';

	public function getTrait() {

		return new Model_Trait($this->get('trait_id'));

	}

	public function getInter() {

		return new Model_Inter($this->get('inter_id'));

	}

}
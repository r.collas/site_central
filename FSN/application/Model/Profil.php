<?php

class Model_Profil extends Yab_Db_Table {

	protected $_name = 'profil';

	public function getProfilIhms() {

		return $this->getTable('Model_Profil_Ihm')->search(array('profil_id' => $this->get('Id')));

	}

	public function getUsers() {

		return $this->getTable('Model_User')->search(array('profil_id' => $this->get('Id')));

	}

}
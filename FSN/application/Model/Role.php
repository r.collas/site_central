<?php

class Model_Role extends Yab_Db_Table {

	protected $_name = 'role';

	public function getOaIntervenants() {

		return $this->getTable('Model_Oa_Intervenant')->search(array('role_id' => $this->get('id')));

	}

}
<?php

class Model_Prod_Precisionprod extends Yab_Db_Table {

	protected $_name = 'prod_precisionprod';

	public function getProd() {

		return new Model_Prod($this->get('prod_id'));

	}

	public function getPrecisionprod() {

		return new Model_Precisionprod($this->get('precisionprod_id'));

	}

}
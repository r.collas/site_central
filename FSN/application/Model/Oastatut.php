<?php

class Model_Oastatut extends Yab_Db_Table {

	protected $_name = 'oastatut';

	public function getNatures() {

		return $this->getTable('Model_Nature')->search(array('oastatut_id' => $this->get('id')));

	}

	public function getOas() {

		return $this->getTable('Model_Oa')->search(array('oastatut_id' => $this->get('id')));

	}

}
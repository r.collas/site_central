<?php

class Model_Dashboard extends Yab_Db_Table {

	//protected $_name = 'dashboard';
	protected $_name = 'oa';

	public function getSitefouille() {

		return new Model_Sitefouille($this->get('sitefouille_id'));

	}

	public function getOastatut() {

		return new Model_Oastatut($this->get('oastatut_id'));

	}

	public function getNature() {

		return new Model_Nature($this->get('nature_id'));

	}

	public function getOaDocs() {

		return $this->getTable('Model_Oa_Doc')->search(array('oa_id' => $this->get('id')));

	}

	public function getOaIntervenants() {

		return $this->getTable('Model_Oa_Intervenant')->search(array('oa_id' => $this->get('id')));

	}

	public function getUsOas() {

		return $this->getTable('Model_Us_Oa')->search(array('oa_id' => $this->get('id')));

	}

}
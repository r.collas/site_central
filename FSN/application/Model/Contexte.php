<?php

class Model_Contexte extends Yab_Db_Table {

	protected $_name = 'contexte';

	public function getUses() {

		return $this->getTable('Model_Us')->search(array('contexte_id' => $this->get('id')));

	}

}
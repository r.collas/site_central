<?php

class Model_Phasechrono extends Yab_Db_Table {

	protected $_name = 'phasechrono';

	public function getEpisodeurbain() {

		return new Model_Episodeurbain($this->get('episodeurbain_id'));

	}

	public function getSitefouille() {

		return new Model_Sitefouille($this->get('sitefouille_id'));

	}

	public function getVisiblePhases($sf_id = null){
		if(!$sf_id)
			$sf_id = Yab_Loader::getInstance()->getSession()->get('sitefouille_id');
		$db = $this->getTable()->getAdapter();
		return $db->prepare("SELECT * FROM phasechrono WHERE sitefouille_id = '".$sf_id."'");
	}
	
	public function getSitefouilleByIdSite($site) {

		$db = $this->getTable()->getAdapter();
		$nom_complet = $db->prepare('SELECT *, id, CONCAT_WS(" - ", nom, nomabrege) AS nom_nomabrege
      								FROM sitefouille where id="'.$site.'"');
		return $nom_complet;

	}

}
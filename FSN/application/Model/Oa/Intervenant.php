<?php

class Model_Oa_Intervenant extends Yab_Db_Table {

	protected $_name = 'oa_intervenant';

	public function getOa() {

		return new Model_Oa($this->get('oa_id'));

	}

	public function getIntervenant() {

		return new Model_Intervenant($this->get('intervenant_id'));

	}

	public function getRole() {

		return new Model_Role($this->get('role_id'));

	}

}
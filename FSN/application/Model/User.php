<?php

class Model_User extends Yab_Db_Table {

	protected $_name = 'user';

	public function getProfil() {

		return new Model_Profil($this->get('profil_id'));

	}

	public function getEntiteadmin() {

		return new Model_Entiteadmin($this->get('entiteadmin_id'));

	}

	public function getIntervenant() {

		return new Model_Intervenant($this->get('intervenant_id'));

	}
  public function getUserDetail(){
    
    $db = $this->getTable()->getAdapter();
    
    $sql = 'SELECT 
      	usr.id AS user_id, 
      	usr.login, 
      	usr.mdp, 
      	usr.nom AS user_nom, 
      	usr.prenom AS user_prenom, 
      	usr.profil_id, 
      	pro.Nomprofil AS profil_nom,
      	usr.entiteadmin_id, 
      	fo.nom AS organisme_nom,
      	usr.intervenant_id
      FROM `user` usr
      INNER JOIN profil pro ON pro.Id = usr.profil_id
      LEFT JOIN fsn_entiteadmin fe ON fe.id = usr.entiteadmin_id
      LEFT JOIN fsn_organisme fo ON fo.id = fe.organisme_id
      LEFT JOIN intervenant inte ON inte.id = usr.intervenant_id
    ';
        
    $result = $db->prepare($sql);
    
    return $result ;
  
  }
  
  	public function getEntiteAdminAll() {
  		$db = $this->getTable()->getAdapter();
  
  		$sql = "SELECT entite.id, entite.description, organisme.nom
				FROM fsn_entiteadmin as entite
				LEFT JOIN fsn_organisme as organisme ON entite.organisme_id=organisme.id
		" ;
  
  		$result = $db->prepare($sql);
  
  		return $result->toArray() ;
  	}

	public function getVisibleUsers($entiteadmin = null){
		$db = $this->getTable()->getAdapter();
		if(!$entiteadmin)
			$entiteadmin = Yab_Loader::getInstance()->getSession()->get('session')['entiteadmin_id'];
		$sql = 'SELECT * FROM user WHERE entiteadmin_id = '.$entiteadmin;

		$result = $db->prepare($sql);

		return $result ;
	}
  
}
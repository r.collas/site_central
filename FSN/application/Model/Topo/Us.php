<?php

class Model_Topo_Us extends Yab_Db_Table {

	protected $_name = 'topo_us';

	public function getUs() {

		return new Model_Us($this->get('us_id'));

	}

	public function getTopo() {

		return new Model_Topo($this->get('topo_id'));

	}
    
    public function getListTopoUs() {
            
        $db = $this->getTable()->getAdapter();
        
        $sql = 'SELECT 
                    topo_us.us_id, 
                    us.identification,
                    topo_us.topo_id,
                    topo.x,
                    topo.y,
                    topo.z
                FROM topo_us
                INNER JOIN us ON us.id = topo_us.us_id
                INNER JOIN topographie topo ON topo.id = topo_us.topo_id         
        ';
        
        $result = $db->prepare($sql);
        
        return $result ;        
        
    }

}
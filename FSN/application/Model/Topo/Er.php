<?php

class Model_Topo_Er extends Yab_Db_Table {

	protected $_name = 'topo_er';

	public function getEr() {

		return new Model_Er($this->get('er_id'));

	}

	public function getTopo() {

		return new Model_Topo($this->get('topo_id'));

	}

}
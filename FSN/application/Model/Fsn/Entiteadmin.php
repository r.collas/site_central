<?php

class Model_Fsn_Entiteadmin extends Yab_Db_Table {

	protected $_name = 'fsn_entiteadmin';

	public function getOrganisme() {

		return new Model_Organisme($this->get('organisme_id'));

	}
  
  public function getEntiteAdministrativeDetail () {
    $db = $this->getTable()->getAdapter();

    $sql = 'SELECT 
      	fe.id, 
      	fe.organisme_id, 
      	fo.nom AS organisme_nom,
      	fe.description AS entiteadmin_description
      FROM fsn_entiteadmin fe
      inner join  fsn_organisme fo ON fo.id = fe.organisme_id';
    
    $result = $db->prepare($sql);
    
    return $result ;
  
  }

}
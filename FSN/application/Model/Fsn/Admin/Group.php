<?php

class Model_Fsn_Admin_Group extends Yab_Db_Table {

	protected $_name = 'fsn_admin_group';

	public function getGroup() {

		return new Model_Group($this->get('group_id'));

	}

	public function getParentGroup() {

		return new Model_Parent_Group($this->get('parent_group_id'));

	}

}
<?php
class Model_Fsn_Historique extends Yab_Db_Table {
	
	protected $_name = 'fsn_historique';
	
	public function getHistorique() {
		return new Model_Fsn_Historique ( $this->get ( 'id' ) );
	}
	
	public function listHistorique($categorie_object = null, $type_action = null, $date_historique = null, $login = null, $data_modified = null) {
		$db = $this->getAdapter ();
        $sql = 'SELECT 
            id, 
            categorie_object, 
            login, 
            type_action, 
            date_historique,
            cle_metier, 
            data_modified, 
            description 
          FROM fsn_historique 
          ORDER BY date_historique desc, id desc
        '; 
		$result = $db->prepare ($sql);
		if (! empty ( $categorie_object ))
			$result->where ( 'UPPER(categorie_object)=UPPER(:module)' )->bind ( ':module', $categorie_object );
		if (! empty ( $type_action ))
			$result->where ( 'UPPER(type_action)=UPPER(:typeaction)' )->bind ( ':typeaction', $type_action );
		if (! empty ( $date_historique ))
			$result->where ( 'date_historique=:date_modify' )->bind ( ':date_modify', $date_historique );
		if (! empty ( $login ))
			$result->where ( 'UPPER(login)=UPPER(:login_modify)' )->bind ( ':login_modify', $login );
		if (! empty ( $data_modified ))
			$result->where ( "UPPER(data_modified) LIKE UPPER( '%".$data_modified."%' )" );
		return $result;
	}
	
	public function listHistoriqueDelta($date_historique, $object) {
		$db = $this->getAdapter();
		$result = $db->prepare ( 'SELECT id, categorie_object, login, type_action, date_historique,cle_metier, data_modified, description FROM fsn_historique ORDER BY date_historique desc, id desc');
		if (! empty($date_historique))
			$result->where('date_historique>=:date_modify' )->bind ( ':date_modify', $date_historique);
		if (!empty($object))
			$result->where('categorie_object=:categorie_object' )->bind(':categorie_object', $object);
		return $result;
	}
	
	public function foreignKey($table,$field_primary, $value,$field_foreign){
		$db = $this->getAdapter();
		$sql = "SELECT $field_foreign FROM $table WHERE $field_primary='$value'";
		return $db->prepare($sql);
	}

}

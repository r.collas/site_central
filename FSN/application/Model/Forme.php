<?php

class Model_Forme extends Yab_Db_Table {

	protected $_name = 'forme';

	public function getElementpoteries() {

		return $this->getTable('Model_Elementpoterie')->search(array('forme_id' => $this->get('id')));

	}

}
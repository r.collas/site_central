<?php

class Model_Sptype extends Yab_Db_Table {

	protected $_name = 'sptype';

	public function getSuperpositions() {

		return $this->getTable('Model_Superposition')->search(array('sptype_id' => $this->get('id')));

	}

	public function getVisibleSptype(){
		$db = $this->getTable()->getAdapter();
		$loader = Yab_Loader::getInstance();

		$sql = 'SELECT * FROM sptype WHERE visible = 1';

		$result = $db->prepare($sql);

		return $result ;
	}

}
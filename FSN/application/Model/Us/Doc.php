<?php

class Model_Us_Doc extends Yab_Db_Table {

	protected $_name = 'us_doc';

	public function getUs() {

		return new Model_Us($this->get('us_id'));

	}

	public function getDoc() {

		return new Model_Doc($this->get('doc_id'));

	}

    public function listUsDoc($us_id=null)
    {
        $db = $this->getTable()->getAdapter();
        
        $sql = 'SELECT 
            us_id, 
            us.identification,
            doc_id,
            doc.copyright,
            doc.datecreation,
            doc.chemin,
            doc.commentaire,
            doc.type_doc,
            doc.topo_id
        FROM us_doc
        INNER JOIN us ON us.id = us_doc.us_id
        INNER JOIN document doc ON doc.id = us_doc.doc_id        
        ';
        
        $result = $db->prepare($sql);
        
        return $result ;
    }
}
<?php

class Model_Intervenant extends Yab_Db_Table {

	protected $_name = 'intervenant';

	public function getExtorganisme() {

		return new Model_Fsn_Organisme($this->get('organisme_id'));

	}

	public function getMetier() {

		return new Model_Metier($this->get('metier_id'));

	}

	public function getElementrecueillis() {

		return $this->getTable('Model_Elementrecueilli')->search(array('intervenant_id' => $this->get('id')));

	}

	public function getOaIntervenants() {

		return $this->getTable('Model_Oa_Intervenant')->search(array('intervenant_id' => $this->get('id')));

	}

	public function getTraitements() {

		return $this->getTable('Model_Traitement')->search(array('intervenant_id' => $this->get('id')));

	}

	public function getUsIntervenants() {

		return $this->getTable('Model_Us_Intervenant')->search(array('intervenant_id' => $this->get('id')));

	}
  
	public function getSitefouilleIntervenants() {
  
    $db = $this->getTable()->getAdapter();
    $loader = Yab_Loader::getInstance();
    $session = $loader->getSession();
    
    $sql = 'SELECT 
      	inte.id, 
      	inte.titre, 
      	inte.nom, 
      	inte.prenom, 
      	inte.adresse, 
      	inte.cp, 
      	inte.ville, 
      	inte.telephone, 
      	inte.email, 
      	inte.organisme_id, 
      	inte.commentaire, 
      	inte.metier_id
      	
      FROM intervenant inte 
      LEFT JOIN intervention_us intus ON intus.intervenant_id = inte.id
      LEFT JOIN us ON us.id = intus.us_id
      LEFT JOIN sitefouille sf ON sf.id = us.sitefouille_id
    ';
    
    $result = $db->prepare($sql);
    
    return $result;	
  
  
  }

	public function getListIntervenants() {
  
		$db = $this->getTable()->getAdapter();
		$loader = Yab_Loader::getInstance();
		$session = $loader->getSession();

		$sql = 'SELECT
			-- sf.nom AS sitefouille_nom,
			-- sf.nomabrege AS sitefouille_nomabrege ,
			inte.id,
			inte.titre,
			concat(inte.prenom, " ", upper(inte.nom) ) AS nom_complet,
			inte.nom,
			inte.prenom,
			inte.adresse,
			inte.cp,
			inte.ville,
			inte.telephone,
			inte.email,
			inte.organisme_id,
			fo.nom AS organisme_nom,
			inte.commentaire,
			inte.metier_id,
			metier.metier
		  FROM intervenant inte
		  LEFT JOIN fsn_organisme fo ON fo.id = inte.organisme_id
		  LEFT JOIN metier ON metier.id = inte.metier_id
		  -- LEFT JOIN fsn_categorie fsnc ON fsnc.id = inte.metier_id AND fsnc.categorie_type = "intervenant_metier"
		  LEFT JOIN intervention_us intus ON intus.intervenant_id = inte.id
		  LEFT JOIN us ON us.id = intus.us_id
		  -- LEFT JOIN sitefouille sf ON sf.id = us.sitefouille_id
		  GROUP BY
			-- sf.nom,
			-- sf.nomabrege,
			inte.id,
			inte.titre,
			inte.nom
		';
    
    	$result = $db->prepare($sql);
		return $result;
  
  }

  public function getListIntervenantsIndex() { // jfb 2016-06-06 mantis 285
  
  	$db = $this->getTable()->getAdapter();
  	$loader = Yab_Loader::getInstance();
  	$session = $loader->getSession();
  
  	$sql = 'SELECT
			 inte.id             as id
			,inte.nom            as nom
			,inte.prenom         as prenom
			,metier
  			,inte.adresse        as adresse
			,inte.email          as email
			,inte.telephone      as telephone
		  FROM intervenant inte
		  LEFT JOIN metier ON metier.id = inte.metier_id
		  ';
  
  	$result = $db->prepare($sql);
  	return $result;
  
  }
  
  
	public function getAllNomsComplets($organisme=null){
		$db = $this->getTable()->getAdapter();
		$entiteadmin_session = Yab_Loader::getInstance()->getsession()->get('session')['entiteadmin_id'];
		if(!$organisme && $entiteadmin_session) {
			$organisme = "(SELECT org.id
						FROM fsn_entiteadmin ea
						INNER JOIN fsn_organisme org ON org.id = ea.organisme_id
						WHERE ea.id = '".$entiteadmin_session."')";
		}else
			$organisme = "'".$organisme."'";
		$sql = "SELECT
					interv.id,
					CONCAT_WS(' ', interv.nom, interv.prenom) AS nom_complet
					FROM intervenant interv
					WHERE interv.organisme_id = ".$organisme . " order by interv.nom";
		$nom_complet = $db->prepare($sql);
		return $nom_complet;
	}

	public function getAllNoms(){
		$db = $this->getTable()->getAdapter();
		$entiteadmin_session = Yab_Loader::getInstance()->getsession()->get('session')['entiteadmin_id'];
		
		$sql = "SELECT
					interv.id,
					CONCAT_WS(' ', interv.nom, interv.prenom) AS nom_complet
					FROM intervenant interv";
		$nom_complet = $db->prepare($sql);
		return $nom_complet;
	}
	
	public function getVisibleIntervenants(){
		$db = $this->getTable()->getAdapter();
		$entiteadmin_session = Yab_Loader::getInstance()->getsession()->get('session')['entiteadmin_id'];
		$sql = "SELECT *
					FROM intervenant
					WHERE organisme_id = (SELECT org.id
						FROM fsn_entiteadmin ea
						INNER JOIN fsn_organisme org ON org.id = ea.organisme_id
						WHERE ea.id = '".$entiteadmin_session."')";
		$nom_complet = $db->prepare($sql);
		return $nom_complet;
	}

	public function getIntervenantsActifs($sf = null){
		$db = $this->getTable()->getAdapter();
		if(!$sf)
			$sf = Yab_Loader::getInstance()->getsession()->get('sitefouille_id');
		$sql = "SELECT DISTINCT i.nom, i.prenom, i.id
					FROM intervenant i
					INNER JOIN intervention_us iu ON iu.intervenant_id = i.id
					INNER JOIN us ON us.id = iu.us_id
					WHERE us.sitefouille_id = '".$sf."'";
		return $db->prepare($sql);

	}


}
<?php

class Model_Consistance extends Yab_Db_Table {

	protected $_name = 'consistance';

	public function getUses() {

		return $this->getTable('Model_Us')->search(array('consistance_id' => $this->get('id')));

	}

}
<?php

class Model_Intervention_Us extends Yab_Db_Table {

	protected $_name = 'intervention_us';

	public function getIntervenant() {

		return new Model_Intervenant($this->get('intervenant_id'));

	}

	public function getUs() {

		return new Model_Us($this->get('us_id'));

	}

	public function getType() {

		return new Model_Type($this->get('type_id'));

	}

    public function getInterventions($us_id){
        $db = $this->getTable()->getAdapter();

        $sql = "SELECT
		inter.us_id,
		inter.debut,
		inter.fin,
		inter.intervenant_id,
		inter.type_id,
        typeint.typeinterv,
        CONCAT(inte.prenom,' ', upper(inte.nom) ) AS nom_complet
		FROM intervention_us inter
		 INNER JOIN us ON us.id = inter.us_id
		 INNER JOIN intervenant inte ON inte.id = inter.intervenant_id
		 LEFT JOIN typeintervention typeint ON typeint.id = inter.type_id
		WHERE us_id = '".$us_id."'";

        $result = $db->prepare($sql);

        return $result ;
    }

    public function getInterventionsHisto($us_id){ // jfb 2016-04-29 mantis 250
    	$db = $this->getTable()->getAdapter();
    
    	$sql = "SELECT
    	    us_id
		   ,intervenant_id
		   ,type_id
		   ,DATE_FORMAT(debut,'%d/%m/%Y') as debut
		   ,DATE_FORMAT(fin,'%d/%m/%Y') as fin
    	   ,id
        FROM intervention_us
		WHERE us_id = '".$us_id."'";
    
    	$result = $db->prepare($sql);
    
    	return $result ;
    }
    
	public function deleteRelations($us_id){
		$this->delete( "us_id = '".$us_id."'");
	}

}
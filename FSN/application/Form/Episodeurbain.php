<?php

class Form_Episodeurbain extends Yab_Form {

	public function __construct(Model_Episodeurbain $episodeurbain) {

		$this->set('method', 'post')->set('name', 'form_episodeurbain')->set('action', '')->set('role', 'form')->set('class', 'form-horizontal');

		$this->setElement('identification', array(
			'type' => 'text',
			'id' => 'identification',
			'label' => 'identification',
			'value' => $episodeurbain->has('identification') ? $episodeurbain->get('identification') : null,
			'validators' => array('NotEmpty'),
			'errors' => array(),
		));

		$this->setElement('description', array(
			'type' => 'textarea',
      'rows' => '3',
			'id' => 'description',
			'label' => 'description',
			'value' => $episodeurbain->has('description') ? $episodeurbain->get('description') : null,
			'validators' => array(),
			'errors' => array(),
		));

		$this->setElement('datationdebutbasse', array(
			'type' => 'text',
			'id' => 'datationdebutbasse',
			'label' => 'datationdebutbasse',
      'class' => 'datepicker',
			'value' => $episodeurbain->has('datationdebutbasse') ? $episodeurbain->get('datationdebutbasse') : null,
			'validators' => array(),
			'errors' => array(),
		));

		$this->setElement('datationdebuthaute', array(
			'type' => 'text',
			'id' => 'datationdebuthaute',
			'label' => 'datationdebuthaute',
      'class' => 'datepicker',
			'value' => $episodeurbain->has('datationdebuthaute') ? $episodeurbain->get('datationdebuthaute') : null,
			'validators' => array(),
			'errors' => array(),
		));

		$this->setElement('datationfinbasse', array(
			'type' => 'text',
			'id' => 'datationfinbasse',
			'label' => 'datationfinbasse',
      'class' => 'datepicker',
			'value' => $episodeurbain->has('datationfinbasse') ? $episodeurbain->get('datationfinbasse') : null,
			'validators' => array(),
			'errors' => array(),
		));

		$this->setElement('datationfinhaute', array(
			'type' => 'text',
			'id' => 'datationfinhaute',
			'label' => 'datationfinhaute',
      'class' => 'datepicker',
			'value' => $episodeurbain->has('datationfinhaute') ? $episodeurbain->get('datationfinhaute') : null,
			'validators' => array(),
			'errors' => array(),
		));

	}

}
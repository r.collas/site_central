<?php

class Form_Us_Intervenant extends Yab_Form {

	public function __construct(Model_Us_Intervenant $us_intervenant) {

		$this->set('method', 'post')->set('name', 'form_us_intervenant')->set('action', '')->set('role', 'form')->set('class', 'form-horizontal');

		$this->setElement('debut', array(
			'type' => 'text',
			'id' => 'debut',
			'label' => 'debut',
      'class' => 'datepicker',
			'value' => $us_intervenant->has('debut') ? $us_intervenant->get('debut') : null,
			'validators' => array('NotEmpty'),
			'errors' => array(),
		));

		$this->setElement('fin', array(
			'type' => 'text',
			'id' => 'fin',
			'label' => 'fin',
      'class' => 'datepicker',
			'value' => $us_intervenant->has('fin') ? $us_intervenant->get('fin') : null,
			'validators' => array('NotEmpty'),
			'errors' => array(),
		));

	}

}
<?php

class Form_Croquis extends Yab_Form {

	public function __construct(Model_Croquis $croquis) {

		$this->set('method', 'post')->set('action', '')->set('role', 'form');

		$this->setElement('file_name', array(
			'type' => 'text',
			'id' => 'file_name',
			'label' => 'file_name',
			'value' => $croquis->has('file_name') ? $croquis->get('file_name') : null,
			'validators' => array(),
			'errors' => array(),
		));

		$this->setElement('image', array(
			'type' => 'text',
			'id' => 'image',
			'label' => 'image',
			'value' => $croquis->has('image') ? $croquis->get('image') : null,
			'validators' => array(),
			'errors' => array(),
		));

		$this->setElement('svg', array(
			'type' => 'textarea',
			'id' => 'svg',
			'label' => 'svg',
			'value' => $croquis->has('svg') ? $croquis->get('svg') : null,
			'validators' => array(),
			'errors' => array(),
		));

		$this->setElement('vg', array(
			'type' => 'textarea',
			'id' => 'vg',
			'label' => 'vg',
			'value' => $croquis->has('vg') ? $croquis->get('vg') : null,
			'validators' => array(),
			'errors' => array(),
		));

	}

}
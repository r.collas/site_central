<?php

class Form_Phasechrono extends Yab_Form {

	public function __construct(Model_Phasechrono $phasechrono) {
		
		// appel fichier internationalisation
		$registry = Yab_Loader::getInstance() -> getRegistry();
        $i18n = $registry -> get('i18n');
        $filter_no_html = new Yab_Filter_NoHtml();
		
		$this->set('method', 'post')->set('name', 'form_phasechrono')->set('action', '')->set('role', 'form');		
		
		$session = Yab_Loader::getInstance()->getSession();
		$sf = new Model_Sitefouille($phasechrono->has('sitefouille_id') ? $phasechrono->get('sitefouille_id') : $session->get('sitefouille_id'));
		
		$this->setElement('sitefouille_id', array(
			'type' => 'hidden',
			'id' => 'sitefouille_id',
			'label' => $filter_no_html->filter( $i18n -> say('sitefouille_id') ),
			'value' => $sf->get('id'),
			'intitule' => $sf->get('nomabrege')." - ".$sf->get('nom'),
			'validators' => array('NotEmpty'),
			'readonly' => true,
			'errors' => array(),
		));
		
		$this->setElement('identification', array(
			'type' => 'text',
			'id' => 'identification',
			'label' => $filter_no_html->filter( $i18n -> say('phase') ),
			'placeholder' => $filter_no_html->filter( $i18n -> say('phase') ),
			'value' => $phasechrono->has('identification') ? $phasechrono->get('identification') : null,
			'validators' => array('NotEmpty'),
			'errors' => array(),
		));

		$this->setElement('description', array(
			'type' => 'textarea',
			'rows' => '3',
			'id' => 'description',
			'label' => $filter_no_html->filter( $i18n -> say('description') ),
			'placeholder' => $filter_no_html->filter( $i18n -> say('description') ),
			'value' => $phasechrono->has('description') ? $phasechrono->get('description') : null,
			'validators' => array(),
			'errors' => array(),
		));

		$this->setElement('datatdebbas_aaaa', array(
			'type' => 'text',
			'id' => 'datatdebbas_aaaa',
			'label' => $filter_no_html->filter( $i18n -> say('datatdebbas_aaaa') ),
			'placeholder' => 2000,
			'value' => $phasechrono->has('datatdebbas_aaaa') ? $phasechrono->get('datatdebbas_aaaa') : null,
			'needed' => true,
			'validators' => array('Int', 'NotEmpty'),
			'errors' => array(),
		));

		$this->setElement('datatdebhaut_aaaa', array(
			'type' => 'text',
			'id' => 'datatdebhaut_aaaa',
			'label' => $filter_no_html->filter( $i18n -> say('datatdebhaut_aaaa') ),
			'placeholder' => 2000,
			'value' => $phasechrono->has('datatdebhaut_aaaa') ? $phasechrono->get('datatdebhaut_aaaa') : null,
			'needed' => true,
			'validators' => array('Int', 'NotEmpty'),
			'errors' => array(),
		));

		$this->setElement('datatfinbas_aaaa', array(
			'type' => 'text',
			'id' => 'datatfinbas_aaaa',
			'label' => $filter_no_html->filter( $i18n -> say('datatfinbas_aaaa') ),
			'placeholder' => 2000,
			'value' => $phasechrono->has('datatfinbas_aaaa') ? $phasechrono->get('datatfinbas_aaaa') : null,
			'needed' => true,
			'validators' => array('Int', 'NotEmpty'),
			'errors' => array(),
		));

		$this->setElement('datatfinhaut_aaaa', array(
			'type' => 'text',
			'id' => 'datatfinhaut_aaaa',
			'label' => $filter_no_html->filter( $i18n -> say('datatfinhaut_aaaa') ),
			'placeholder' => 2000,
			'value' => $phasechrono->has('datatfinhaut_aaaa') ? $phasechrono->get('datatfinhaut_aaaa') : null,
			'needed' => true,
			'validators' => array('Int', 'NotEmpty'),
			'errors' => array(),
		));

		// $this->setElement('episodeurbain_id', array(
			// 'type' => 'select',
			// 'id' => 'episodeurbain_id',
			// 'label' => 'episodeurbain_id',
			// 'value' => $phasechrono->has('episodeurbain_id') ? $phasechrono->get('episodeurbain_id') : null,
			// 'fake_options' => array(),
			// 'options' => $phasechrono->getTable('Model_Episodeurbain')->fetchAll()->setKey('id')->setValue('identification'),
			// 'errors' => array(),
		// ));		

		$this->setElement('episode_urbain', array(
			'type' => 'textarea',
			'rows' => '3',
			'id' => 'episode_urbain',
			'label' => $filter_no_html->filter( $i18n -> say('episode_urbain') ),
			'placeholder' => $filter_no_html->filter( $i18n -> say('episode_urbain') ),
			'value' => $phasechrono->has('episode_urbain') ? $phasechrono->get('episode_urbain') : null,
			'validators' => array(),
			'errors' => array(),
		));

	}

}
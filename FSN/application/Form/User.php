<?php

class Form_User extends Yab_Form {

	public function __construct(Model_User $user) {
	    
        $registry = Yab_Loader::getInstance()->getRegistry();
        // appel fichier internationalisation
        $i18n = $registry -> get('i18n');
        $filter_no_html = new Yab_Filter_NoHtml();

		$this->set('method', 'post')->set('name', 'form_user')->set('action', '')->set('role', 'form');

				
		$this->setElement('login', array(
			'type' => 'text',
			'id' => 'login',
			'label' => $filter_no_html->filter( $i18n -> say('login')),
			'placeholder' => $filter_no_html->filter( $i18n -> say('login')),
			'value' => $user->has('login') ? $user->get('login') : null,
			'validators' => array('NotEmpty'),
			'errors' => array(),
			'needed' => true,
		));

		$this->setElement('mdp', array(
			'type' => 'password',
			'id' => 'mdp',
			'label' => $filter_no_html->filter( $i18n -> say('mdp')),
            'placeholder' => $filter_no_html->filter( $i18n -> say('mdp')),
			'value' => $user->has('mdp') ? $user->get('mdp') : null,
			'validators' => array('NotEmpty'),
			'errors' => array(),
			'needed' => true,
		));

		$this->setElement('nom', array(
			'type' => 'text',
			'id' => 'nom',
			'label' => $filter_no_html->filter( $i18n -> say('nom')),
            'placeholder' => $filter_no_html->filter( $i18n -> say('nom')),
			'value' => $user->has('nom') ? $user->get('nom') : null,
			'validators' => array(),
			'errors' => array(),
		));

		$this->setElement('prenom', array(
			'type' => 'text',
			'id' => 'prenom',
			'label' => $filter_no_html->filter( $i18n -> say('prenom')),
            'placeholder' => $filter_no_html->filter( $i18n -> say('prenom')),
			'value' => $user->has('prenom') ? $user->get('prenom') : null,
			'validators' => array(),
			'errors' => array(),
		));

		$this->setElement('profil_id', array(
			'type' => 'select',
			'id' => 'profil_id',
			'label' => $filter_no_html->filter( $i18n -> say('profil_id')),
            'placeholder' => $filter_no_html->filter( $i18n -> say('profil_id')),
			'value' => $user->has('profil_id') ? $user->get('profil_id') : null,
			'fake_options' => array(),
			'options' => $user->getTable('Model_Profil')->fetchAll()->setKey('Id')->setValue('Nomprofil'),
			'validators' => array('NotEmpty'),
			'errors' => array(),
			'needed' => true,
		));
    
		// $this->setElement('entiteadmin_id', array(
			// 'type' => 'select',
			// 'id' => 'entiteadmin_id',
			// 'label' => $filter_no_html->filter( $i18n -> say('entiteadmin_id')),
			// 'placeholder' => $filter_no_html->filter( $i18n -> say('entiteadmin_id')),
			// 'value' => $user->has('entiteadmin_id') ? $user->get('entiteadmin_id') : null,
			// 'fake_options' => array(),
			// 'options' => $user->getTable('Model_Fsn_Entiteadmin')->getEntiteAdministrativeDetail()->setKey('id')->setValue('organisme_nom'),
			// 'validators' => array('NotEmpty'),
			// 'errors' => array(),
			// 'needed' => true,
		// ));

		
		$this->setElement('intervenant_id', array(
			'type' => 'select',
			'id' => 'intervenant_id',
			'label' => $filter_no_html->filter( $i18n -> say('intervenant_id')),
			'placeholder' => $filter_no_html->filter( $i18n -> say('intervenant_id')),
			'value' => $user->has('intervenant_id') ? $user->get('intervenant_id') : null,
			'fake_options' => array('' => ''),
			'options' => $user->getTable('Model_Intervenant')->getListIntervenants()->setKey('id')->setValue('nom_complet'),
			'validators' => array('NotEmpty'),
			'errors' => array(),
			'needed' => true,
		));  
    
	}
}
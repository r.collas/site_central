<?php

class Form_Document extends Yab_Form {

	public function __construct(Model_Document $document) {

		$this->set('method', 'post')->set('name', 'form_document)')->set('action', '')->set('role', 'form');

		$this->setElement('copyright', array(
			'type' => 'text',
			'id' => 'copyright',
			'label' => 'copyright',
			'value' => $document->has('copyright') ? $document->get('copyright') : 'UASD',
			'validators' => array(),
			'errors' => array(),
		));

		$this->setElement('datecreation', array(
			'type' => 'text',
			'id' => 'datecreation',
			'label' => 'date de creation',
      'class' => 'datepicker',
			'value' => $document->has('datecreation') ? $document->get('datecreation') :  date("Y-m-d H:i:s"),
			'validators' => array(),
			'errors' => array(),
		));

		$this->setElement('chemin', array(
			'type' => 'text',
			'id' => 'chemin',
			'label' => 'chemin',
			'value' => $document->has('chemin') ? $document->get('chemin') : null,
			'validators' => array(),
			'errors' => array(),
		));

		$this->setElement('commentaire', array(
			'type' => 'textarea',
      'rows' => '3',
			'id' => 'commentaire',
			'label' => 'commentaire',
			'value' => $document->has('commentaire') ? $document->get('commentaire') : null,
			'validators' => array(),
			'errors' => array(),
		));

		$this->setElement('type_doc', array(
			'type' => 'text',
			'id' => 'type_doc',
			'label' => 'type_doc',
			'value' => $document->has('type_doc') ? $document->get('type_doc') : null,
			'validators' => array('NotEmpty'),
			'errors' => array(),
		));

	}

}
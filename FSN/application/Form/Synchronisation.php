<?php

class Form_Synchronisation extends Yab_Form {

	public function __construct(Model_Synchronisation $synchronisation, $sf_id = null) {

		$this->set('method', 'post')->set('name', 'form_synchronisation')->set('action', '')->set('role', 'form')->set('class', 'form-horizontal');

		if(!$sf_id)
			$sf_id = Yab_Loader::getInstance()->getSession()->get('sitefouille_id');
		$us = $synchronisation->getTable('Model_Us')->getAllUsFromSf($sf_id)->setKey('id')->setValue('identification');

		$this->setElement('synchro1_id', array(
			'type' => 'text',
			'id' => 'synchro1_id',
			'label' => 'US Synchrone',
			'value' => null,
			'options' => $us,
			'validators' => array('NotEmpty'),
			'errors' => array(),
		));

		$this->setElement('synchro2_id', array(
			'type' => 'text',
			'id' => 'synchro2_id',
			'label' => 'US Synchrone',
			'value' => null,
			'options' => $us,
			'validators' => array('NotEmpty'),
			'errors' => array(),
		));

		$this->setElement('incertitude', array(
			'type' => 'text',
			'id' => 'incertitude',
			'label' => 'incertitude',
			'value' => $synchronisation->has('incertitude') ? $synchronisation->get('incertitude') : null,
			'validators' => array(),
			'errors' => array(),
		));

		$this->setElement('synchrotype_id', array(
			'type' => 'select',
			'id' => 'synchrotype_id',
			'label' => 'Type',
			'value' => $synchronisation->has('synchrotype_id') ? $synchronisation->get('synchrotype_id') : null,
			'fake_options' => array(null => 'Non déterminé'),
			'needed' => false,
			'options' => $synchronisation->getTable('Model_Synchrotype')->getVisibleSynchrotype()->setKey('id')->setValue('synchrotype'),
			'errors' => array(),
		));

		$this->setElement('observation', array(
			'type' => 'text',
			'id' => 'observation',
			'label' => 'observation',
			'value' => $synchronisation->has('observation') ? $synchronisation->get('observation') : null,
			'validators' => array(),
			'errors' => array(),
			'maxlength' => 100
		));

	}

}
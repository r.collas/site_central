<?php

class Form_Ihm extends Yab_Form {

	public function __construct(Model_Ihm $ihm) {

		$this->set('method', 'post')->set('name', 'form_ihm')->set('action', '')->set('role', 'form');

		$this->setElement('NomIHM', array(
			'type' => 'text',
			'id' => 'NomIHM',
			'label' => 'NomIHM',
			'value' => $ihm->has('NomIHM') ? $ihm->get('NomIHM') : null,
			'validators' => array('NotEmpty'),
			'errors' => array(),
		));

		$this->setElement('Description', array(
			'type' => 'text',
			'id' => 'Description',
			'label' => 'Description',
			'value' => $ihm->has('Description') ? $ihm->get('Description') : null,
			'validators' => array(),
			'errors' => array(),
		));

	}

}
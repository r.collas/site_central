<?php

class Form_Methodefouille extends Yab_Form {

	public function __construct(Model_Methodefouille $methodefouille) {

		$this->set('method', 'post')->set('name', 'form_methodefouille')->set('action', '')->set('role', 'form')->set('class', 'form-horizontal');

		$this->setElement('methodefouille', array(
			'type' => 'text',
			'id' => 'methodefouille',
			'label' => 'methodefouille',
			'value' => $methodefouille->has('methodefouille') ? $methodefouille->get('methodefouille') : null,
			'validators' => array('NotEmpty'),
			'errors' => array(),
		));

		$this->setElement('visible', array(
			'type' => 'text',
			'id' => 'visible',
			'label' => 'visible',
			'value' => $methodefouille->has('visible') ? $methodefouille->get('visible') : '1',
			'validators' => array('Int', 'NotEmpty'),
			'errors' => array(),
		));

	}

}
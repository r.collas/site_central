<?php

class Form_Inclusion extends Yab_Form {

	public function __construct(Model_Inclusion $inclusion, $sf_id = null) {

		$this->set('method', 'post')->set('name', 'form_inclusion')->set('action', '')->set('role', 'form')->set('class', 'form-horizontal');

		if(!$sf_id)
			$sf_id = Yab_Loader::getInstance()->getSession()->get('sitefouille_id');
		$us = $inclusion->getTable('Model_Us')->getAllUsFromSf($sf_id)->setKey('id')->setValue('identification');

		$this->setElement('inclusion1_id', array(
			'type' => 'text',
			'id' => 'inclusion1_id',
			'label' => 'US Incluante',
			'value' => null,
			'options' => $us,
			'validators' => array('NotEmpty'),
			'errors' => array(),
		));

		$this->setElement('inclusion2_id', array(
			'type' => 'text',
			'id' => 'inclusion2_id',
			'label' => 'US Inclus',
			'value' => null,
			'options' => $us,
			'validators' => array('NotEmpty'),
			'errors' => array(),
		));

	}

}
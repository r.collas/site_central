<?php

class Form_Synchrotype extends Yab_Form {

	public function __construct(Model_Synchrotype $synchrotype) {

		$this->set('method', 'post')->set('name', 'form_synchrotype')->set('action', '')->set('role', 'form')->set('class', 'form-horizontal');

		$this->setElement('synchrotype', array(
			'type' => 'text',
			'id' => 'synchrotype',
			'label' => 'synchrotype',
			'value' => $synchrotype->has('synchrotype') ? $synchrotype->get('synchrotype') : null,
			'validators' => array('NotEmpty'),
			'errors' => array(),
		));

		$this->setElement('visible', array(
			'type' => 'text',
			'id' => 'visible',
			'label' => 'visible',
			'value' => $synchrotype->has('visible') ? $synchrotype->get('visible') : '1',
			'validators' => array('Int', 'NotEmpty'),
			'errors' => array(),
		));

	}

}
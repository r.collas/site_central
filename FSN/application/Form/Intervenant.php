<?php

class Form_Intervenant extends Yab_Form {

	public function __construct(Model_Intervenant $intervenant) {
		
		// appel fichier internationalisation
		$registry = Yab_Loader::getInstance() -> getRegistry();
        $i18n = $registry -> get('i18n');
        $filter_no_html = new Yab_Filter_NoHtml();
		
		$this->set('method', 'post')->set('name', 'form_intervenant')->set('action', '')->set('role', 'form')->set('class', 'form-horizontal');

		$this->setElement('titre', array(
			'type' => 'text',
			'id' => 'titre',
			'label' => $filter_no_html->filter( $i18n -> say('titre_intervenant') ),
			'placeholder' => $filter_no_html->filter( $i18n -> say('titre_intervenant') ),
			'value' => $intervenant->has('titre') ? $intervenant->get('titre') : null,
			'validators' => array(),
			'errors' => array(),
		));

		$this->setElement('nom', array(
			'type' => 'text',
			'id' => 'nom',
			'label' => $filter_no_html->filter( $i18n -> say('nom_intervenant') ),
			'placeholder' => $filter_no_html->filter( $i18n -> say('nom_intervenant') ),
			'value' => $intervenant->has('nom') ? $intervenant->get('nom') : null,
			'validators' => array('NotEmpty'),
			'errors' => array(),
		));

		$this->setElement('prenom', array(
			'type' => 'text',
			'id' => 'prenom',
			'label' => $filter_no_html->filter( $i18n -> say('prenom_intervenant') ),
			'placeholder' => $filter_no_html->filter( $i18n -> say('prenom_intervenant') ),
			'value' => $intervenant->has('prenom') ? $intervenant->get('prenom') : null,
			'validators' => array(),
			'errors' => array(),
		));

		$this->setElement('adresse', array(
			'type' => 'text',
			'id' => 'adresse',
			'label' => $filter_no_html->filter( $i18n -> say('adresse_intervenant') ),
			'placeholder' => $filter_no_html->filter( $i18n -> say('adresse_intervenant') ),
			'value' => $intervenant->has('adresse') ? $intervenant->get('adresse') : null,
			'validators' => array(),
			'errors' => array(),
		));

		$this->setElement('cp', array(
			'type' => 'text',
			'id' => 'cp',
			'label' => $filter_no_html->filter( $i18n -> say('cp_intervenant') ),
			'placeholder' => '75001',
			'value' => $intervenant->has('cp') ? $intervenant->get('cp') : null,
			'validators' => array('Int'),
			'errors' => array(),
		));

		$this->setElement('ville', array(
			'type' => 'text',
			'id' => 'ville',
			'label' => $filter_no_html->filter( $i18n -> say('ville_intervenant') ),
			'placeholder' => $filter_no_html->filter( $i18n -> say('ville_intervenant') ),
			'value' => $intervenant->has('ville') ? $intervenant->get('ville') : null,
			'validators' => array(),
			'errors' => array(),
		));

		$this->setElement('telephone', array(
			'type' => 'text',
			'id' => 'telephone',
			'label' => $filter_no_html->filter( $i18n -> say('telephone_intervenant') ),
			'placeholder' => '0101010101',
			'value' => $intervenant->has('telephone') ? $intervenant->get('telephone') : null,
			'validators' => array(),
			'errors' => array(),
		));

		$this->setElement('email', array(
			'type' => 'text',
			'id' => 'email',
			'label' => $filter_no_html->filter( $i18n -> say('email_intervenant') ),
			'placeholder' => 'email@domain.fr',
			'value' => $intervenant->has('email') ? $intervenant->get('email') : null,
			'validators' => array('Email'),
			'errors' => array(),
		));

		$this->setElement('organisme_id', array(
			'type' => 'select',
			'id' => 'organisme_id',
			'label' =>	$filter_no_html->filter( $i18n -> say('organisme_id_intervenant') ),
			'placeholder' => $filter_no_html->filter( $i18n -> say('organisme_id_intervenant') ),
			'value' => $intervenant->has('organisme_id') ? $intervenant->get('organisme_id') : null,
			'fake_options' => array(),
			'options' => $intervenant->getTable('Model_Fsn_Organisme')->fetchAll()->setKey('id')->setValue('nom'),
			'validators' => array('NotEmpty'),
			'errors' => array(),
		));

		$this->setElement('metier_id', array(
			'type' => 'select',
			'id' => 'metier_id',
			'label' => $filter_no_html->filter( $i18n -> say('metier_id_intervenant') ),
			'placeholder' => $filter_no_html->filter( $i18n -> say('metier_id_intervenant') ),
			'value' => $intervenant->has('metier_id') ? $intervenant->get('metier_id') : null,
			'fake_options' => array(null => 'Non déterminé'),
			'options' => $intervenant->getTable('Model_Metier')->fetchAll()->setKey('id')->setValue('metier'),
			'errors' => array(),
			'needed' => false,
		));
		
		$this->setElement('commentaire', array(
			'type' => 'textarea',
      		'rows' => '3',
			'id' => 'commentaire',
			'label' => $filter_no_html->filter( $i18n -> say('commentaire_intervenant') ),
			'placeholder' => $filter_no_html->filter( $i18n -> say('commentaire_intervenant') ),
			'value' => $intervenant->has('commentaire') ? $intervenant->get('commentaire') : null,
			'validators' => array(),
			'errors' => array(),
		));

	}

}
<?php

class Form_Fsn_Categorie extends Yab_Form {

	public function __construct(Model_Fsn_Categorie $fsn_categorie, $mode = 'add') {

    // Definition entete formulaire
    $this->set('method', 'post')->set('name', 'form_fsn_categorie')->set('action', '')->set('role', 'form')->set('class', 'form-horizontal');
    
    
    // Appel a la connexion BDD
    $registry = Yab_Loader::getInstance()->getRegistry(); 
    $db = Yab_Loader::getInstance()->getRegistry()->get('db');

    // Pre-Selection du categorie_type
    $select_objects = $fsn_categorie->fetchAll()->where('categorie_type = "categorie_object" ');

		$this->setElement('categorie_object', array(
			'type' => 'select',
			'id' => 'categorie_object',
			'label' => 'categorie_object',
			'value' => $fsn_categorie->has('categorie_object') ? $fsn_categorie->get('categorie_object') : null,
			'options' => $select_objects->setKey('categorie_key')->setValue('categorie_value'),        
			'onchange' => 'submit()',
      'validators' => array('NotEmpty'),
			'errors' => array(),
		));
    
    if (!$this->getElement('categorie_object')->getValue()) {
  	// Impose un choix pr�alable (� la siute du formulaire ) du Partenariats � d�finir.

      return $this ;    
    } else {
          
      // Pre-Selection du categorie_type
      $object_categories = $this->getElement('categorie_object')->getValue() ;
      $select_categories = $db->prepare('SELECT 
      		id, 
        	categorie_object, 
        	categorie_type, 
        	categorie_key, 
        	categorie_value, 
        	color, 
        	system, 
        	visible, 
        	ordre, 
        	description
        FROM fsn_categorie
      ') 
        ->where('categorie_object = "'.$object_categories.'" ')
        ->where('categorie_type = "categorie_type"');
      /*
      print '<pre>';
      print_r($select_categories) ;
      print '</pre>';
      die;
      */
  		$this->setElement('categorie_type', array(
  			'type' => 'select',
  			'id' => 'categorie_type',
  			'label' => 'categorie_type',
  			'value' => $fsn_categorie->has('categorie_type') ? $fsn_categorie->get('categorie_type') : null,
  			'options' => $select_categories->setKey('categorie_key')->setValue('categorie_value'),        
  			'onchange' => 'submit()',
        'validators' => array('NotEmpty'),
  			'errors' => array(),
  		));
      
      if (!$this->getElement('categorie_type')->getValue()) {
  	// Impose un choix pr�alable (� la siute du formulaire ) du Partenariats � d�finir.
  
        return $this ;    
      } else {
  
        $type_categories = $this->getElement('categorie_type')->getValue() ;
        $post_html = '';
        $categories = $db->prepare('SELECT 
          	id, 
          	categorie_object, 
          	categorie_type, 
          	categorie_key, 
          	categorie_value, 
          	color, 
          	system, 
          	visible, 
          	ordre, 
          	description
          FROM fsn_categorie
        ');
        
        $categories->where('categorie_type = :typecategories')->bind(':typecategories', $type_categories) ;
        
        $count = $categories->count() ;
        if ( $count != 0 ) {
        
          $list_categories = $categories->toArray();
          
          $post_html .= '<label class="col-md-3 control-label" for="'.$type_categories.'">&eacute;l&eacute;ments d&eacute;j&agrave; d&eacute;finis </label>';
          $post_html .= '<div class="col-md-9">';
          $post_html .= '<div class="panel panel-default"><div class="panel-body">';
          $post_html .= '<fieldset class="D3_fieldset">';
          $post_html .= '<table width=100%>';
          foreach ( $list_categories as $categories) {
            $post_html .= '<tr><td>'.$categories->get('categorie_key').'</td>';
            $post_html .= '<td> <=> </td>';
            $post_html .= '<td>'.$categories->get('categorie_value').'</td></tr>';    
          }
          $post_html .= '</table>';
          $post_html .= '</fieldset>';       
          $post_html .= '</div></div></div>';
          
          $this->getElement('categorie_type')->set('post_html', $post_html );
        }
              
    		$this->setElement('categorie_value', array(
    			'type' => 'text',
    			'id' => 'categorie_value',
    			'label' => 'categorie_value',
    			'value' => $fsn_categorie->has('categorie_value') ? $fsn_categorie->get('categorie_value') : null,
    			'validators' => array('NotEmpty'),
    			'errors' => array(),
    		));
        
        $categorie_value = $this->getElement('categorie_value')->getValue() ;
        $categorie_key = ( !empty($categorie_value) ) ? $this->_calculeKey($categorie_value) : null ;
        
    		$this->setElement('categorie_key', array(
    			'type' => 'text',
    			'id' => 'categorie_key',
    			'label' => 'categorie_key',
    			'value' => $fsn_categorie->has('categorie_key') ? $fsn_categorie->get('categorie_key') : $categorie_key,
    			'validators' => array('NotEmpty'),
    			'errors' => array(),
    		));
    		
        
        $default_color = !empty($categorie_value ) ? substr(hash('crc32', $categorie_value ), -6 ) : 'FFFFFF';
        
        $this->setElement('color', array(
    			'type' => 'text',
    			'id' => 'color',
          'label' => 'Couleur',
          'class' => 'color',
    			'value' => $fsn_categorie->has('color') ? $fsn_categorie->get('color') : $default_color,
    			'validators' => array(),
    			'errors' => array(),
    		));
    
    		$this->setElement('system', array(
    			'type' => 'checkbox',
    			'id' => 'system',
    			'label' => 'system',
          'class' =>'text-justify',
    			'value' => $fsn_categorie->has('system') ? $fsn_categorie->get('system') : '0',
    			'validators' => array('NotEmpty'),
    			'errors' => array(),
    		));
          
    		$this->setElement('visible', array(
    			'type' => 'checkbox',
    			'id' => 'visible',
    			'label' => 'visible',
          'class' =>'text-justify',
    			'value' => $fsn_categorie->has('visible') ? $fsn_categorie->get('visible') : '1',
    			'validators' => array('NotEmpty'),
    			'errors' => array(),
    		));
    
    		$this->setElement('ordre', array(
    			'type' => 'text',
    			'id' => 'ordre',
    			'label' => 'ordre',
    			'value' => $fsn_categorie->has('ordre') ? $fsn_categorie->get('ordre') : null,
    			'validators' => array('Int', 'NotEmpty'),
    			'errors' => array(),
    		));
    
    		$this->setElement('description', array(
    			'type' => 'textarea',
          'rows' => '3',
    			'id' => 'description',
    			'label' => 'description',
    			'value' => $fsn_categorie->has('description') ? $fsn_categorie->get('description') : null,
    			'validators' => array(),
    			'errors' => array(),
    		));

    		$this->setElement('old_id', array(
    			'type' => 'text',
    			'id' => 'old_id',
    			'label' => 'old_id',
          'readonly' => 'readonly',
    			'value' => $fsn_categorie->has('old_id') ? $fsn_categorie->get('old_id') : null,
    			'validators' => array(),
    			'errors' => array(),
    		));
                
        foreach($this->getElements() as $element) {
      
          if($mode == 'add')
            if ($element->get('name') == 'categorie_key' ) {
                $element->set('type', 'hidden');
              }
            
          if($mode == 'edit') {
              if ($element->get('name') == 'type_categories' || 
                  $element->get('name') == 'categorie_key') {
                $element->set('readonly', 'readonly');
              }
          }
           
          if($mode == 'show') {
            $element->set('readonly', 'readonly');
            $element->set('disabled', 'disabled');
          
          }
        
        }
      
      }
    
    }

	}
  
  public function _calculeKey($str, $charset = 'utf-8') {
	
  	$str = htmlentities ( $str, ENT_NOQUOTES, $charset );
		
		$str = preg_replace ( '#&([A-za-z])(?:acute|cedil|caron|circ|grave|orn|ring|slash|th|tilde|uml);#', '\1', $str );
		$str = preg_replace ( '#&([A-za-z]{2})(?:lig);#', '\1', $str ); // pour les ligatures e.g. '&oelig;'
		$str = preg_replace ( '#&[^;]+;#', '', $str ); // supprime les autres caract?res
		$str = preg_replace ( "#[^a-zA-Z ]#", "", $str );
		
		return str_replace ( " ", "_", $str );
	}

}

 
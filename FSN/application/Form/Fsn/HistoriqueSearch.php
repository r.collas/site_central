<?php

class Form_Fsn_HistoriqueSearch extends Yab_Form {

	public function __construct(Model_Fsn_Historique $fsn_historique) {

	$registry = Yab_Loader::getInstance()->getRegistry(); 
    $i18n = $registry->get('i18n');
    $db = Yab_Loader::getInstance()->getRegistry()->get('db');
	
	$filter_html = new Yab_Filter_NoHtml();

	$this->set('method', 'post')->set('name', 'form_fsn_historique')->set('action', '')->set('role', 'form')->set('class', 'form-horizontal');
    
    $list_modules = $db->prepare('
      SELECT categorie_object
      FROM fsn_historique
      GROUP BY categorie_object
    ')->setKey('categorie_object')->setValue('categorie_object');

		$this->setElement('categorie_object', array(
			'type' => 'select',
			'id' => 'categorie_object',
			'label' => $filter_html->filter( $i18n -> say('categorie_object') ), 
			'placeholder' => $filter_html->filter( $i18n -> say('categorie_object') ),   
			'value' => $fsn_historique->has('categorie_object') ? $fsn_historique->get('categorie_object') : null,
			'options' => $list_modules,
			'onchange' => 'submit() ;',
			'fake_options' => array('' => '-- '.$i18n->say('make_a_choice').' le '.$i18n->say('module').' ',),
			'validators' => array('NotEmpty'),
			'errors' => array(
				'NotEmpty' => array(
					'IS_EMPTY' => $i18n->say('make_a_choice').' le '.$i18n->say('module'), 
				),
			),
		));

    $list_action = $db->prepare('
      SELECT type_action
      FROM fsn_historique
      GROUP BY type_action
    ')->setKey('type_action')->setValue('type_action');
 
		$this->setElement('type_action', array(
			'type' => 'select',
			'id' => 'type_action',
			'label' => 'Action',
			'value' => $fsn_historique->has('type_action') ? $fsn_historique->get('type_action') : null,
			'options' => $list_action,
			'fake_options' => array('' => '-- Toutes actions ',),
			'needed' => false,
		));
    
    $list_login = $db->prepare('
      SELECT login
      FROM fsn_historique
      GROUP BY login
    ')->setKey('login')->setValue('login');
       
		$this->setElement('login', array(
			'type' => 'select',
			'id' => 'login',
			'label' => $filter_html->filter( $i18n -> say('login') ), 
			'placeholder' => $filter_html->filter( $i18n -> say('login') ),
			'value' => $fsn_historique->has('login') ? $fsn_historique->get('login') : null,
      		'options' => $list_login,
      		'fake_options' => array('' => '-- Tout utilisateurs ',),
      		'needed' => false,
		));

	}

}
<?php

class Form_Fsn_Admin_Profil extends Yab_Form {

	public function __construct(Model_Fsn_Admin_Profil $fsn_admin_profil) {

		$this->set('method', 'post')->set('name', 'form_fsn_admin_profil')->set('action', '')->set('role', 'form');

		$this->setElement('user_id', array(
			'type' => 'select',
			'id' => 'user_id',
			'label' => 'user_id',
			'value' => $fsn_admin_profil->has('user_id') ? $fsn_admin_profil->get('user_id') : null,
			'fake_options' => array(),
			'options' => $fsn_admin_profil->getTable('Model_User')->fetchAll()->setKey('id')->setValue('login'),
			'errors' => array(),
		));

		$this->setElement('profil_name', array(
			'type' => 'text',
			'id' => 'profil_name',
			'label' => 'profil_name',
			'value' => $fsn_admin_profil->has('profil_name') ? $fsn_admin_profil->get('profil_name') : null,
			'validators' => array('NotEmpty'),
			'errors' => array(),
		));

		$this->setElement('environnement', array(
			'type' => 'text',
			'id' => 'environnement',
			'label' => 'environnement',
			'value' => $fsn_admin_profil->has('environnement') ? $fsn_admin_profil->get('environnement') : null,
			'validators' => array('NotEmpty'),
			'errors' => array(),
		));

		$this->setElement('comment', array(
			'type' => 'text',
			'id' => 'comment',
			'label' => 'comment',
			'value' => $fsn_admin_profil->has('comment') ? $fsn_admin_profil->get('comment') : null,
			'validators' => array(),
			'errors' => array(),
		));

	}

}
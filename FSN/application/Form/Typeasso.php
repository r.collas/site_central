<?php

class Form_Typeasso extends Yab_Form {

	public function __construct(Model_Typeasso $typeasso) {

		$this->set('method', 'post')->set('name', 'form_typeasso')->set('action', '')->set('role', 'form')->set('class', 'form-horizontal');

		$this->setElement('type', array(
			'type' => 'text',
			'id' => 'type',
			'label' => 'type',
			'value' => $typeasso->has('type') ? $typeasso->get('type') : null,
			'validators' => array('NotEmpty'),
			'errors' => array(),
		));

		$this->setElement('visible', array(
			'type' => 'text',
			'id' => 'visible',
			'label' => 'visible',
			'value' => $typeasso->has('visible') ? $typeasso->get('visible') : '1',
			'validators' => array('Int', 'NotEmpty'),
			'errors' => array(),
		));

	}

}
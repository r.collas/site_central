<?php

class Form_Classification extends Yab_Form {

	public function __construct(Model_Classification $classification) {

		$this->set('method', 'post')->set('name', 'form_classification')->set('action', '')->set('role', 'form')->set('class', 'form-horizontal');

		$this->setElement('classification', array(
			'type' => 'text',
			'id' => 'classification',
			'label' => 'classification',
			'value' => $classification->has('classification') ? $classification->get('classification') : null,
			'validators' => array('NotEmpty'),
			'errors' => array(),
		));

		$this->setElement('visible', array(
			'type' => 'text',
			'id' => 'visible',
			'label' => 'visible',
			'value' => $classification->has('visible') ? $classification->get('visible') : '1',
			'validators' => array('Int', 'NotEmpty'),
			'errors' => array(),
		));

	}

}
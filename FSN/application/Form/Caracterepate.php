<?php

class Form_Caracterepate extends Yab_Form {

	public function __construct(Model_Caracterepate $caracterepate) {

		$this->set('method', 'post')->set('name', 'form_caracterepate')->set('action', '')->set('role', 'form')->set('class', 'form-horizontal');

		$this->setElement('caracterepate', array(
			'type' => 'text',
			'id' => 'caracterepate',
			'label' => 'caracterepate',
			'value' => $caracterepate->has('caracterepate') ? $caracterepate->get('caracterepate') : null,
			'validators' => array('NotEmpty'),
			'errors' => array(),
		));

		$this->setElement('visible', array(
			'type' => 'text',
			'id' => 'visible',
			'label' => 'visible',
			'value' => $caracterepate->has('visible') ? $caracterepate->get('visible') : '1',
			'validators' => array('Int', 'NotEmpty'),
			'errors' => array(),
		));

	}

}
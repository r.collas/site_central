<?php
/**
 * Plugin Le Stratifiant - programme macro excel d'analyse stratigraphique 
 * Chargements des informations des unités stratigraphique dans une feuille excel contenant une macro d'analyse fonctionnel.
 * Le Stratifiant étant un programme en code VBA développé par Bruno Desachy (Titulaire)    
 * @category   Yab_Controller_Plugin_Abstract
 * @package Plugin_Stratifiant
 * @author Cyrille GICQUEL
 * @link http://le-nid-du-stratifiant.ouvaton.org/spip.php?article2
 *   
 */
 
class Plugin_Stratifiant extends Yab_Controller_Plugin_Abstract {

  public function preDispatch(Yab_Controller_Request $request, Yab_Controller_Response $response) { }
  public function preRender(Yab_Controller_Request $request, Yab_Controller_Response $response){ }
	public function postRender(Yab_Controller_Request $request, Yab_Controller_Response $response){}
	public function postDispatch(Yab_Controller_Request $request, Yab_Controller_Response $response){}

  private function _dataSourceConfig(){
    
    // liste des fichiers Data Sources our Le_Stratifiant :
    $data_source_file = array( 'ExportPhases', 'ExportUS', 'ExportRelations', 'ExportSynchros', 'ExportLegendRequete' ) ;

    // transcryption entre le "contexte" FSN et le type 
    $StratifiantType_def = array(
      "* à déterminer" => "autre",
      "Altération" => "altération",
      "Construction" => "couche",
      "Démolition" => "autre",
      "Négatif" => "négatif",
      "Occupation" => "couche",
      "Regroupement" => "couche",
      "Remblais" => "couche",
      "Remblais d'occupation" => "couche",
      "Reste humain en connexion" => "autre",
      "Sous-sol naturel" => "autre",
      "Technique" => "autre",
    ) ;
/*

    $StratifiantType_def = array(
      hash("crc32","* à déterminer") => "autre",
      hash("crc32","Altération") => "altération",
      hash("crc32","Construction") => "couche",
      hash("crc32","Démolition") => "autre",
      hash("crc32","Négatif") => "négatif",
      hash("crc32","Occupation") => "couche",
      hash("crc32","Regroupement") => "couche",
      hash("crc32","Remblais") => "couche",
      hash("crc32","Remblais d'occupation") => "couche",
      hash("crc32","Reste humain en connexion") => "autre",
      hash("crc32","Sous-sol naturel") => "autre",
      hash("crc32","Technique") => "autre",
    ) ;
*/        
    // liste des US dans la feuille excel
    $ExportUS_def = array (
      'identifiant' => array ( 'stratifiant_column' => 'A', 'fsn_us' => 'us_identification' ), 
      'type'        => array ( 'stratifiant_column' => 'B', 'fsn_us' => 'contexte', 'stratifiant_type_us' => $StratifiantType_def ), 
      'tpq'         => array ( 'stratifiant_column' => 'C', 'fsn_us' => 'tpq_aaaa' ), // approximation, cas particulier faisant référence à la liste des phases,        
      'tpq_estime'  => array ( 'stratifiant_column' => 'D', 'fsn_us' => 'tpq_aaaa' ), 
      'taq_estime'  => array ( 'stratifiant_column' => 'E', 'fsn_us' => 'taq_aaaa' ),
      'taq'         => array ( 'stratifiant_column' => 'F', 'fsn_us' => 'taq_aaaa' ), // approximation, cas particulier faisant référence à la liste des phases,
      'couleur'     => array ( 'stratifiant_column' => 'G', 'fsn_us' => '' ),
      'phase_deb'   => array ( 'stratifiant_column' => 'H', 'fsn_us' => 'phase_ordre' ),  // approximation, cas particulier faisant référence à la liste des phases,  
      'phase_fin'   => array ( 'stratifiant_column' => 'I', 'fsn_us' => 'phase_ordre' ), // approximation, cas particulier faisant référence à la liste des phases,
      'phase_deduite' => array ( 'stratifiant_column' => 'J', 'fsn_us' => 'phase_ordre' ), // approximation, cas particulier faisant référence à la liste des phases,
        
    );
    // liste des relations entre US dans la feuille excel # superposition / synchronisme
    $ExportRelations_def = array(
      'us_1' => array ( 'stratifiant_column' => 'A', 'fsn_us' => 'us_anterieur' ),
      'us_2' => array ( 'stratifiant_column' => 'B',  'fsn_us' => 'us_posterieur' ),
      'type' => array ( 'stratifiant_column' => 'C', 'fsn_us' => 'strati_relation_type' ),    
    );

    // liste des relations entre US dans la feuille excel # superposition / synchronisme
    $ExportSynchros_def = array(
      'us_1' => array ( 'stratifiant_column' => 'A', 'fsn_us' => 'us_left_synchro' ),
      'us_2' => array ( 'stratifiant_column' => 'B', 'fsn_us' => 'us_right_synchro' ),
      'type' => array ( 'stratifiant_column' => 'C', 'fsn_us' => 'strati_relation_type' ),    
    );
    
    $ExportLegendRequete_def = array() ;
    
    // liste des relations entre US dans la feuille excel # superposition / synchronisme
    $ExportPhases_def = array(
      'phases'      => array ( 'stratifiant_column' => 'A', 'fsn_us' => 'phase_identification' ), 
      'ordre'       => array ( 'stratifiant_column' => 'B', 'fsn_us' => 'phase_ordre' ), 
      'tpq'         => array ( 'stratifiant_column' => 'C', 'fsn_us' => 'tpq_aaaa' ), // approximation
      'tpq_deduit'  => array ( 'stratifiant_column' => 'D', 'fsn_us' => '' ),
      'taq'         => array ( 'stratifiant_column' => 'E', 'fsn_us' => 'taq_aaaa' ), // approximation
      'taq_deduit'  => array ( 'stratifiant_column' => 'F', 'fsn_us' => '' ),      
    );
    
    /* Initialisation des valeurs */
    $data_source_config = array(
      'data_source_file' => $data_source_file,
      'ExportPhases' => $ExportPhases_def,
      'ExportUS' => $ExportUS_def,
      'ExportRelations' => $ExportRelations_def,
      'ExportSynchros' => $ExportSynchros_def,
      'ExportLegendRequete' => $ExportLegendRequete_def,      
    ) ;
    
    return $data_source_config ;   
  
  }
  
  private function _stratifiantConfig(){

    /* Positionnement des champs de saisie dans la feuile de calcul*/
    $worksheets = array ( 'traitement', 'saisie_donnees', 'import_donnees', 'mise en couleurs' );
    
    // transcryption entre le "contexte" FSN et le type 
    $StratifiantType_def = array(
      "* à déterminer" => "autre",
      "Altération" => "altération",
      "Construction" => "couche",
      "Démolition" => "autre",
      "Négatif" => "négatif",
      "Occupation" => "couche",
      "Regroupement" => "couche",
      "Remblais" => "couche",
      "Remblais d'occupation" => "couche",
      "Reste humain en connexion" => "autre",
      "Sous-sol naturel" => "autre",
      "Technique" => "autre",
    ) ;
        
    // liste des US dans la feuille excel
    $liste_us_col_init = array (
      'identifiant' => array ( 'stratifiant_column' => 'B', 'fsn_us' => 'us_identification' ), 
      'type'        => array ( 'stratifiant_column' => 'C', 'fsn_us' => 'contexte', 'stratifiant_type_us' => $StratifiantType_def ), 
      'tpq'         => array ( 'stratifiant_column' => 'D', 'fsn_us' => 'tpq' ), // approximation, cas particulier faisant référence à la liste des phases,        
      'tpq_estime'  => array ( 'stratifiant_column' => 'E', 'fsn_us' => 'tpq' ), 
      'taq_estime'  => array ( 'stratifiant_column' => 'F', 'fsn_us' => 'taq' ),
      'taq'         => array ( 'stratifiant_column' => 'G', 'fsn_us' => 'taq' ), // approximation, cas particulier faisant référence à la liste des phases,
      'couleur'     => array ( 'stratifiant_column' => 'H', 'fsn_us' => '' ),
      'phase_deb'   => array ( 'stratifiant_column' => 'I', 'fsn_us' => 'phase_ordre' ),  // approximation, cas particulier faisant référence à la liste des phases,  
      'phase_fin'   => array ( 'stratifiant_column' => 'J', 'fsn_us' => 'phase_ordre' ), // approximation, cas particulier faisant référence à la liste des phases,
      'phase_deduite' => array ( 'stratifiant_column' => 'K', 'fsn_us' => 'phase_ordre' ), // approximation, cas particulier faisant référence à la liste des phases,
         
    );
    // liste des relations entre US dans la feuille excel # superposition / synchronisme
    $superposition_us_col_init = array( 
      'us_1' => array ( 'stratifiant_column' => 'O', 'fsn_us' => 'us_anterieur' ),
      'type' => array ( 'stratifiant_column' => 'P', 'fsn_us' => 'strati_relation_type' ),
      'us_2' => array ( 'stratifiant_column' => 'Q', 'fsn_us' => 'us_posterieur' ),    
    );

    // liste des relations entre US dans la feuille excel # superposition / synchronisme
    $synchronisme_us_col_init = array(
      'us_1' => array ( 'stratifiant_column' => 'O', 'fsn_us' => 'us_left_synchro' ),
      'type' => array ( 'stratifiant_column' => 'P', 'fsn_us' => 'strati_relation_type' ),
      'us_2' => array ( 'stratifiant_column' => 'Q', 'fsn_us' => 'us_right_synchro' ),    
    );

    
    // liste des relations entre US dans la feuille excel # superposition / synchronisme
    $phases_col_init = array(
      'phases'      => array ( 'stratifiant_column' => 'S', 'fsn_us' => 'phase_identification' ), 
      'ordre'       => array ( 'stratifiant_column' => 'T', 'fsn_us' => 'phase_ordre' ), 
      'tpq'         => array ( 'stratifiant_column' => 'U', 'fsn_us' => 'datatdebbas_aaaa' ), // approximation
      'tpq_deduit'  => array ( 'stratifiant_column' => 'V', 'fsn_us' => '' ),
      'taq'         => array ( 'stratifiant_column' => 'W', 'fsn_us' => 'datatfinhaut_aaaa' ), // approximation
      'taq_deduit'  => array ( 'stratifiant_column' => 'X', 'fsn_us' => '' ),      
    );

    /* Initialisation des valeurs */
    $le_stratifiant_config = array(
      'worksheets' => $worksheets,
      'liste_us_col_init' => $liste_us_col_init,
      'superposition_us_col_init' => $superposition_us_col_init,
      'synchronisme_us_col_init' => $synchronisme_us_col_init,
      'phases_col_init' => $phases_col_init,
    ) ;
    
    return $le_stratifiant_config ;   
  
  }
  
  private function _dupliqueLestratifiant($sitefouille_nomabrege, $directory=null) {
    
    $stratifiant_path = 'tmp/';
    $stratifiant_file_name = 'Le_Stratifiant_0_3_5.xls';
    
    $stratifiant_ini_file = $stratifiant_path.$stratifiant_file_name ;
    
    // Controle existance du répertoire 
    if ( !empty($directory) ){
      if( !file_exists($stratifiant_path.$directory)){
        mkdir($stratifiant_path.$directory,0777);      
      }
      $stratifiant_new_path = $stratifiant_path.$directory."/";    
    }
    $stratifiant_sdf_file = $stratifiant_new_path."Le_Stratifiant_".$sitefouille_nomabrege."-".date("Ymd").".xls";
    
    // Duplication du fichier "Le Stratifiant" source.
    $fsrc = fopen($stratifiant_ini_file,'r');
    $fdest = fopen($stratifiant_sdf_file,'w+');
    $len = stream_copy_to_stream($fsrc,$fdest);
    fclose($fsrc);
    fclose($fdest);
    
    return $stratifiant_sdf_file ;
  }
  
  /**
   * Publication des information du site de fouille dans la feuille de calcul Le_Stratifiant
   * @Param     
   */
  public function publish($sitefouille, $liste_us, $anteroposterite, $synchronisation, $phases_us ){
    
    $loader = Yab_Loader::getInstance();
    $config = $loader->getConfig();

    // Récupération de tous les paramètres Le Stratifiant <=> FSN     
    $init_stratifiant_value = $this->_stratifiantConfig();
    
    $base_url = $loader->getRequest()->getBaseUrl() ;
    
    $sitefouille_nom_secure = preg_replace('/ /', '_', strtolower($sitefouille->get('nom','Accent')));
    
    // Duplication du fichier "Le Stratifiant" source.
    $stratifiant_sdf_file = $this->_dupliqueLestratifiant($sitefouille_nom_secure, date("Ymd") ) ;

    $objReader = new PHPExcel_Reader_Excel5();
    
    $objPHPExcel = $objReader->load($stratifiant_sdf_file);
    
    $objWorksheet = $objPHPExcel->getSheet(1);

    /*
    * On Commance par lister les phases
    * => pour les numéroter (numéro repris dans les autres insertion)
    */
    $phase_ordre = 0 ;
    foreach($phases_us as $phases_key => $phases_value){
      $liste_phases_col_init = $init_stratifiant_value['phases_col_init'] ; 
      
      $row = $phase_ordre + 3 ; // premier ligne à partir de laquelle on ecrit dans le excel
      $phases_us[$phases_key]['phase_ordre'] = $phase_ordre ;
      
      // liste sur la définition des colonnes pour les phases
      foreach( $liste_phases_col_init as $column_key => $column_value ) {
        
        $column = $column_value['stratifiant_column'] ;
        $cell_value = (!empty($column_value['fsn_us']) ) ? $phases_value[$column_value['fsn_us']] : '' ; 
        $cell = "$column"."$row" ;
        $objWorksheet->SetCellValue($cell, $cell_value); // On écrit notre texte dans la cellule adHoc 

      }
      $phase_ordre = $phase_ordre + 1 ;
      
    }

    foreach($liste_us as $us_key => $us_value ){
      $liste_us_col_init = $init_stratifiant_value['liste_us_col_init'] ;
      
      $row = $us_key + 3 ;
  
      foreach( $liste_us_col_init as $column_key => $column_value ) {
        
        $column = $column_value['stratifiant_column'] ;
        $cell_value = (!empty($column_value['fsn_us']) ) ? $us_value->get($column_value['fsn_us']) : '' ;  
        $cell = "$column"."$row" ;
        $objWorksheet->SetCellValue($cell, $cell_value);// On écrit notre texte en A1 
      
      }
    
    }

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    $objWriter->save($stratifiant_sdf_file);
    
    /// header('Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    // header('Content-Disposition:inline;filename='.$stratifiant_sdf_file);
    // $objWriter->save('php://output');
    
    // return ;
    
  }
  
  /**
   * Publication des fichiers sources de donnees pour la feuille de calcul Le_Stratifiant
   *    
   * @Param $sitefouille guuid, 
   * @Param $liste_us array(), 
   * @Param $anteroposterite array(), 
   * @Param $synchronisation array(), 
   * @Param $phases_us array()
   * 
   * @result : fichier archive [Nom_Abrege_Site_De_Fouille]_[Date].zip
   *          contenant               
   */
  public function StratiDataSources($sitefouille, $liste_us, $anteroposterite, $synchronisation, $phases_us ){
    
    $loader = Yab_Loader::getInstance();
    $config = $loader->getConfig();
    
    // Initialisation des informations à retourner
    $return_strati_data_source = array() ;
    
    // Récupération de tous les paramètres Le Stratifiant <=> FSN 
    $data_source_config = $this->_dataSourceConfig();
    
    $base_url = $loader->getRequest()->getBaseUrl() ;
    $sitefouille_nom_secure = preg_replace('/ /', '_', strtolower($sitefouille->get('nom','Accent')) );
    $sitefouille_path = hash('crc32', $sitefouille->get('id') ) ;  
    
    $stratifiant_file_name = 'Le_Stratifiant_0_3_5.xls';
    $stratifiant_path = 'tmp/';
    
    $datasource_path = $sitefouille_nom_secure."-".date("Ymd") ;
    $global_datasource_path = $stratifiant_path.$datasource_path."/" ;
    // Controle existance du répertoire 
    if ( !empty( $global_datasource_path ) ){
      if( !file_exists($stratifiant_path.$datasource_path)){
        mkdir($global_datasource_path,0777);      
      }
    }

    $return_strati_data_source['info'] = array (
      'stratifiant_file_name' => $stratifiant_file_name,
      'stratifiant_path' => $stratifiant_path,
      'datasource_path' => $datasource_path,
    );
    
    /*
    * On Commance par lister les phases
    * => pour les numéroter (numéro repris dans les autres insertion)
    * => Liste ensuite les US contenu pour propager la numérotation de la phase    
    */
    $us_phase = array() ;
    $phase_ordre = 0 ;
    foreach($phases_us as $phase_key => $phase_value){
      // Numerotation de la phase (correspond au tri par ordre chrono)
      $phases_us[$phase_key]['phase_ordre'] = $phase_ordre ;
      
      $liste_us = $phase_value['us'] ;
      
      foreach($liste_us as $us_key => $us_value ){
        // Numerotation de la phase de l'us (correspond au tri par ordre chrono)
        $phases_us[$phase_key]['us'][$us_key]['phase_ordre'] = $phase_ordre ;
        $us_value['phase_ordre'] = $phase_ordre ;
        
        // tableau de US avec correspondance au nomero d'ordre des phase
        $us_phase[] = $us_value ;
      }
      
      $phase_ordre = $phase_ordre + 1 ;
        
    }

    /*
     * Declaration des informations pour la génération du fichier Excel
     *
     */
    $strati_data_sources = array() ;
    $strati_data_sources['sitefouille'] = $sitefouille ;
    
    $data_source_file = $data_source_config['data_source_file'] ;
    foreach($data_source_file as $data_source_key => $data_source_value ) {
      
      $strati_data_sources['filename'] = $data_source_value ;
      $strati_data_sources['colunm'] = $data_source_config[$data_source_value] ;
      
      switch ($data_source_value) {
        
        case 'ExportPhases':
          $strati_data_sources['value'] = $phases_us ;
          break;
            
        case 'ExportUS':
          $strati_data_sources['value'] = $us_phase ;
          // $strati_data_sources['value'] = $liste_us ;
          break;            
        
        case 'ExportRelations':
          $strati_data_sources['value'] = $anteroposterite ;
          break;
        
        case 'ExportSynchros':
          $strati_data_sources['value'] = $synchronisation ;
          break;
        
        case 'ExportLegendRequete':
          break;
            
        default:
          break;
                      
      }
      $stratifiant_sdf_files[$data_source_value] = array(
      'name' => $data_source_value.'.xls',  
      'path' => $this->getExcelDataFile($strati_data_sources),
      ) ;
    
    } 
    $return_strati_data_source['source_files'] = $stratifiant_sdf_files;
    
    return $return_strati_data_source;
    
  }
  
  public function getExcelDataFile($strati_data_sources){
  
    //$session = $this->getSession();
    $session = $this->getSession()->get('session') ;
    
    $user_name = $session['prenom']." ".$session['nom'] ;

    // Recuperation des valeurs
    $sitefouille = $strati_data_sources['sitefouille'] ;
    $filename = $strati_data_sources['filename'] ;
    $datasource = $strati_data_sources['value'] ;
    $datasource_config = $strati_data_sources['colunm'] ;
    
    /*
     *  Initialisation des info filesystem (repertoire / fichier )
     */ 
    $sitefouille_nom_secure = preg_replace('/ /', '_', strtolower($sitefouille->get('nom','Accent')) );
    $sitefouille_path = hash('crc32', $sitefouille->get('id') ) ;  
    
    $stratifiant_file_name = 'Le_Stratifiant_0_3_5.xls';
    $stratifiant_path = 'tmp/';
    
    $datasource_path = $sitefouille_nom_secure."-".date("Ymd") ;
    $global_datasource_path = $stratifiant_path.$datasource_path."/" ; 
    // Controle existance du répertoire 
    if ( !empty($global_datasource_path) ){
      if( !file_exists($global_datasource_path)){
        mkdir($global_datasource_path,0777);      
      }
    }
    // Declaration du (futur) fichier Excel
    $stratifiant_sdf_file = $global_datasource_path.$filename.".xls" ;
     
    /*
     * 1 - Declaration du Fichier Excel
     */ 
    $objPHPExcel = new PHPExcel();
    
    // Ecriture des proprietes du fichier excel
    $objPHPExcel->getProperties()->setCreator($user_name)
    							 ->setLastModifiedBy($user_name)
    							 ->setTitle("Fichier DataSource ".$filename." Le Stratifiant")
    							 ->setSubject("DataSource ".$filename." Le Stratifiant")
    							 ->setDescription("Fichier DataSource ".$filename." Le Stratifiant pour ".$sitefouille->get('nom') )
    							 ->setKeywords("FSN Stratifiant ".$sitefouille->get('nomabrege') )
    							 ->setCategory("Fichier DataSource ");   
    /*
     * 2 - Insertion des donnes dans le fichier Excel
     */ 

    foreach($datasource as $source_key => $source_value ){
      $row = $source_key + 1 ;
      
      $objPHPExcel->setActiveSheetIndex(0) ;
      
      foreach( $datasource_config as $config_key => $config_value ) {
        
        $column = $config_value['stratifiant_column'] ;
        $fsn_us = $config_value['fsn_us'] ;
        if( $fsn_us == 'contexte' ){

          // transcryption du contexte de l'us fsn en type de l'us stratifiant 
          $fsn_us_value = trim($source_value[$fsn_us]) ;
          // $fsn_us_value = hash('crc32',(trim($source_value[$fsn_us]))) ;
           
          $cell_value = (!empty($fsn_us) ) ? $config_value['stratifiant_type_us'][$fsn_us_value] : '' ;
        } else {
          $cell_value = (!empty($fsn_us) ) ? $source_value[$fsn_us] : '' ;
        }         

        // Ecriture de l'entête de colonne    
        $entete = "$column"."1" ;
        $objPHPExcel->getActiveSheet()->setCellValue($entete, $config_key);
        $objPHPExcel->getActiveSheet()->getStyle($entete)->getFont()->setBold(true);
        
        $cell = "$column"."$row" ;
        $objPHPExcel->getActiveSheet()->setCellValue($cell, $cell_value);
      
      }

    }                 
    // Set active sheet index to the first sheet, so Excel opens this as the first sheet
    $objPHPExcel->setActiveSheetIndex(0);          

    /*
     * 2 - Ecriture du fichier Excel
     */   
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    $objWriter->save($stratifiant_sdf_file);
    
    return $stratifiant_sdf_file ;
  }
  
  
  

}
<?php
/**
 * Plugin Zenphoto - Phototh�ques 
 *    
 * @category Plugin Zenphoto
 * @package Plugin_Zenphoto
 * @author Cyrille GICQUEL
 * @link http://www.zenphoto.org
 *   
 */

class Plugin_Zenphoto extends Yab_Controller_Plugin_Abstract {

/*  
  const ZENPHOTO_SERVER = 'localhost/Tests/Applis/zenphoto' ; 
  const ZENPHOTO_USER = 'fsn' ;
  const ZENPHOTO_PASSWORD = 'fsn' ;
*/  


/**
 * Fonctions li�es au framwork Yab 
 * n�cessaire au bon fonctionnement du plugin, mais pas utile en l'occurrence
 */  
  public function preDispatch(Yab_Controller_Request $request, Yab_Controller_Response $response) {}
  public function preRender(Yab_Controller_Request $request, Yab_Controller_Response $response){}
	public function postRender(Yab_Controller_Request $request, Yab_Controller_Response $response){}
	public function postDispatch(Yab_Controller_Request $request, Yab_Controller_Response $response){}
  
/**
 * Fonction memo,
 */  
/*  
  public function init(){
  
    $zenphoto_config = array (
      // 'server' => '185.39.216.145/mediatheque',
      // 'user' => 'fsn_user',
      // 'password' => 'fsn_pass',
      'server' => 'localhost/Tests/Applis/zenphoto',
      'user' => 'fsn',
      'password' => 'fsn',
       
    );
    
    /**
     *  Correspondance entre les pr�rogative du webservice lightroom pour Zenphoto
     *  Et les fonction de la Class       
     */
     /*          
  	$bindings = ( array(
      'zenphoto.login' => 'authorize',
  		'zenphoto.check' => 'checkConnection',
  		'zenphoto.album.getList' => 'getAlbumList',
  		'zenphoto.album.getImages' => 'getAlbumImages',
  		'zenphoto.album.delete' => 'deleteAlbum',
  		'zenphoto.album.create' => 'createAlbum',
  		'zenphoto.album.edit' => 'changeAlbum',
  		'zenphoto.image.delete' => 'deleteImage',
  		'zenphoto.image.upload' => 'imageUpload',
  		'zenphoto.get.comments' => 'getImageComments',
  		'zenphoto.get.thumbnail' => 'getAlbumThumbnail',
  		'zenphoto.get.ratings' => 'getImageRatings',
  		'zenphoto.get.version' => 'getsysVersion',
  		'zenphoto.get.update' => 'updateCheck',
  		'zenphoto.chk.func' => 'checkFunc',
  		'zenphoto.add.comment' => 'addImageComments',
  		'zenphoto.test' => 'test' 
  	) );
  }
*/
  
  private function _jsonisation($jsonisable){
  
    if (is_object($jsonisable)) {
      $jsonisable = $jsonisable->toArray();
    }
    
    $jsonisable = json_encode($jsonisable, JSON_NUMERIC_CHECK) ;
    
    return $jsonisable ;
  
  }

  /**
   * Fonction d'appel � l'API du Plugin Zenphoto-Lightroom-Publisher-4.5.0
   * POST request with JSON body
   * PRE-REQUIS : extension PHP curl active   
   * @params : $methodName (string) => nom de la methode appel�e
   * @params : $params (string) => information JSON
   * @params : $timeout (integer) => Le nombre de secondes � attendre durant la tentative de connexion. 0 pour attendre ind�finiment.                    
   */       
  public function sendJSONRequest( $methodName, $params, $timeout=0){
      	
    $params = json_encode($params, JSON_NUMERIC_CHECK) ;
  	$params = $methodName.'='.($params) ;
  
      // Conf de connexion � la mediatheque
    $loader = Yab_Loader::getInstance();
    $config = $loader->getConfig();
    $config_zenphoto = new Yab_Config();
    $config_zenphoto->setFile($config->get('zenphoto_config'));
    
    $zenphoto_server = $config_zenphoto->get('server');
    $zenphoto_user = $config_zenphoto->get('user');
    $zenphoto_password = $config_zenphoto->get('paswword');
    $zenphoto_level = $config_zenphoto->get('level');

  	$zenphotoURL = 'http://'.$zenphoto_user.':'.$zenphoto_password.'@'.$zenphoto_server.'/plugins/ZenPublisher/ZenRPC.php' ;
    
    $curl = curl_init($zenphotoURL) ;
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array(
              'User-Agent: Fsn Zenphoto Publisher Plugin',
              'Content-type: application/x-www-form-urlencoded',
              'Content-length:  '.strlen($params)            
            ));
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $params);
    curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, $timeout ); 
    
    $responseJSON = curl_exec($curl);
    
    $responseHeaders = curl_getinfo($curl) ;
    $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
    
    if ( !empty($responseHeaders) && ( $status==500 || $status==401 || $status==400 ) ) {
    // if ( $status != 201 ) {
        die("Error: call to URL $zenphotoURL failed with status $status, response $responseJSON, curl_error " . curl_error($curl) . ", curl_errno " . curl_errno($curl));
    }

    curl_close($curl);
  	
  	return trim($responseJSON) ;
    
  }
  
  private function authorize( $login, $password ) {

    // log:trace('ZenphotoAPI.authorize')
    // log:info('Authorizing '..tostring(login).. ' on host:'.. prefs[instanceID].host)
    
  	$auth = false ;
  	$showMsg = true ;
  	
  	$paramMap = $this->initRequestParams() ;
  	$jsonResponse = $this->sendJSONRequest( 'zenphoto.login', $paramMap, 20 ) ;
  	// // Parse response
  	// log:debug("authorize.jsonResponse: "..tostring(jsonResponse))
  	
    if ( empty($jsonResponse) || stristr($jsonResponse, 'html') ){
      die("Error: Server could not be connected!\r\n" .
          "Please make sure that an internet connection is established and that the web service is running." ) ;
  		// log:fatal('Server could not be connected!')
  		$fault = true ;
  		$showMsg = false ;
    
    }
    // log:info('jsonResponse', table_show(jsonResponse))
    
    $result = json_decode( $jsonResponse ) ;
    
    // log:info('jsonresult', table_show(result))
    	
    if (!empty($result) ) {
    		$auth = true ;
    		$showMsg = false ;
        // log:info('Authorization successful')
    } else {
    	if ($result['code'] == '-2') {
        die("Zenphoto version error!\r\n" . 
          $result['message'] );
    		// log:fatal( 'Zenphoto version error!', result.message, 'error' )
    		$fault = true ;
    		$showMsg = false ;
      } else if ($result['code'] == '-1' ){
        // log:info('Authorization failed!')
    		$auth = false ;
    		$showMsg = true ;
      } else if ( empty($jsonResponse) ) {
        // LrDialogs.message( 'Zenphoto plugin not installed!','error' )
        // log:info('Zenphoto plugin not installed!')
    		$fault = true ;
    		$auth = false ;
    		$showMsg = true ;
      }
    	
    }
    
    return $auth ;
    return $showMsg ;

  }
  
  /**
   * checks headers of a http request for failure/errors, if any are found an error with
   * error_message is thrown, error_message is written to log and log_message (if any) is written to
   * the log too      
   */     
   private function on_error($headers, $error_message, $log_message) {
   
    
  	if( !empty($headers) && ( $headers['error'] || ($headers['status']==200 || $headers['status']==201))) {
  
  		if (!empty($headers['status']) ) {
  			$error_message = $error_message." (Code: ".$headers['status'].")" ;
  		} else {
  			$error_message = $error_message." (Error: ".serialize($headers['error']).")" ;
  		}
  		
  		// logger:debug(error_message)
  		
  		if (!empty($log_message) )  {
  			// logger:debug(log_message)
  		}
  		// LrErrors.throwUserError(error_message)
  	}
    
    return "Error : ".$error_message ;	
   }
  
  private function initRequestParams(){
  	
    // Conf de connexion � la mediatheque
    $loader = Yab_Loader::getInstance();
    $config = $loader->getConfig();
    $config_zenphoto = new Yab_Config();
    $config_zenphoto->setFile($config->get('zenphoto_config'));

    // log:trace('initRequestParams')
  	$paramMap = array() ;
  
    $zenphoto_server = $config_zenphoto->get('server');
    $username = $config_zenphoto->get('user');
    $password = $config_zenphoto->get('paswword');
  	$loglevel = $config_zenphoto->get('level');
  	$version = $this->getVersion() ;
     
  	$paramMap = array (
      'loginUsername' => $username,
      'loginPassword' => $password,
      'loglevel' => $loglevel,
      'checkver' => $version,
    ) ;
    
    $this->getUpdate( $version ) ;
    
  	return $paramMap ;
  } 

  /**
   *
   */     
  public function getVersion() {
    // log:trace('ZenphotoAPI.getVersion')
    // $paramMap = $this->initRequestParams() ;
    
  	$paramMap = array() ;
    $jsonResponse = $this->sendJSONRequest( 'zenphoto.get.version', $paramMap, 10 ) ;
  	
  	//	log:debug('getVersion paramMap:', table_show(paramMap))
  	//	log:debug('getVersion:', $jsonResponse)	
  	
    return $this->getSingleValueJSON($jsonResponse) ;
  }

  private function getSingleValueJSON($jsonResponse) {
    // log:trace("ZenphotoAPI.getSingleValueJSON")
  	return json_decode( $jsonResponse) ;
  }


}
/*
  
  ////////////////////////////////////////////////////////////////////////////////
  
  public function uploadImage( filename, $params, file ) {
  	log:trace('ZenphotoAPI.uploadImage')
  	log:info('Uploading JSON file: ', filename)
  
  	$paramMap = initRequestParams()
  		paramMap[1]['filename'] = filename
  		paramMap[1]['file'] = file
  				
  	for key,value in pairs($params) do 
  		paramMap[1][key] = value		
  	}
  	$jsonResponse = $this->sendJSONRequest( 'zenphoto.image.upload', paramMap, 360 )
  	$paramMapTable = table_show(paramMap)
  	log:debug('uploadPhoto.paramMap: ', paramMapTable:gsub("(%[\"file\"%]%s*=%s*)%b\"\"", "%1\"********truncated - file data********\""))
  	log:debug('uploadPhotoJSON:', $jsonResponse)
  	
  	return $this->getTableFromJSON($jsonResponse)
  }
  
  ////////////////////////////////////////////////////////////////////////////////
  
  public function getAlbums( propertyTable, simple ) {	
  	log:trace('ZenphotoAPI.getAlbums')
  	
  	$paramMap = initRequestParams()
  	if simple {
  		//table.insert( paramMap, { simplelist = tostring(simple) } )
  			paramMap[1]['simplelist'] = simple
  			} else { 
  			paramMap[1]['simplelist'] = false
  	}
  	
  	$jsonResponse = $this->sendJSONRequest( 'zenphoto.album.getList', paramMap, 10 )
  	log:debug('getAlbums paramMap:', table_show(paramMap))
  	log:debug('getAlbums: ', $jsonResponse)
  
  	if simple == true {
  		$result = $this->getTableFromJSON(jsonResponse, true)
  		log:debug('getAlbums result:', table_show(result))
  		$noalbum = { { title = '- no sub-album -', value = ''} }
  		
  		if jsonResponse == 'null' {
  		prefs.getAlbums_simplelist = noalbum
  		LrDialogs.message("empty")
  		return noalbum
  		} else { 
  		prefs.getAlbums_simplelist = Utils.joinTables(noalbum, result)
  		return Utils.joinTables(noalbum, result)
  		}
  		
  	} else {
  	prefs.getAlbums_fulllist = $this->getTableFromJSON(jsonResponse, false, false)
  		return $this->getTableFromJSON(jsonResponse, false, false)
  	}
  }
  
  ////////////////////////////////////////////////////////////////////////////////
  
  public function getAlbumImages(id) {	
  	log:trace('ZenphotoAPI.getAlbumImages')
  
  	$paramMap = initRequestParams()
  	paramMap[1]['id'] = id
  	$jsonResponse = $this->sendJSONRequest( 'zenphoto.album.getImages', paramMap, 10 )
  	log:debug('getAlbumImages.paramMap'..table_show(paramMap))
  	log:debug('getAlbumImages:'..$jsonResponse)
  	return $this->getTableFromJSON(jsonResponse, false, false)	
  }
  
  ////////////////////////////////////////////////////////////////////////////////
  
  public function getImageComments(id, propertyTable) {	
  	log:info('ZenphotoAPI.Getimagecomments')
  
  	$zenphotoURLroot = 'http://'.. prefs[instanceID].host..'/'
  $paramMap = initRequestParams()
  paramMap[1]['id'] = id.remoteId
  paramMap[1]['url'] = zenphotoURLroot..id.url
  		
  	$jsonResponse = $this->sendJSONRequest( 'zenphoto.get.comments', paramMap, 10 )
  	log:debug('getImageComments.paramMap '..table_show(paramMap))
  	log:debug('getImageComments:'..$jsonResponse)
  	return $this->getTableFromJSON(jsonResponse, false, false)	
  }
  ////////////////////////////////////////////////////////////////////////////////-
  public function getAlbumthumbnail(id, propertyTable) {	
  	log:info('ZenphotoAPI.getAlbumthumbnail:', table_show(getImageComments))
  
  	$zenphotoURLroot = 'http://'.. prefs[instanceID].host..'/'
  $paramMap = initRequestParams()
  paramMap[1]['id'] = id.remoteId
  
  	$jsonResponse = $this->sendJSONRequest( 'zenphoto.get.thumbnail', paramMap, 10 )
  	log:debug('getImageComments.paramMap '..table_show(paramMap))
  	log:debug('getImageComments:'..$jsonResponse)
  	return $this->getTableFromJSON(jsonResponse, false, false)	
  }
  ////////////////////////////////////////////////////////////////////////////////-
  
  public function addComment( propertyTable, $params ) {
  log:trace('ZenphotoAPI.addComment')
  
  $paramMap = initRequestParams()
  		paramMap[1]['id'] = $params.Id
  		paramMap[1]['commentText'] = $params.commentText
  		
  		$jsonResponse = $this->sendJSONRequest( 'zenphoto.add.comment', paramMap, 10 )
  		
  		log:debug('addImageComments paramMap:', table_show(paramMap))		
  		log:debug('addImageComment:'..$jsonResponse)
  
  	return $this->getSingleValueJSON($jsonResponse)
  } 
  
  ////////////////////////////////////////////////////////////////////////////////-
  public function getImageRating( propertyTable, $params ) {
  log:trace('ZenphotoAPI.getImageRating')
  
  $paramMap = initRequestParams()
  		paramMap[1]['id'] = $params.photoId
  		$jsonResponse = $this->sendJSONRequest( 'zenphoto.get.ratings', paramMap, 10 )
  		
  		log:debug('getRating paramMap:', table_show(paramMap))
  		log:debug('getRating:', $jsonResponse)	
  			
  		$result = (json_decode( $jsonResponse))
  	
  	log:info('getImageRating.jsonresult', table_show(result))	
  	return $this->getSingleValueJSON($jsonResponse)
  } 

  
  ////////////////////////////////////////////////////////////////////////////////-
  public function chkFunction(param) {
  log:trace('ZenphotoAPI.chkFunction')
  
  //$paramMap = initRequestParams()
  	$paramMap = array() ;
  		table.insert( paramMap, { getFunction = param } )
  		$jsonResponse = $this->sendJSONRequest( 'zenphoto.chk.func', paramMap, 10 )
  		
  		log:debug('chkFunction paramMap:', table_show(paramMap))
  		log:debug('chkFunction:'..$jsonResponse)	
  		//log:debug('chkFunction:'..ZenphotoAPI.getSingleValueJSON($jsonResponse))	
  	return $this->getSingleValueJSON($jsonResponse)
  } 
  
  ////////////////////////////////////////////////////////////////////////////////-
  public function getUpdate( version ) {
  log:trace('ZenphotoAPI.getUpdate', version)
  
  	$paramMap = array() ;
  	table.insert( paramMap, { sysversion = version } )
  		$jsonResponse = $this->sendJSONRequest( 'zenphoto.get.update', paramMap, 10 )
  		
  		log:debug('getUpdate paramMap:', table_show(paramMap))
  		log:debug('getUpdate:', $jsonResponse)
  if (jsonResponse == true) {
  	$zenphotopluginURL = 'http://'..prefs[instanceID].host..'/plugins/ZenPublisher.php'
  	$responseRPC, responseHeaders = LrHttp.post( zenphotopluginURL, 'updateRPC='..prefs.getgitreply, nil, 'POST' )
  log:debug('responseHeaders',table_show(responseHeaders))
  	log:debug('responseRPC',responseRPC)
  	
  	if responseHeaders and (responseHeaders.status==500 or responseHeaders.status==401 or responseHeaders.status==400){
  	LrDialogs.message( 'ERROR '..responseHeaders.status..' Server could not be reached!', 'Please make sure that an internet connection is established and that the web service is running.', 'error' )
  	log:debug('ZenphotoAPI.getUpdate - Host Error: ', responseHeaders.status)
  	return false;
  	}	
  }		
  	
  	return responseRPC
  } 
  
  ////////////////////////////////////////////////////////////////////////////////
  
  public function deletePhoto(propertyTable, $params) {
  	log:trace('ZenphotoAPI.deletePhoto')
  
  	$paramMap = initRequestParams()
  	for key,value in pairs($params) do 
  		paramMap[1][key] = value		
  	}
  
  	$jsonResponse = $this->sendJSONRequest( 'zenphoto.image.delete', paramMap, 10 )
  		log:debug('deletePhoto:', table_show(paramMap))
  		log:debug("deletePhoto.jsonResponse: " .. $jsonResponse)
  		
  		errors = string.find(jsonResponse, "MySQL Error:")
  		if  errors {
  			log:fatal('Unable to delete image with id: ' .. paramMap[1].id, jsonResponse, ' critical')
  			return "MySQL ERROR: image ID not found or removed."
  		}
  	return $this->getSingleValueJSON($jsonResponse)
  }
  
  ////////////////////////////////////////////////////////////////////////////////
  
  public function deleteAlbum( propertyTable, albumId ) {
  	log:trace('ZenphotoAPI.deleteAlbum')
  	log:info('Delete album from server with imageId: ' .. table_show(albumId))
  
  	$paramMap = initRequestParams()
  		log:info('deleteAlbum'..table_show(paramMap))
  		paramMap[1]['id'] = albumId 	
  	$jsonResponse = $this->sendJSONRequest( 'zenphoto.album.delete', paramMap, 10 )
  	log:debug('deleteAlbum: ' ..table_show(paramMap))
  	log:debug("deleteAlbum.jsonResponse: " .. $jsonResponse)
  	return $this->getSingleValueJSON($jsonResponse)
  }
  
  ////////////////////////////////////////////////////////////////////////////////
  
  public function createAlbum( propertyTable, $params ) {
  	log:trace('ZenphotoAPI.createAlbum')
  	$paramMap = initRequestParams()
  	for key,value in pairs($params) do 
  	paramMap[1][key] = value
  	}
  	$jsonResponse = $this->sendJSONRequest( 'zenphoto.album.create', paramMap, 10 )
  	log:debug('createAlbum:', table_show(paramMap))
  	log:debug("createAlbum.jsonResponse: " .. $jsonResponse)
  	
  	return $this->getTableFromJSON($jsonResponse)
  }
  
  ////////////////////////////////////////////////////////////////////////////////
  
  public function editAlbum( propertyTable, $params ) {
  	log:trace('ZenphotoAPI.editAlbum')
  	$paramMap = initRequestParams()
  	for key,value in pairs($params) do 
  	paramMap[1][key] = value
  	}
  	
  	$jsonResponse = $this->sendJSONRequest( 'zenphoto.album.edit', paramMap, 10 )
  	log:debug('editAlbum.API:', table_show(paramMap))
  	log:debug("editAlbum.jsonResponse: " .. $jsonResponse)
  
  	return $this->getTableFromJSON($jsonResponse)
  }
  
  ////////////////////////////////////////////////////////////////////////////////
  
  public function initinstanceID( propertyTable ) {
  log:trace( 'ZenphotoAPI.initinstanceID')
  	$catalog = import 'LrApplication'.activeCatalog()
  	$publishServices = catalog:getPublishServices( _PLUGIN.id )
  log:trace( 'initinstanceID'..table_show(publishServices))
  		for i, publishService in pairs ( publishServices ) do
  			if ( publishService:getName() == propertyTable.LR_publish_connectionName) {
  				instanceID = publishService.localIdentifier
  			}
  		}
  	return instanceID
  }
  
  public function getTableFromJSON(jsonResponse, formatForUI, allowSingleEntry) {
  log:trace( 'ZenphotoAPI.getTableFromJSON Main' )
  
  	if formatForUI == nil { formatForUI = false }
  	if allowSingleEntry == nil { allowSingleEntry = true }
  
  	if not jsonResponse {
  		LrDialogs.message( 'Server could not be connected!', 'Please make sure that an internet connection is established and that the web service is running.', 'error' )
  		log:fatal('Server could not be connected!', 'Please make sure that an internet connection is established and that the web service is running.', 'error')
  		return ''
  	}
  		
  	$luaTableString = jsonResponse
  
  	if formatForUI {
  		$clear = { id = 'value', name = 'title' }
  		for key, value in pairs ( clear ) do
  			luaTableString = string.gsub(luaTableString,key,value)
  		}
  	}
  
  	log:debug('luaTableString',table_show(luaTableString))
  	resultTable = json_decode( luaTableString)
  	
    // $luaTableFunction = luaTableString and loadstring( luaTableString )
  	// $_, resultTable = LrFunctionContext.pcallWithEmptyEnvironment( luaTableFunction )
  
  if resultTable and #resultTable == 1 and allowSingleEntry == true {
  		return resultTable[1]
  	} else {
  		return resultTable
  	}
  
  }
  
  ////////////////////////////////////////////////////////////////////////////////
  
  	// handle errors and return the error code and string
  
  public function getJSONError($jsonResponse) {
  log:trace("ZenphotoAPI.getJSONError")
  
  }
  
  
  
*/

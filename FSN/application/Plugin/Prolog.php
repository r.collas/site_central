<?php
/**
 * Plugin Prolog - systeme expert 
 * Appel pour execution de programme Prolog a partir des informations en base comme base de faits/ base de regles   
 * @category Plugin Prolog
 * @package Plugin_Prolog
 * @author Cyrille GICQUEL
 * @link http://www.swi-prolog.org/pldoc/doc_for?object=manual
 * @link http://www.j-paine.org/dobbs/prolog_from_php.html
 *   
 */

class Plugin_Prolog extends Yab_Controller_Plugin_Abstract {

/**
 *
 * @params $facts = array( 'us' => array( ), 'superposition' => array() ...  )
 */     
  public function exec_prolog($facts, $rules, $request){
    
    $loader = Yab_Loader::getInstance();
    $config = $loader->getConfig();
    $config_prolog = new Yab_Config();
    $config_prolog->setFile($config->get('prolog_config'));
    $PROLOG_PATH = $config_prolog->get('path') ;
    $PROLOG_BINARY = $config_prolog->get('binary') ;    
    
    $PROLOG_BINARY_PATH =  $PROLOG_PATH.DIRECTORY_SEPARATOR.$PROLOG_BINARY ;
    $PROLOG_OPTION_PATH = dirname(YAB_PATH).DIRECTORY_SEPARATOR.'public'.DIRECTORY_SEPARATOR.'cgi-bin'.DIRECTORY_SEPARATOR ;
    // DocRef : http://www.swi-prolog.org/pldoc/doc_for?object=section%282,+%272.4%27,+swi%28%27/doc/Manual/cmdline.html%27%29%29


    $file_name = hash('crc32',date('c') );
    $dirname = 'tmp/' ;
    $prolog_arguments = '' ;
    $prolog_result = '' ;
    
    //travail dans le repertoire D:\wamp\www\Projets\fsn\fsn_test\public\tmp\
    $prolog_option_file = $PROLOG_OPTION_PATH.$file_name.'.pl';
    $prolog_arguments .= "go :-\r\n";
    $prolog_arguments .= "  go. \r\n";
    
    
    $prolog_facts = $this->arrayToPrologFacts($facts) ;
    
    // import des fait
    $prolog_arguments .= $prolog_facts ;
    
    // import des regles
    $prolog_arguments .= $rules."\r\n";
    
    // import des requetes
    $prolog_arguments .= $request."\r\n";
         
    /**
     * Test OK
     *
     */
     // $prolog_arguments = $this->_test() ;
         
    $prolog_arguments_file = fopen($prolog_option_file, 'w');
    fwrite($prolog_arguments_file, $prolog_arguments);
    fclose($prolog_arguments_file);
    
    // $cmd = $_prolog_path.DIRECTORY_SEPARATOR.$_prolog_binary.' -s '.$prolog_option_file.' -g\"$goal,halt\"";' ;
    // $cmd = '"'.$PROLOG_BINARY_PATH.'" -s '.$prolog_option_file.'  -g '.$request.' 2>&1' ;
    $cmd = '"'.$PROLOG_BINARY_PATH.'" -s '.$prolog_option_file ;
    
    exec($cmd, $result, $retval );
    
    return array('command' => $cmd,  'result' => $result, 'retval' => $retval) ;

    
  
  }
  
  function ask_prolog($request)	{

	  $PROLOG_BINARY_PATH =  self::PROLOG_PATH.DIRECTORY_SEPARATOR.self::PROLOG_BINARY ;

		$pos = strpos($request, 'prolog');


		if($pos == 0) {
			$parts = explode(" ", $request);
			$request = $parts[2];
		}



		$result = shell_exec($PROLOG_BINARY_PATH.' --quiet -f ../prolog/facts/mi_iw.pl -g '.$request.' 2>&1');
		if($parts[1] == "courses") {
			return "Les cours suivants sont offerts : ".substr($result, 0, -2).".";
		} else if($parts[1] == "modules") {
			return "Les modules suivants vous devez remplir: ".substr($result, 0, -2).".";
		} else {
			return $result;
		}
	}

	function ask_prolog_politely($request)	{
  
    $PROLOG_BINARY_PATH =  self::PROLOG_PATH.DIRECTORY_SEPARATOR.self::PROLOG_BINARY ;
    
		$result = shell_exec($PROLOG_BINARY_PATH.' --quiet -f ../prolog/facts/mi_iw.pl -g "'.$request.'" 2>&1');
		
    return $result;
	}
  
  private function _jsonisation($jsonisable){
  
    if (is_object($jsonisable)) {
      $jsonisable = $jsonisable->toArray();
    }
    
    $jsonisable = json_encode($jsonisable, JSON_NUMERIC_CHECK | JSON_UNESCAPED_SLASHES | JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE) ;
    
    return $jsonisable ;
  
  }
  
  private function _test(){
  
    $prolog_arguments = <<<EOF
      :- nl,nl,
    	write('#############################################################################'),nl,
    	write('##++++++++##++++++++++##++++++++++##++##########++++++++++##++++++++++###++##'),nl,
    	write('##++####++##++######++##++######++##++##########++######++##++######++###++##'),nl,
    	write('##++####++##++######++##++######++##++##########++######++##++###########++##'),nl,
    	write('##++####++##++######++##++######++##++##########++######++##++###########++##'),nl,
    	write('##++++++++##++++++++++##++######++##++##########++######++##++##++++++###++##'),nl,
    	write('##++########++##+++#####++######++##++##########++######++##++######++###++##'),nl,
    	write('##++########++####++####++######++##++##########++######++##++######++#######'),nl,
    	write('##++########++#####++###++++++++++##++++++++++##++++++++++##++++++++++###++##'),nl,
    	write('#############################################################################'),nl,
    	write('Test environment is running. All systems functioning within normal parameters.'),nl,
    	init,
    	write('For testing purposes you can load testcases with setup/1.'),nl,
    	write('Currently available are:'),nl,
    	write('1: Examination Rules'),nl,
    	nl,
    	write('Example: setup(1) or setup("Examinaton Rules")'),nl,
    	write('#############################################################################'),nl,nl.
      init,
    	write('For testing purposes you can load testcases with setup/1.'),nl,
    	write('Currently available are:'),nl,
    	write('1: Examination Rules'),nl,
    	nl,
    	write('Example: setup(1) or setup("Examinaton Rules")'),nl,
    	write('#############################################################################'),nl,nl.
EOF;
    
    return  $prolog_arguments ;
    
    
  
  }
  
  
  public function arrayToPrologFacts($array){
    $base_facts = "" ;
	  foreach($array as $key_entet => $value_entet ) {

		/**
		 * D�finition des Pr�dicat
		 */

      $base_facts .= "% ".$key_entet."(";
      $definition_entet = array_keys($value_entet[0] ) ;
      $index=0 ;
      foreach($definition_entet as $key_def => $value_def ){
        $base_facts .= strtoupper ( $value_def ) ;
        if( $index != sizeof($definition_entet)-1){ //Miss out last comma
  				$base_facts .= ",";
  			}
  		  $index++ ;      
      }
      $base_facts .=") \r\n";
      $array_facts = $value_entet ;
      
      /**
       *  Alimentation des lignes de faits
       */             
      foreach ( $array_facts as $lignes ) {
        $facts = $this->arrayToPrologList($lignes);
        $base_facts .= $key_entet.$facts.". \r\n" ;
      }
      
      $base_facts .= "\r" ;

    }
    
    return $base_facts;
      
  }
  //Convert an array into a prolog list [x,y,z,...]
	public function arrayToPrologList($array){
              
    $arrayString = "(";
		
		$index=0 ;
    
	  foreach($array as $key => $value ) {
      $arrayString .= "'".$value."'" ; 
      if( $index != sizeof($array)-1){ //Miss out last comma
				$arrayString .= ",";
			}
		  $index++ ;
    }
		
		$arrayString .= ")";

		return $arrayString;
		
	}
  
  public function findAnterieur($us_identifiant=null){
    
    $us_identifiant = (!empty($us_identifiant)) ? "'".$us_identifiant."'"  : "_" ;    
    
    $request = "?- findAnterieur(".$us_identifiant.")." ;
    
    return $request ;

  }
  
  public function findPosterieur($us_identifiant=null){
    
    $us_identifiant = (!empty($us_identifiant)) ? "'".$us_identifiant."'"  : "_" ;    
    
    $request = "?- findPosterieur(".$us_identifiant.")." ;
    
    return $request ;

  }
  
  public function findAllRacine($us_identifiant=null){
    $us_identifiant = (!empty($us_identifiant)) ? "'".$us_identifiant."'"  : "_" ; 
    
    $request = "?- findAllRacine(".$us_identifiant.")." ;
    
    return $request ;

  }

  public function incoherenceTemporelle($x=null,$y=null){
    $x = (!empty($x)) ? "'".$x."'"  : "_" ;
    $y = (!empty($y)) ? "'".$y."'"  : "_" ;
    
    
    $request = "?- incoherenceTemporelle(".$x.",".$y.")." ;
    
    return $request ;

  }

      
  public function stratiProlog(){
    
    return $rules = <<<EOT
% superposition(A,P)
% synchronisation(S1,S2)

synchro(X,Y) :-
	synchronisation(X,Y).
  
synchro(X,Y) :-
	synchronisation(X,Z),
	synchro(Z,Y).

anterieur(X,Y) :-
	superposition(X,Y).
  
anterieur(X,Y) :-
	superposition(X,Z),
	anterieur(Z,Y).
  
anterieur(X,Y) :-
	synchro(X,Z),
	anterieur(Z,Y).

posterieur(X,Y) :-
	anterieur(Y,X).

incoherenceTemporelle(X,Y) :-
	anterieur(X,Y),
	posterieur(X,Y).

% Recherche des niveaux anteroposterite
% initialisation de la recherche du niveau initial
niveauPosteriorite(1,X) :-
  not(posterieur(X,_)).

% Recherche des niveaux successifs
niveauPosteriorite(N,X) :-
  superposition(Y,X),
  niveauPosteriorite(N-1,Y).
  
% Initialisation du niveau initial
niveauSynchro(1,X) :-
  synchro(X,_).

% Recherche des niveaux successifs
niveauSynchro(N,X) :-
  synchronisation(X,Y),
  niveauSynchro(N,Y).
  
  

findAnterieur(X) :-
	findall(Y, anterieur(X,Y), RESULT),
  write(RESULT).

findPosterieur(X) :-
	findall(Y, posterieur(X,Y), RESULT),
  write(RESULT).

findAllRacine(X) :-
  findall(X, niveauPosteriorite(1,X), RESULT),
  write(RESULT).
    
EOT;
  
  }
/**
 * fonction inh�rente � un Plugin du framework Yab.
 * En l'occurrence d'aucune utilit�, mais pr�sence requise
 */    
  public function preDispatch(Yab_Controller_Request $request, Yab_Controller_Response $response) { }
  public function preRender(Yab_Controller_Request $request, Yab_Controller_Response $response){ }
	public function postRender(Yab_Controller_Request $request, Yab_Controller_Response $response){}
	public function postDispatch(Yab_Controller_Request $request, Yab_Controller_Response $response){}
  
}


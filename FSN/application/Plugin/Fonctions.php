<?php

class Plugin_Fonctions extends Yab_Controller_Plugin_Abstract {
	
	public function preDispatch(Yab_Controller_Request $request, Yab_Controller_Response $response) { }
	public function preRender(Yab_Controller_Request $request, Yab_Controller_Response $response){ }
	public function postRender(Yab_Controller_Request $request, Yab_Controller_Response $response){}
	public function postDispatch(Yab_Controller_Request $request, Yab_Controller_Response $response){}
	
	
	public static function html($string)
	{
		$ret = '';
		if (!empty($string))
		{
			$ret = stripslashes(htmlspecialchars($string, ENT_QUOTES));
		}
		return $ret;
	}
	
	public static function convertitDateUsFr($dateUS)
	{
	
		if (empty($dateUS))
		{
			return $dateUS;
		}
	
		if (strpos($dateUS, "/"))
		{
			// La date est d�j� au format fran�ais
			return $dateUS;
		}
	
		$sepDateHeure = explode(" ", $dateUS);
		$dateFR = implode('/', array_reverse(explode('-', $sepDateHeure[0])));
		if (count($sepDateHeure) > 1)
		{
			// on rajoute l'heure
			$dateFR = $dateFR." ".$sepDateHeure[1];
		}
	
		return $dateFR;
	}
	
	// Fonction de conversion de la taille des fichiers(visu show multimedia)
	public static function fileSizeConvert($bytes)
	{
		$bytes = floatval($bytes);
			$arBytes = array(
				0 => array(
					"UNIT" => "TB",
					"VALUE" => pow(1024, 4)
				),
				1 => array(
					"UNIT" => "GB",
					"VALUE" => pow(1024, 3)
				),
				2 => array(
					"UNIT" => "MB",
					"VALUE" => pow(1024, 2)
				),
				3 => array(
					"UNIT" => "KB",
					"VALUE" => 1024
				),
				4 => array(
					"UNIT" => "B",
					"VALUE" => 1
				),
			);

		foreach($arBytes as $arItem)
		{
			if($bytes >= $arItem["VALUE"])
			{
				$result = $bytes / $arItem["VALUE"];
				$result = str_replace(".", "," , strval(round($result, 2)))." ".$arItem["UNIT"];
				break;
			}
		}
		
		return $result;
	}
	
	// Fonction d'interdiction de caractères spéciaux
	public static function nospecialcharacter($donnees, $field, $fieldMessage, &$errors_messages)
	{
		// appel fichier internationalisation
		$registry = Yab_Loader::getInstance() -> getRegistry();
		$i18n = $registry -> get('i18n');
		$filter_no_html = new Yab_Filter_NoHtml();
		
		if(!preg_match("#^[a-zA-Z0-9_.;\-]+$#", $donnees) && !empty($donnees)){
			$errors_messages[$field] = $filter_no_html->filter( $i18n -> say($fieldMessage) );
		}
	}
	
	// Fonction d'interdiction de virgules
	public static function novirgule($donnees, $field, $fieldMessage, &$errors_messages)
	{
		// appel fichier internationalisation
		$registry = Yab_Loader::getInstance() -> getRegistry();
		$i18n = $registry -> get('i18n');
		$filter_no_html = new Yab_Filter_NoHtml();
		
		if(preg_match("#,#", $donnees)){
			$errors_messages[$field] = $filter_no_html->filter( $i18n -> say($fieldMessage) );
		}
	}
}

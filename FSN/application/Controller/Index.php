<?php
class Controller_Index extends Yab_Controller_Action {
	
	public function actionIndex() {
            
	    /*    
        $this->forward('Dashboard_Sitefouille', 'index' );
		*/
        
        $session = $this->getSession ();
        $sitefouille_id = $session->has('sitefouille_id') ? $session->has('sitefouille_id') : null ;                
        if(empty($sitefouille_id) ) {
            $session->set ( 'forward_controller', 'index' );
            $session->set ( 'forward_action', 'index' );
            $this->forward('Sitefouille', 'Select', array(1) );    
        }
        
        $sitefouille = new Model_Sitefouille ();
		$form_sitefouille = new Form_SitefouilleSearch ( $sitefouille );
		
		$formvalue = $form_sitefouille->getValues ();
		
		if ($form_sitefouille->isSubmitted() && $form_sitefouille->isValid() ) {
			$id_sitefouille = $formvalue ['sitefouille_id'];
			$session = $this->getSession ();
			$session->set ( 'sitefouille_id', $sitefouille_id );
			$this->getSession ()->set ( 'flash_title', '' );
			$this->getSession ()->set ( 'flash_status', 'info' );
			$this->getSession ()->set ( 'flash_message', 'sitefouille as been selected' );
			$this->forward ( 'index', 'index' );
		}
		
		$this->_view->set ( 'form', $form_sitefouille );
		$this->_view->setFile ( 'View/index/index.html' );         
        
	}
	
	public function actionDebug($layout = true) {
		if (! $layout) {
			$this->getLayout ()->setEnabled ( false );
		}        
		$this->_view->setFile ( 'View/index/debug.html' );
	}
		
    public function actionSetting() {
            
        $this->_view->setFile( 'View/index/setting.html' );    
        
    } 

}
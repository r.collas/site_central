<?php

class Controller_Inclusion extends Yab_Controller_Action {

	// jfb 2016-04-21 correctif fiche mantis 230 début
	//const LABEL_HISTO = "Inclusion Us";
	const LABEL_HISTO = "Inclusion";
	// jfb 2016-04-21 correctif fiche mantis 230 fin
	
	public function actionIndex() {

		$inclusion = new Model_Inclusion();

		$inclusions = $inclusion->fetchAll();

		$this->_view->set('inclusions', $inclusions);
	}

	public function actionAdd() {

		$inclusion = new Model_Inclusion();

		$form = new Form_Inclusion($inclusion);

		if($form->isSubmitted() && $form->isValid()) {

			$inclusion->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'inclusion as been added');

			$this->forward('Inclusion', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionEdit() {

		$inclusion = new Model_Inclusion($this->_request->getParams());

		$form = new Form_Inclusion($inclusion);

		if($form->isSubmitted() && $form->isValid()) {

			$inclusion->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'inclusion as been edited');

			$this->forward('Inclusion', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionDelete() {

		$inclusion = new Model_Inclusion($this->_request->getParams());

		$inclusion->delete();

		$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'inclusion as been deleted');

		$this->forward('Inclusion', 'index');

	}

	public function actionListInclusions($layout=false, $us_id, $v_action=false){
		$this->getLayout()->setEnabled($layout);

		$inclusion = new Model_Inclusion();
		$form_inclusion = new Form_Inclusion($inclusion);

		$us_inclusions = $inclusion->getInclusions($us_id);

		$this->_view->set('form_inclusion',$form_inclusion);
		$this->_view->set('us_inclusions', $us_inclusions);
		$this->_view->set('v_action', $v_action);

		if (!$layout) { $this->_view->setFile('View/inclusion/list_simple.html') ; }
	}

}
<?php

class Controller_Classe_Naturematiere extends Yab_Controller_Action {

	public function actionIndex() {

		$classe_naturematiere = new Model_Classe_Naturematiere();

		$classe_naturematieres = $classe_naturematiere->fetchAll();

		$this->_view->set('classe_naturematieres', $classe_naturematieres);
	}

	public function actionAdd() {

		$classe_naturematiere = new Model_Classe_Naturematiere();

		$form = new Form_Classe_Naturematiere($classe_naturematiere);

		if($form->isSubmitted() && $form->isValid()) {

			$classe_naturematiere->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'classe_naturematiere as been added');

			$this->forward('Classe_Naturematiere', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionEdit() {

		$classe_naturematiere = new Model_Classe_Naturematiere($this->_request->getParams());

		$form = new Form_Classe_Naturematiere($classe_naturematiere);

		if($form->isSubmitted() && $form->isValid()) {

			$classe_naturematiere->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'classe_naturematiere as been edited');

			$this->forward('Classe_Naturematiere', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionDelete() {

		$classe_naturematiere = new Model_Classe_Naturematiere($this->_request->getParams());

		$classe_naturematiere->delete();

		$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'classe_naturematiere as been deleted');

		$this->forward('Classe_Naturematiere', 'index');

	}

}
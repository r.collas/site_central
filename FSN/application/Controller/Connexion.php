<?php
class Controller_Connexion extends Yab_Controller_Action {
        
    public function actionIndex() {
            
        $this->forward('Connexion', 'login');
    
    }

	public function actionLogin() {
        $session = $this->getSession();
        if($session->has('session'))
            $this->forward('Index', 'index');
		$user = new Model_User ();
		$form = new Form_Connexion ( $user );
		$message = '' ;
        
		$this->getLayout ()->setFile ( "View/layout_login.html" );
		
		if ($form->isSubmitted () && $form->isValid ()) {
		    
            $formvalue = $form->getValues() ;
            $login = $formvalue['login'] ;
            $password = $formvalue['mdp'] ;            	        

			 if (! empty ($login) && (! empty ( $password ))) {
                
                // on récupère les valeurs des champs
                // $login = $_REQUEST ['txt_login'];
                // $mdp = $_REQUEST ['txt_pwd'];
                
                $username = $form->getElement('login')->get('value') ;
                $password = $form->getElement('mdp')->get('value') ;
        
                // on vérifie que le login est présent dans la DB
                $donnees = $user->fetchAll()->where('login ="'.$username.'" ') ;
                
                $nb_row = $donnees->count(); 

                if ( empty($nb_row) )
                {
                    $message = 'Le login ou mot de passe incorrect <br/>';
                }
                else
                {
                    $donnees = $donnees->toRow();
        
                    // on vérifie que le mdp est bon
                    if (strlen($donnees['mdp']) != 40)  // le mdp n\'est pas crypte
                    {
                        if ($donnees ['mdp'] == $password)
                        {
                            // vérification de la définition du champ mdp de la table user
                            $results = $db->prepare ("SHOW TABLES", $db);
                            $results->execute (array());
                            if($results == false)   die("Empty database");
        
                            $tables = array();
        
                            while($champ = $results->fetch())
                            {
                                array_push($tables, $champ['0']);
                            }
        
                            foreach($tables as $table)
                            {
                                $results = $db->prepare ("DESCRIBE $table", $db);
                                $results->execute (array());
                                while($champ = $results->fetch())
                                {
                                    // le champ mdp n\'est pas définie pour cryptage
                                    if (($table == 'user') && ($champ['0'] == 'mdp') && ($champ['1'] != 'varchar(40)'))
                                    {
                                        $modif_champ_mdp = $db->prepare ("ALTER TABLE  `user` CHANGE  `mdp`  `mdp` VARCHAR( 40 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL");
                                        $modif_champ_mdp->execute (array());
        
                                        // 1er passage. On crypte le mdp du user
                                        $mdp_crypter = sha1(sha1($mdp).$mdp);
        
                                        $user_id = ($donnees['id']);
        
                                        $maj_mdp_crypter = $db->prepare ('UPDATE `user` SET `mdp`=:mdp_crypter WHERE id=:id');
                                        $maj_mdp_crypter->execute (array(':mdp_crypter' => $mdp_crypter, ':id' => $user_id));
        
                                        // lancement de la session
                                        $SID = session_name () . '=' . session_id (); // construction du SID
                                        session_start ();
                                        $_SESSION ['sessionId'] = session_id (); // variable de controle de l'existence d'une session
                                        $_SESSION ['session'] = $donnees;
                                        unset($_SESSION["session"]['mdp']);
        
                                        $_SESSION ['session_user_id'] = $donnees ['id'];
                                        $_SESSION ['session_profil_id'] = $donnees ['profil_id'];
                                        $_SESSION ['session_prenom_nom'] = "<b>Utilisateur connecté : </b>" . $donnees ['prenom'] . " " . $donnees ['nom'] . "";
                                        $_SESSION ['session_user_entiteadmin_id'] = $donnees ['entiteadmin_id'];
                                        $sf = (new Model_Sitefouille())->getVisibleSitefouilles($donnees ['entiteadmin_id'])->setKey('id')->setValue('nom_nomabrege')->toArray();
                                        asort($sf);
                                        reset($sf);
                                        $_SESSION ['sitefouille_id'] = key($sf);
                                        $_SESSION ['pb_droit'] = 0;
        
                                        $this->forward('Index', 'index'); // appel de la page menu
                                        exit ();
                                    }
                                    // le champ mdp est déjà définie pour cryptage, mais le mdp n'est pas crypte
                                    if (($table == 'user') && ($champ['0'] == 'mdp') && ($champ['1'] == 'varchar(40)'))
                                    {
                                        $mdp_crypter = sha1(sha1($mdp).$mdp);
        
                                        $user_id = ($donnees['id']);
        
                                        $maj_mdp_crypter = $db->prepare ('UPDATE `user` SET `mdp`=:mdp_crypter WHERE id=:id');
                                        $maj_mdp_crypter->execute (array(':mdp_crypter' => $mdp_crypter, ':id' => $user_id));
        
                                        // lancement de la session
                                        $SID = session_name () . '=' . session_id (); // construction du SID
                                        session_start ();
                                        $_SESSION ['sessionId'] = session_id (); // variable de controle de l'existence d'une session
                                        $_SESSION ['session'] = $donnees;
                                        unset($_SESSION["session"]['mdp']);
        
                                        $_SESSION ['session_user_id'] = $donnees ['id'];
                                        $_SESSION ['session_profil_id'] = $donnees ['profil_id'];
                                        $_SESSION ['session_prenom_nom'] = "<b>Utilisateur connecté : </b>" . $donnees ['prenom'] . " " . $donnees ['nom'] . "";
                                        $_SESSION ['session_user_entiteadmin_id'] = $donnees ['entiteadmin_id'];
                                        $sf = (new Model_Sitefouille())->getVisibleSitefouilles($donnees ['entiteadmin_id'])->setKey('id')->setValue('nom_nomabrege')->toArray();
                                        asort($sf);
                                        reset($sf);
                                        $_SESSION ['sitefouille_id'] = key($sf);
                                        $_SESSION ['pb_droit'] = 0;
        
                                        $this->forward('Index', 'index'); // appel de la page menu
                                        exit ();
                                    }
                                }
                            }
                        }
                        else
                        {
                            $message .= 'Le login ou mot de passe incorrect <br/>';
                        }
                    }
        
                    if (strlen($donnees['mdp']) == 40)  // le mdp est crypte
                    {
                        $mdp_crypter = sha1(sha1($password).$password);
        
                        if ($donnees ['mdp'] == $mdp_crypter)
                        {
                            // lancement de la session
                            $SID = session_name () . '=' . session_id (); // construction du SID
                            session_start ();
                            $_SESSION ['sessionId'] = session_id (); // variable de controle de l'existence d'une session
                            $_SESSION ['session'] = $donnees;
                            unset($_SESSION["session"]['mdp']);
        
                            $_SESSION ['session_user_id'] = $donnees ['id'];
                            $_SESSION ['session_profil_id'] = $donnees ['profil_id'];
                            $_SESSION ['session_prenom_nom'] = "<b>Utilisateur connecté : </b>" . $donnees ['prenom'] . " " . $donnees ['nom'] . "";
                            $_SESSION ['session_user_entiteadmin_id'] = $donnees ['entiteadmin_id'];
                            $sf = (new Model_Sitefouille())->getVisibleSitefouilles($donnees ['entiteadmin_id'])->setKey('id')->setValue('nom_nomabrege')->toArray();
                            asort($sf);
                            reset($sf);
                            $_SESSION ['sitefouille_id'] = key($sf);
                            $_SESSION ['pb_droit'] = 0;
        
                            $this->forward('Index', 'index'); // appel de la page menu
                            exit ();
                        }
                        else
                        {
                            $message = 'Le login ou mot de passe incorrect <br/>';
                        }
                    }
                }
            }
            else
            {
                $message =  'le login et/ou le mdp n\'a pas ete saisi.<br/>';
            }
            
            if ( !empty( $message )) {
                    
                $this->getSession()->set('flash_title', '');
                $this->getSession()->set('flash_status', 'info');
                $this->getSession()->set('flash_message', $message );

            }

		}

		$this->_view->setFile ( 'View/connexion/login.html' );
		$this->_view->set('helper_form', new Yab_Helper_Form($form));
	}

    public function actionLogout() {
        
        // destruction des variables de la session
        session_unset();
        session_destroy();
        
        $this->forward('Connexion', 'login');   
  
  }

}
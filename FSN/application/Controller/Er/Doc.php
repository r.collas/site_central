<?php

class Controller_Er_Doc extends Yab_Controller_Action {

	public function actionIndex() {

		$er_doc = new Model_Er_Doc();

		$er_docs = $er_doc->fetchAll();

		$this->_view->set('er_docs', $er_docs);
	}

	public function actionAdd() {

		$er_doc = new Model_Er_Doc();

		$form = new Form_Er_Doc($er_doc);

		if($form->isSubmitted() && $form->isValid()) {

			$er_doc->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'er_doc as been added');

			$this->forward('Er_Doc', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionEdit() {

		$er_doc = new Model_Er_Doc($this->_request->getParams());

		$form = new Form_Er_Doc($er_doc);

		if($form->isSubmitted() && $form->isValid()) {

			$er_doc->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'er_doc as been edited');

			$this->forward('Er_Doc', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionDelete() {

		$er_doc = new Model_Er_Doc($this->_request->getParams());

		$er_doc->delete();

		$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'er_doc as been deleted');

		$this->forward('Er_Doc', 'index');

	}

}
<?php

class Controller_Er_Naturematiere extends Yab_Controller_Action {

	public function actionIndex() {

		$er_naturematiere = new Model_Er_Naturematiere();

		$er_naturematieres = $er_naturematiere->fetchAll();

		$this->_view->set('er_naturematieres', $er_naturematieres);
	}

	public function actionAdd() {

		$er_naturematiere = new Model_Er_Naturematiere();

		$form = new Form_Er_Naturematiere($er_naturematiere);

		if($form->isSubmitted() && $form->isValid()) {

			$er_naturematiere->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'er_naturematiere as been added');

			$this->forward('Er_Naturematiere', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionEdit() {

		$er_naturematiere = new Model_Er_Naturematiere($this->_request->getParams());

		$form = new Form_Er_Naturematiere($er_naturematiere);

		if($form->isSubmitted() && $form->isValid()) {

			$er_naturematiere->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'er_naturematiere as been edited');

			$this->forward('Er_Naturematiere', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionDelete() {

		$er_naturematiere = new Model_Er_Naturematiere($this->_request->getParams());

		$er_naturematiere->delete();

		$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'er_naturematiere as been deleted');

		$this->forward('Er_Naturematiere', 'index');

	}

}
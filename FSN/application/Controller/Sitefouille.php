<?php

class Controller_Sitefouille extends Yab_Controller_Action {
		
	public function actionIndex() {

		$sitefouille = new Model_Sitefouille();
		$sitefouilles = $sitefouille->fetchAll();
		$this->_view->set('sitefouilles_index', $sitefouilles);
	}

	public function actionAdd() {
			
		// appel fichier internationalisation
		$registry = Yab_Loader::getInstance() -> getRegistry();
		$i18n = $registry -> get('i18n');
		$filter_no_html = new Yab_Filter_NoHtml();
		
        $mode = self::ACTION_MODE_CREATE ;
		$errors_messages = '';
		
		$sitefouille = new Model_Sitefouille();
        $form = new Form_Sitefouille($sitefouille);
        $formvalue = $form->getValues();
        		
		if($form->isSubmitted()){	

			// Ajout du GUUID 
			$generateGuuid = new Plugin_Guuid() ;
			$guuid = $generateGuuid->GetUUID() ;

			try{
				
				// Recuperation entiteadmin_id dans variables de session
				$user_session= $this->getSession()->get('session') ;
				$entiteadmin_id= $user_session['entiteadmin_id'];
				$formvalue['id']= $guuid;
				$formvalue['fsn_entiteadmin_id']= $entiteadmin_id;
			
				// Control champs requis
				function isNumber($idElt, &$formvalue, &$form, &$errors_messages, $message){
					$eltvalue = addslashes($formvalue[$idElt]);
					if(!empty($eltvalue)){
						if($form->getElement($idElt)->getErrors()) $errors_messages[$idElt] = $message;
					}
				}
				function isUnique($idElt, &$formvalue, &$sitefouille, &$errors_messages, $message){
					$eltvalue = addslashes($formvalue[$idElt]);
					if(!empty($eltvalue)){	
						$req = $sitefouille->fetchAll()->where(addslashes($idElt).' ="'.$eltvalue.'" ');
						$nb_row = $req->count();
						if($nb_row > 0){
							$errors_messages[$idElt] = $message;
						}
					}
				}			
			
				$errors = $form->getErrors();
				unset($errors['surfaceestime']); unset($errors['lambert_x']); unset($errors['lambert_y']); unset($errors['altitude']);
				if($errors){
					$errors_messages[] = $filter_no_html->filter( $i18n -> say('verror_requisAll') );
				}
				isNumber('surfaceestime', $formvalue, $form, $errors_messages, $filter_no_html->filter( $i18n -> say('sitefouille_requisSurface') ));	
				isNumber('lambert_x', $formvalue, $form, $errors_messages, $filter_no_html->filter( $i18n -> say('sitefouille_requisLambertX') ));	
				isNumber('lambert_y', $formvalue, $form, $errors_messages, $filter_no_html->filter( $i18n -> say('sitefouille_requisLambertY') ));	
				isNumber('altitude', $formvalue, $form, $errors_messages, $filter_no_html->filter( $i18n -> say('sitefouille_requisAltitude') ));	
				isNumber('datation_debut', $formvalue, $form, $errors_messages, $filter_no_html->filter( $i18n -> say('sitefouille_requisDatation_debut') ));	
				isNumber('datation_fin', $formvalue, $form, $errors_messages, $filter_no_html->filter( $i18n -> say('sitefouille_requisDatation_fin') ));	
				
				isUnique('nom', $formvalue, $sitefouille, $errors_messages, $filter_no_html->filter( $i18n -> say('sitefouille_uniciteNom') ));
				isUnique('nomabrege', $formvalue, $sitefouille, $errors_messages, $filter_no_html->filter( $i18n -> say('sitefouille_uniciteNomAbrege') ));
				isUnique('numerosite', $formvalue, $sitefouille, $errors_messages, $filter_no_html->filter( $i18n -> say('sitefouille_uniciteNumeroSite') ));
				
				if($formvalue['datation_debut'] == 0 || $formvalue['datation_fin'] == 0){
					$errors_messages['datation_debut'] = $filter_no_html->filter( $i18n -> say('sitefouille_nullDatation') );
				}
				if(is_numeric($formvalue['datation_debut']) && is_numeric($formvalue['datation_fin']) && $formvalue['datation_debut'] > $formvalue['datation_fin']){
					$errors_messages['datation_debut'] = $filter_no_html->filter( $i18n -> say('sitefouille_superieurDatation') );
				}
				if(is_numeric($formvalue['datation_debut']) && $formvalue['datation_debut'] < -5000){
					$errors_messages['datation_debut'] = $filter_no_html->filter( $i18n -> say('sitefouille_limiteDatation_debut') );
				}
				if(is_numeric($formvalue['datation_fin']) && $formvalue['datation_fin'] > date('Y')){
					$errors_messages['datation_fin'] = $filter_no_html->filter( $i18n -> say('sitefouille_limiteDatation_fin') );
				}
				
				//Caractères spéciaux interdits: Trois param($donnees, $field, $fieldMessage, $errors_messages)
				Plugin_Fonctions::nospecialcharacter($formvalue['nomabrege'], 'nomabrege', 'sitefouille_nopecialcaractere', $errors_messages);
				
				if(!empty($errors_messages)) throw new Exception();
				
				$sitefouille->populate($formvalue)->save();	
				
				//Debut création arborescence multimédia 				
					//Recuperation nomabrege de l'organisme
					$user_session= $this->getSession()->get('session');
					$entiteadmin_id= $user_session['entiteadmin_id'];
					
					$organismes = $sitefouille->getEntiteOrganisme()->where('enti.id="'.$entiteadmin_id.'" ');
					$nb_row = $organismes->count();
					
					if($nb_row > 0){						
						foreach($organismes as $organisme){								
							$arborescence = '../mediatheque/albums/'.$organisme['nomabrege_organisme'].'/sitefouille/'.$formvalue['nomabrege'];
						}

						if(!is_dir($arborescence)){
							mkdir($arborescence, 0777, true);
						}
					}				
				//Fin création arborescence multimédia 
				
				// Historisation de la modif
				$id_sitefouille = $sitefouille->get('id');
				$formvalue['id'] = $id_sitefouille ;
				$formvalue['fsn_entiteadmin_id'] = (int) $entiteadmin_id; // jfb 2016-05-03 mantis 259
				$historisation = new Plugin_Historique() ;
				
				$formvalue = $form->getTypedValues($formvalue);
				$historisation->addhistory('Sitefouille', self::MODE_CREATE,$formvalue) ;

				// jfb 2016-06-03 mantis 261 début
				// Lorsque l'utilisateur crée un nouveau site, ce site devient le site en cours (alimentation de la variable de session par ce nouveau site)
				$this->getSession()->set('sitefouille_id', $id_sitefouille);
				// jfb 2016-06-03 mantis 261 fin
				
				$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'sitefouille as been added');
				$this->forward('Sitefouille', 'index');
			}catch(Exception $e){
				$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'warning'); $this->getSession()->set('flash_message', '');
				$this->getSession()->setFlashErrors($errors_messages);
			}
		}
		//$this->_view->set('helper_form', new Yab_Helper_Form($form));
		$this->_view->set('helper_form', $form);
	}
	
	public function actionShow(){
		
		$mode = self::ACTION_MODE_SHOW ;

		$sitefouille = new Model_Sitefouille($this->_request->getParams());
        $form = new Form_Sitefouille($sitefouille);
		$formvalue= $form->getValues();
		$id_sitefouille = $sitefouille->get('id');
		
		$releves = $sitefouille->getTable('Model_Releve')->fetchAll()->where('sitefouille_id="'.$id_sitefouille.'"');
		$nb_row = $releves->count();
		
		if($nb_row > 0){
			$idCroquis = Array();
			foreach($releves as $releve){
				$idCroquis[] = $releve->get('croquis_id');
			}
		}
		else		
			$idCroquis[] = "";
		
		$sitefouille->populate($formvalue)->save();	
				
		//Recuperation nomabrege de l'organisme
		$user_session= $this->getSession()->get('session');
		$entiteadmin_id= $user_session['entiteadmin_id'];
		
		$organismes = $sitefouille->getEntiteOrganisme()->where('enti.id="'.$entiteadmin_id.'" ');
		$nb_row = $organismes->count();
		
		if($nb_row > 0){						
			foreach($organismes as $organisme){			
				$codeorganisme = $organisme['nomabrege_organisme'];
			}
		}
		else 				
			$codeorganisme = "UASD";
				
		//$this->_view->set('helper_form', new Yab_Helper_Form($form));
		$this->_view->set('helper_model', $sitefouille);
		$this->_view->set('helper_form', $form);
		$this->_view->set('idCroquis', $idCroquis);
		$this->_view->set('codesite', $formvalue['nomabrege']);
		$this->_view->set('codeorganisme', $codeorganisme);
	}
			
	public function actionEdit() {
		
		// appel fichier internationalisation
		$registry = Yab_Loader::getInstance() -> getRegistry();
		$i18n = $registry -> get('i18n');
		$filter_no_html = new Yab_Filter_NoHtml();
				
		$mode = self::ACTION_MODE_UPDATE ;
		$errors_messages = '';
		
		$sitefouille_associe = '';
		
        $sitefouille = new Model_Sitefouille($this->_request->getParams());
		$form = new Form_Sitefouille($sitefouille);
        $formvalue= $form->getValues();
		$id = $this->_request->getParams();

		if($form->isSubmitted()) { 	
		
			try{
				// Blocage modification nomabrege si site rélié à Us/ER 
				$lastformvalues = $sitefouille->fetchAll()->where('id="'.$id[0].'"');
				$nb_row = $lastformvalues->count();
				if($nb_row > 0){
					foreach($lastformvalues as $lastformvalue){
						if($formvalue['nomabrege'] != $lastformvalue['nomabrege']){
							$uses = $sitefouille->getTable('Model_Us')->fetchAll()->where('sitefouille_id="'.$id[0].'"');
							$lien_sitefouille_us = $uses->count();
							
							if($lien_sitefouille_us > 0){
								$errors_messages['nomabrege'] = $filter_no_html->filter( $i18n -> say('sitefouille_associe') );
							}
						}
					}
				}

				// Control champs requis
				function isNumber($idElt, &$formvalue, &$form, &$errors_messages, $message){
					$eltvalue = addslashes($formvalue[$idElt]);
					if(!empty($eltvalue)){
						if($form->getElement($idElt)->getErrors()) $errors_messages[$idElt] = $message;
					}
				}
				function isUnique($idElt, &$formvalue, &$sitefouille, &$id, &$errors_messages, $message){
					$eltvalue = addslashes($formvalue[$idElt]);
					if(!empty($eltvalue)){		
						$req = $sitefouille->fetchAll()->where(addslashes($idElt).' ="'.$eltvalue.'" and id !="'.$id[0].'" ');
						$nb_row = $req->count();
						if($nb_row > 0){
							$errors_messages[$idElt] = $message;
						}
					}
				}
								
				$errors = $form->getErrors();
				unset($errors['surfaceestime']); unset($errors['lambert_x']); unset($errors['lambert_y']); unset($errors['altitude']);
				if($errors){
					$errors_messages[] = $filter_no_html->filter( $i18n -> say('verror_requisAll') );
				}
				
				isNumber('surfaceestime', $formvalue, $form, $errors_messages, $filter_no_html->filter( $i18n -> say('sitefouille_requisSurface') ));	
				isNumber('lambert_x', $formvalue, $form, $errors_messages, $filter_no_html->filter( $i18n -> say('sitefouille_requisLambertX') ));	
				isNumber('lambert_y', $formvalue, $form, $errors_messages, $filter_no_html->filter( $i18n -> say('sitefouille_requisLambertY') ));	
				isNumber('altitude', $formvalue, $form, $errors_messages, $filter_no_html->filter( $i18n -> say('sitefouille_requisAltitude') ));	
				isNumber('datation_debut', $formvalue, $form, $errors_messages, $filter_no_html->filter( $i18n -> say('sitefouille_requisDatation_debut') ));	
				isNumber('datation_fin', $formvalue, $form, $errors_messages, $filter_no_html->filter( $i18n -> say('sitefouille_requisDatation_fin') ));	

				isUnique('nom', $formvalue, $sitefouille, $id, $errors_messages, $filter_no_html->filter( $i18n -> say('sitefouille_uniciteNom') ));
				isUnique('nomabrege', $formvalue, $sitefouille, $id, $errors_messages, $filter_no_html->filter( $i18n -> say('sitefouille_uniciteNomAbrege') ));
				isUnique('numerosite', $formvalue, $sitefouille, $id, $errors_messages, $filter_no_html->filter( $i18n -> say('sitefouille_uniciteNumeroSite') )); 
				
				if($formvalue['datation_debut'] == 0 || $formvalue['datation_fin'] == 0){
					$errors_messages['datation_debut'] = $filter_no_html->filter( $i18n -> say('sitefouille_nullDatation') );
				}
				if(is_numeric($formvalue['datation_debut']) && is_numeric($formvalue['datation_fin']) && $formvalue['datation_debut'] > $formvalue['datation_fin']){
					$errors_messages['datation_debut'] = $filter_no_html->filter( $i18n -> say('sitefouille_superieurDatation') );
				}
				if(is_numeric($formvalue['datation_debut']) && $formvalue['datation_debut'] < -5000){
					$errors_messages['datation_debut'] = $filter_no_html->filter( $i18n -> say('sitefouille_limiteDatation_debut') );
				}
				if(is_numeric($formvalue['datation_fin']) && $formvalue['datation_fin'] > date('Y')){
					$errors_messages['datation_fin'] = $filter_no_html->filter( $i18n -> say('sitefouille_limiteDatation_fin') );
				}
				
				//Caractères spéciaux interdits: Trois param($donnees, $field, $fieldMessage, $errors_messages)
				Plugin_Fonctions::nospecialcharacter($formvalue['nomabrege'], 'nomabrege', 'sitefouille_nopecialcaractere', $errors_messages);
				
				if(!empty($errors_messages)) throw new Exception();

				$sitefouille->populate($formvalue)->save();	
								
				//Debut création arborescence multimédia 				
					//Recuperation nomabrege de l'organisme
					$user_session= $this->getSession()->get('session');
					$entiteadmin_id= $user_session['entiteadmin_id'];
					
					$organismes = $sitefouille->getEntiteOrganisme()->where('enti.id="'.$entiteadmin_id.'" ');
					$nb_row = $organismes->count();
					
					if($nb_row > 0){						
						foreach($organismes as $organisme){								
							$arborescence = '../mediatheque/albums/'.$organisme['nomabrege_organisme'].'/sitefouille/'.$formvalue['nomabrege'];
						}

						if(!is_dir($arborescence)){
							mkdir($arborescence, 0777, true);
						}
					}				
				//Fin création arborescence multimédia 
				
				// Historisation de la modif
				$id_sitefouille = $sitefouille->get('id');
				$formvalue['id'] = $id_sitefouille ;
				// jfb 2016-05-02 mantis 257 début
				$fsn_entiteadmin_id = $sitefouille->get('fsn_entiteadmin_id');
				$formvalue['fsn_entiteadmin_id'] = (int) $fsn_entiteadmin_id; // jfb 2016-05-03 mantis 259
				// jfb 2016-05-02 mantis 257 fin
				$historisation = new Plugin_Historique() ;
				
				$formvalue = $form->getTypedValues($formvalue);
				$historisation->addhistory('Sitefouille', self::MODE_UPDATE, $formvalue) ;	

				$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'sitefouille as been edited');
				
				$this->forward('Sitefouille', 'index');
			}catch(Exception $e){
				$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'warning'); $this->getSession()->set('flash_message', '');
				$this->getSession()->setFlashErrors($errors_messages);
			}
		}
		
		//$this->_view->set('helper_form', new Yab_Helper_Form($form));
		$this->_view->set('helper_form', $form);

	}
  
    public function actionSelect($layout=true, $sitefouille_id=null, $controller=null, $action=null) {
            
        $loader = Yab_Loader::getInstance();
        $request = $loader->getRequest() ;
        $session = $loader->getSession() ;
        
        $this->getLayout()->setEnabled($layout);
        
        $sitefouille = new Model_Sitefouille ();
        $form_sitefouille = new Form_SitefouilleSearch ( $sitefouille );
        
        $forward_controller = !empty($controller) ? $controller : $session->get('forward_controller');
        $forward_action = !empty($action) ? $action : $session->get('forward_action');
        if ($form_sitefouille->isSubmitted() ) {

            $formvalue = $form_sitefouille->getValues ();
            
            if($form_sitefouille->isValid() ){
                $sitefouille_id = $formvalue ['sitefouille_id'];
                $session = $this->getSession ();
                $session->set ( 'sitefouille_id', $sitefouille_id );
                $this->getSession ()->set ( 'flash_title', '' );
                $this->getSession ()->set ( 'flash_status', 'info' );
                $this->getSession ()->set ( 'flash_message', 'sitefouille as been selected' );  
            }
               
            $this->forward ( $forward_controller, $forward_action );
        }
        
        $this->_view->set ( 'form', $form_sitefouille );
    }

	public function actionSelection($layout=false){

		$this->getLayout()->setEnabled($layout);
		$msf = new Model_Sitefouille();
		$sf_list = $msf->getVisibleSitefouilles()->setKey('id')->setValue('nom_nomabrege')->toArray();
		$current_sf = Yab_Loader::getInstance()->getSession()->get('sitefouille_id');
		$id_select = "select_sf";
		
		if(isset($_POST[$id_select])){
			$this->getSession()->set('sitefouille_id', $_POST[$id_select]);
			$this->getSession ()->set ( 'flash_title', '' );
			$this->getSession ()->set ( 'flash_status', 'info' );
			$this->getSession ()->set ( 'flash_message', 'sitefouille as been selected' );
			
			// Concerne uniquement les ERs
			if (isset($this->getSession ()["us_id"]))
				unset($this->getSession ()["us_id"]) ;
			
			$exp_url = explode('/',$_SERVER['REQUEST_URI']);
			foreach($exp_url as $index => $segment){
				if ($segment == "public"){
					if(!empty($exp_url[$index+3]))
						$this->forward($exp_url[$index+1],$exp_url[$index+2].'/'.$exp_url[$index+3]);
					else
						$this->forward($exp_url[$index+1],$exp_url[$index+2]);
				}
			}
		}
			
		$this->_view->set('sf_list', $sf_list);
		$this->_view->set('current_sf', $current_sf);
		$this->_view->set('id_select', $id_select);

		if (!$layout) { $this->_view->setFile('View/sitefouille/select.html') ; }
	
	}
  
	public function actionVisuObj($layout=false, $sitefouille_id ) {
            
        $this->getLayout()->setEnabled($layout);
        $sitefouille = new Model_Sitefouille($sitefouille_id);
        $sitefouilles = $sitefouille->fetchAll();
        $sitefouille_id = $sitefouille->get('id') ;
        $nomabrege = $sitefouille->get('nomabrege') ;
        $this->_view->set('nomabrege', $nomabrege );
        $this->_view->set('sitefouilles', $sitefouilles);
    
	}  
	
	public function actionDelete() {

		$sitefouille = new Model_Sitefouille($this->_request->getParams());
		$form = new Form_Sitefouille($sitefouille);
		$formvalue = $form->getValues();
		
		$formvalue['id'] = $sitefouille->get('id');
		$sf_id = $formvalue['id']; // jfb 2016-04-22 fiche mantis 231

		// jfb 2016-05-02 mantis 257 début
		$fsn_entiteadmin_id = $sitefouille->get('fsn_entiteadmin_id');
		$formvalue['fsn_entiteadmin_id'] = (int) $fsn_entiteadmin_id; // jfb 2016-05-03 mantis 259
		// jfb 2016-05-02 mantis 257 fin
		
		// jfb 2016-04-18 correctif fiche mantis 99 début
		$message = ''; // init. à blanc message de deletion
		
		try {
			$sitefouille->delete(); // try delete avant historisation
		
			// historisation début
			$historisation = new Plugin_Historique();		
			$formvalue = $form->getTypedValues($formvalue);
        	$historisation->addhistory('Sitefouille', self::MODE_DELETE, $formvalue);
        	// historisation fin
			
        	// jfb 2016-04-22 fiche mantis 231 début : supprimer les lignes dans fsn_historique dont la colonne sitefouille_id est égal à l'id du site supprimé
			$fsn_historique = new Model_Fsn_Historique();
			$fsn_historique->delete("sitefouille_id = '" . $sf_id . "'") ;
			// jfb 2016-04-22 fiche mantis 231 fin
			
        	$message='sitefouille supprimé';
        } catch (Exception $e) {
       		$message = 'Suppression sitefouille impossible';
       	}
        	 
		//$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'sitefouille as been deleted');
       	$this->getSession()->set('flash_title', '');
       	$this->getSession()->set('flash_status', 'info');
       	$this->getSession()->set('flash_message', $message);
		// jfb 2016-04-18 correctif fiche mantis 99 fin
       	
		$this->forward('Sitefouille', 'index');
	}
}
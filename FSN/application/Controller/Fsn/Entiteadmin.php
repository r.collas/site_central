<?php

class Controller_Fsn_Entiteadmin extends Yab_Controller_Action {

	public function actionIndex() {
		$this->getSession ()->set('titre', 'GESTION DES ENTITES ADMINISTRATIVES');
		
		
		$fsn_entiteadmin = new Model_Fsn_Entiteadmin();

		// $fsn_entiteadmins = $fsn_entiteadmin->fetchAll();
		// $this -> getLayout() -> setFile("View/layout_fsn.html");
    	$fsn_entiteadmins = $fsn_entiteadmin->getEntiteAdministrativeDetail();   // Daniel
    			
		$this->_view->set('fsn_entiteadmins', $fsn_entiteadmins);
	}

	public function actionAdd() {
		$this->getSession ()->set('titre', "CREATION D'UNE ENTITE ADMINISTRATIVE");
		
		// $this -> getLayout() -> setFile("View/layout_fsn.html");
		$fsn_entiteadmin = new Model_Fsn_Entiteadmin();

		$form = new Form_Fsn_Entiteadmin($fsn_entiteadmin);

		if($form->isSubmitted() && $form->isValid()) {

			$fsn_entiteadmin->populate($form->getValues())->save();

			$this->getSession()->set('flash', 'fsn_entiteadmin as been added');

			$this->forward('Fsn_Entiteadmin', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionEdit() {
		
		$this->getSession ()->set('titre', 'MODIFICATION ENTITE ADMINISTRATIVE');
		// $this -> getLayout() -> setFile("View/layout_fsn.html"); // Daniel
		$fsn_entiteadmin = new Model_Fsn_Entiteadmin($this->_request->getParams());

		$form = new Form_Fsn_Entiteadmin($fsn_entiteadmin);

		if($form->isSubmitted() && $form->isValid()) {

			$fsn_entiteadmin->populate($form->getValues())->save();

			$this->getSession()->set('flash', 'fsn_entiteadmin as been edited');

			$this->forward('Fsn_Entiteadmin', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionDelete() {

		$fsn_entiteadmin = new Model_Fsn_Entiteadmin($this->_request->getParams());

		$fsn_entiteadmin->delete();

		$this->getSession()->set('flash', 'fsn_entiteadmin as been deleted');

		$this->forward('Fsn_Entiteadmin', 'index');

	}

}
<?php

class Controller_Fsn_Categorie extends Yab_Controller_Action {

	public function actionGestion($reference = null) {
  
		$fsn_categorie = new Model_Fsn_Categorie ();		
	
  	$liste = $fsn_categorie->getListReference ();

		$this->getSession ()->set('titre', 'GESTION DES LISTES DE REFERENCE');
		//$this->getLayout ()->setFile ( "View/layout_fsn.html" );
		$this->_view->set ( 'liste', $liste );
        $this->_view->set ( 'existe', '');
		$this->_view->set ( 'reference', $reference);
	}
	
	public function actionAjaxAfficher() {
    $fsn_categorie = new Model_Fsn_Categorie ();
		if (isset ( $_POST ['nameRef'] )) { $type = $_POST ['nameRef']; }			
		else { $type = null; }
    			
		$liste = $fsn_categorie->getListByType ( $type );
		$maxordre = $fsn_categorie->getMaxOrdre ( $type );
    
    $this->_view->set ( 'liste', $liste );
		$this->_view->set ( 'maxordre', $maxordre );
		
    /* Recuperation du nombre de relation possible avec cette Categorie */
    $fsn_categorie_relation = new Model_Fsn_Categorie_Relation();
    $nb_relation = $fsn_categorie_relation->getListRelations()->where('fcb.categorie_key = "'.$type.'" AND fcr.visible = 1' )->count() ;
    // print '<pre>'.$nb_relation.'</pre>' ;
    $this->_view->set ( 'nb_relation', $nb_relation );  
		
		$this->getLayout()->setEnabled(false) ;
		$this->_view->setFile('View/fsn/categorie/ajax_afficher.html') ;
	}
	
	public function actionAjaxSauver() {
		$fsn_categorie = new Model_Fsn_Categorie ();
		if (isset ( $_POST ['nameRef'] ) && ! empty ( $_POST ['nameRef'] ) && isset ( $_POST ['statut'] )) {
			$notify = null;
			$notification = null ;
			if ($_POST ['statut'] == 'liste') {
				$success = true;
			}
			
			if ($_POST ['statut'] == 'insert' && ! empty ( $_POST ['libelle'] ) && $_POST ['visible'] != '') {
				try {
					$insertion = Array ();
                    $insertion_post = explode ( "_", $_POST ["nameRef"] ) ;
					$insertion ["categorie_object"] = $insertion_post[0];
					$insertion ["categorie_type"] = $_POST ["nameRef"];
					$insertion ["categorie_key"] = $this->_calculeKey ( $_POST ["libelle"] );
					$insertion ["categorie_value"] = $_POST ['libelle'];
					$insertion ["ordre"] = $_POST ["ordreMax"] + 1;
					if ($_POST ['visible'] == 'true')
						$insertion ["visible"] = 1;
					else
					   $insertion ["visible"] = 0;
					   $existe = $fsn_categorie->isExiste($_POST["nameRef"], $this->_calculeKey($_POST["libelle"])) ;
					
					if(empty($existe)) {
						$fsn_categorie->populate( $insertion )->save() ;
            
            // Mise à jours du champs old_id
            $id_fsn_categorie = $fsn_categorie->get('id');
            $fsn_categorie->set('old_id',$id_fsn_categorie)->save() ;
             
						$notification = "nouvelle référence créée." ;
					} else
						$notification = "La référence est déjà existante." ;
					
				} catch ( Exception $e ) {
					$notification =  "Échec : la référence n'a pas été créée." ;
				}
			}
			
			if ($_POST ['statut'] == 'updateLibelle') {
				try {
					
					$update["id"] = $_POST ['id'];
					$update ["categorie_value"] = $_POST ['libelle'];
					//$update ["categorie_key"] = $this->_calculeKey ( $_POST ["libelle"] );
					
					$fsn_categorie->populate( $update)->save() ;
					$notification =  "la référence a été modifiée." ;
					/*
					$existe = $fsn_categorie->isExiste($_POST["nameRef"], $this->_calculeKey($_POST["libelle"].'azi')) ;
					
					if (empty($existe)) {
						$fsn_categorie->populate( $update)->save() ;
						$notification =  "la référence a été modifiée." ;
					} else
						$notification =  "La référence est déjà existante." ;
						*/
				} catch ( Exception $e ) {
					$notification = "Échec : la référence n'a pas été modifiée." ;
				}
			}
			
			if ($_POST ['statut'] == 'updateVisible') {
				try {
					$update["id"] = $_POST ['id'];
					if ($_POST ['visible'] == 'true')
						$update ["visible"] = 1;
					else
						$update ["visible"] = 0;
					
					$update ["ordre"] = $_POST ["ordreMax"] + 1;
					
					$updateResult = $fsn_categorie->populate( $update)->save() ;
					$notification =  "la référence a été modifiée." ;
				} catch ( Exception $e ) {
					$notification = "Échec : la référence n'a pas été modifiée." ;
				}
			}
			
			if ($_POST ['statut'] == 'updateOrdre') {
				try {
					$ordre = json_decode ( $_POST ['ordre'], true );
					
					foreach ( $ordre as $id => $v ) {
						$update ["ordre"] = $v;
						$updateResult = $fsn_categorie->update ( $update, " id = '" . $id . "' " );
					}
				} catch ( Exception $e ) {
					$notification = "Il s'est produit une erreur lors de la modification de l'ordre." ;
				}
			}
			
			if ($_POST ['statut'] == 'delete') {
				try {
					$reference = new Model_Fsn_Categorie ($_POST["id"]);
					$reference->delete() ;
					$notification = "la référence a été supprimée." ;
				} catch ( Exception $e ) {
					$notification = "Échec : la référence est utilisée par l'application." ;
				}
			}
			
			$this->actionAjaxAfficher ();
			if (!empty($notification) ) {
        $this->getSession()->set('flash_title', ''); 
  			$this->getSession()->set('flash_message', $notification);
        $this->getSession()->set('flash_status', 'success');
      } 
		}
	}
	
	public function actionAjaxRelier() {
		$fsn_categorie = new Model_Fsn_Categorie();
		$fsn_categorie_relation = new Model_Fsn_Categorie_Relation();
		
    /* Récupération de l'élément à relier */
    $element_b = $_POST["element_b"] ;
		
    $reference_categorie_type = $fsn_categorie_relation->getCategorieTypeRelation($element_b) ;
    
    /* Construction d'un tableau par type de categorie possible à mettre en relation 
    *  -> avec le détail des liens possibles (type_relation) par categorie
    *  -> avec le détail des éléments de cette categorie       
    */
    foreach($reference_categorie_type as $reference_type_value ){
      
      $element_a_type = $reference_type_value->get('element_a_type');
      $element_a_key = $reference_type_value->get('element_a_key');
      $type_relation_key = $reference_type_value->get('type_relation_key');

      /* Liste des relations existante avec element_b choisi */
      $list_relations = $fsn_categorie_relation->getListRelations()->where('element_b = '.$element_b.' AND fca.categorie_type = "'.$element_a_key.'" ' ) ;
      
      /* Liste des éléments de la catégorie "element_a" sauf lui-même  
      *  dans le cas où la categorie de element_a et element_b est la même
      */
      $element_b_key = $reference_type_value->get('element_b_key');
      $list_categorie = $fsn_categorie->getListByType($element_a_key)->where('id != "'.$element_b.'" ') ;      

      /* Liste des relations potentielles avec element_b choisi et les element de la liste détail de element_a */
      $list_potentiels = $this->_getListSansRelations($list_categorie, $list_relations) ;
      
      /* Liste des relations existante avec les parents de element_b choisi */
      $list_relations_parents = $this->_getParentsRelations($element_b) ;
      $list_potentiels = $this->_getListSansRelations($list_potentiels, $list_relations_parents) ;
      
      /* Tableau récapitulatif de toutes les infos */      
      $list_reference_type[$element_a_key] = array (
        'element_a' => $reference_type_value->get('element_a'),
        'element_a_type' => $element_a_type,
        'element_a_key' => $element_a_key,
        'element_a_value' => $reference_type_value->get('element_a_value'),
        //'reference' => $reference_type_value->get('element_a_type'),
        'reference' => $element_a_key,
        'nom' => $reference_type_value->get('element_a_value'), 
        'type_relation' => array ( 
          $type_relation_key => array (
            'type_relation' => $reference_type_value->get('type_relation'), 
          	'type_relation_key' => $type_relation_key,
          	'type_relation_value' => $reference_type_value->get('type_relation_value'),
          )
        ),
        'element_a_list_relations_parent' => $list_relations_parents,
        'element_a_list_relations' => $list_relations->toArray(),
        'element_a_list_potentiel' => $list_potentiels,
        
      );
        
    }
    
		$ordre = json_decode ( $_POST ['ordre'], true );

		$categorie_type = $fsn_categorie->getTypeById($element_b) ;
		
		if (!empty($_POST["reference"]))
			$categorie_type = $_POST["reference"] ;
		
    if ($_POST["action"] == "changerReference") {
      /* identification de "categorie_key" de la categorie choisie = categorie_a_key */
			$categorie_a_key = $_POST['reference'] ;
		} else {
      $categorie_a_key = '' ;   
    }

		if ($_POST["action"] == "supprimer") {
        /* Appel du Model_Fsn_Categorie_Relation */
        $relation_id = $_POST ['relation_id'] ;
        
        $this->_modifRelation($relation_id, null, 'delete', 'delete') ; 
        
    } else  if ($_POST["action"] == "updateVisible") {
      /* Appel du Model_Fsn_Categorie_Relation */
      $relation_id = ( !empty( $_POST ['relation_id']) )? $_POST ['relation_id'] : null ;
      $visible = ($_POST ['visible'] == 'true') ? 1 : 0 ;
      $ordre = ($fsn_categorie->getOrdreRMax($element_b, $categorie_type)+1) ;  
      
      $modification['id'] = $relation_id ;      
      $modification['visible'] = $visible;      
      // $form['ordre'] = $ordre;
      
      $this->_modifRelation($relation_id, $modification, 'edit', 'save') ; 

		} else if ($_POST["action"] == "updateDescription") {
      /* Appel du Model_Fsn_Categorie_Relation */
      $visible = ($_POST ['visible'] == 'true') ? 1 : 0 ;
      $relation_id = ( !empty( $_POST ['relation_id']) )? $_POST ['relation_id'] : null ;
      
      $modification['description'] = $_POST['description'] ;
      
      $this->_modifRelation($relation_id, $modification, 'edit', 'save') ;
      
		} else  if ($_POST["action"] == "updateOrdre") {
		/* Mise à jours de l'ordre de tri des relation */
    	foreach ( $ordre as $element_a => $v) {
				$update ["ordre"] = $v;
				$fsn_categorie_relation->update( $update, " element_b = '" . $element_b . "' AND element_a = '" . $element_a . "' " );
			}
		} 
    
    if ($_POST["action"] == "ajouter" && !empty($_POST["element_a"])) {
      /* Idenfication du type_relation_id */
			$detail_type_relation = $fsn_categorie->fetchAll()
        ->where('categorie_type = "categorie_relation" AND categorie_key ="'.$_POST['type_relation'].'" ')->toRow();
      $type_relation_id = $detail_type_relation['id'] ;
      
      $insertion["element_a"] = $_POST["element_a"] ;
			$insertion["type_relation"] = $_POST["type_relation"] ;
      $insertion["type_relation_id"] = $type_relation_id ;					
			$insertion["element_b"] = $element_b ;
      $insertion["visible"] = ($_POST["visible"] == 'true') ? 1 : 0 ;     
      $insertion["ordre"] = ($fsn_categorie->getOrdreRMax($element_b, $categorie_type)+1) ;        
			$insertion["description"] = $_POST["description"] ;				
			
      $this->_modifRelation(null, $insertion, 'create', 'save') ;			
		}
		
		/* Recuperation du nombre de relation possible avec cette Categorie */
    $relations = $fsn_categorie_relation->getListRelations()->where('fcr.element_b = '.$element_b.' ' ) ;
		
    /* Variables pour la view */
	  $this->_view->set ( 'list_reference_type', $list_reference_type);
		$this->_view->set ( 'relations', $relations);
		/*  "categorie_a_key" est egal à la "categorie_key" de la categorie choisie dont depend 
    *   "element_a" de la relation.
    *   categorie_a_key equivaut à categorie_type de element_a  */
		$this->_view->set ( 'categorie_a_key', $categorie_a_key);
    $this->_view->set ( 'categorie_type', $categorie_type);
		$this->_view->set ( 'element_b', $element_b);
	
		$this->getLayout()->setEnabled(false) ;
		$this->_view->setFile("View/fsn/categorie/ajax_relier.html") ;
	}
  
	public function actionIndex() {

		$fsn_categorie = new Model_Fsn_Categorie();

		$fsn_categories = $fsn_categorie->fetchAll();

		$this->_view->set('fsn_categories_index', $fsn_categories);
	}
  
	public function actionList() {

		$fsn_categorie = new Model_Fsn_Categorie();

		$fsn_categories = $fsn_categorie->fetchAll();

		$this->_view->set('fsn_categories', $fsn_categories);
	}
  
	public function actionListTree() {

		$fsn_categorie = new Model_Fsn_Categorie();

		$fsn_categories = $fsn_categorie->fetchAll();

    $categories = array() ;

    foreach ($fsn_categories as $value ) {
      $categorie_id =  $value->get('id');
      $categorie_object = $value->get('categorie_object');
      $categorie_type = $value->get('categorie_type');
      $categorie_key = $value->get('categorie_key');
      $categorie_value = $value->get('categorie_value'); 
      $system = $value->get('system');
      $visible = $value->get('visible');
      $ordre = $value->get('ordre'); 
      $description = $value->get('description');

      $categories[] = $value->toArray() ;   
    }
   
    $categories_tree = array();

    $categories_tree = $this->_buildTreeCategorie($categories, 'categorie_type') ;  
    
    $this->_view->set('categories', $categories_tree);

		$this->_view->set('fsn_categories', $fsn_categories);
	}
  
	public function actionAdd() {
	
		$fsn_categorie = new Model_Fsn_Categorie();
    $mode = 'add' ;
		$params = $this->_request->getParams() ;
    
    $form = new Form_Fsn_Categorie($fsn_categorie, $mode);
    $formvalue = $form->getValues();
		if($form->isSubmitted() ){ 
      
      $system = $form->getElement('system')->get('value') ;
      $visible = $form->getElement('visible')->get('value') ;
      $system = !empty($system) ? $system : 0 ;  
      $visible = !empty($visible) ? $visible : 0 ;
      
      $form->getElement('system')->set('value',$system);
      $form->getElement('visible')->set('value',$visible);
      
      if ($form->isValid()) {
  
  			$fsn_categorie->populate( $form->getValues() )->save();
        
        // Mise à jours du champs old_id
        $id_fsn_categorie = $fsn_categorie->get('id');
        $fsn_categorie->set('old_id',$id_fsn_categorie)->save() ;
        
        // Historisation de la modif
        $formvalue['id'] = $id_fsn_categorie ;
        $formvalue['old_id'] = $id_fsn_categorie ;
        $historisation = new Plugin_Historique() ;
        $historisation->addhistory('Categorie',$mode,$formvalue) ;	
  			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'fsn_categorie as been added');
  
  			$this->forward('Fsn_Categorie', 'index');
  
  		}
    }

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionEdit() {

		$fsn_categorie = new Model_Fsn_Categorie($this->_request->getParams());
    $mode = 'edit' ;
		
    $form = new Form_Fsn_Categorie($fsn_categorie,$mode);
    $formvalue = $form->getValues();
    
		if($form->isSubmitted() ){ 
      $response_value = $form->getValues() ;
      
      $system = $form->getElement('system')->get('value') ;
      $visible = $form->getElement('visible')->get('value') ;
      $system = !empty($system) ? $system : 0 ;  
      $visible = !empty($visible) ? $visible : 0 ;
      
      $form->getElement('system')->set('value',$system);
      $form->getElement('visible')->set('value',$visible);

      if ($form->isValid()) {
  
  			$fsn_categorie->populate($form->getValues())->save();
  
        // Historisation de la modif
        $id_fsn_categorie = $fsn_categorie->get('id');
        $formvalue['id'] = $id_fsn_categorie ;
        $historisation = new Plugin_Historique() ;
        $historisation->addhistory('Categorie',$mode,$formvalue) ;	
        
  			$this->getSession()->set('flash_title', ''); 
        $this->getSession()->set('flash_status', 'info'); 
        $this->getSession()->set('flash_message', 'la cat&eacute;gorie enregistr&eacute;e');
  
  			$this->forward('Fsn_Categorie', 'index');
  
  		} else {
        
        $this->getSession()->set('flash_title', ''); 
        $this->getSession()->set('flash_status', 'warning'); 
        $this->getSession()->set('flash_message', 'la cat&eacute;gorie Non enregistr&eacute;e');
        
      }
    }

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}
	
  public function actionGestionCategorie() {
    
    $this->getLayout()->setFile('View/layout_fsn.html' );
    
		$fsn_categorie = new Model_Fsn_Categorie();
    
    $formSearch = new Form_Fsn_CategorieSearch($fsn_categorie);
    
    $formSearchvalue = $formSearch->getValues() ;
    
    $categorie_type =(!empty($formSearchvalue['categorie_type']) ) ? $formSearchvalue['categorie_type'] : null ;
		
    if($formSearch->isSubmitted() && $formSearch->isValid() && !empty($categorie_type) ) {

			$fsn_categories = $fsn_categorie->fetchAll()->where('categorie_type = "'.$categorie_type.'" ');

		} else {
      $fsn_categories = null ;
    }   
    
    // $this->_view->set('helper_formSearch', new Yab_Helper_Form($formSearch));
    $this->_view->set('formSearch', $formSearch);
    
		$this->_view->set('fsn_categories', $fsn_categories);
    
    $this->getSession()->set("titre", "GESTION DES LISTES DE REFERENCE");
    
	}
  
	public function actionShow() {

		$fsn_categorie = new Model_Fsn_Categorie($this->_request->getParams());
    $mode = 'show' ;
		
    $form = new Form_Fsn_Categorie($fsn_categorie,$mode);

		if($form->isSubmitted() && $form->isValid()) {

			$fsn_categorie->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); 
      $this->getSession()->set('flash_status', 'info'); 
      $this->getSession()->set('flash_message', 'Affichage de la cat&eacute;gorie');

			$this->forward('Fsn_Categorie', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}
  
	public function actionDelete() {

		$fsn_categorie = new Model_Fsn_Categorie($this->_request->getParams());

		$fsn_categorie->delete();

		$this->getSession()->set('flash_title', ''); 
    $this->getSession()->set('flash_status', 'info'); 
    $this->getSession()->set('flash_message', 'la cat&eacute;gorie est supprim&eacute;e');

		$this->forward('Fsn_Categorie', 'index');

	}
/**
 * Publication d'uns Categorie 
 * @param id
 * @return none 
 * 
 */
  public function actionPublish() {
    
    $fsn_categorie = new Model_Fsn_Categorie($this->_request->getParams());
    $mode = 'publish' ;
		
    $form = new Form_Fsn_Categorie($fsn_categorie,$mode);
 
    $visible = $form->getElement('visible')->get('value') ;

    if ($visible == 0 ) {
      $visible = 1 ;
	    $this->getSession()->set('flash', 'Categorie visible');
      $this->getSession()->set('flash_status', 'ok');
    } else {
      $visible = 0 ;
      $this->getSession()->set('flash', 'Categorie visible');
      $this->getSession()->set('flash_status', 'ok');      
    }
    
    $fsn_categorie->set('visible',$visible)->save() ;

    $this->forward('Fsn_Categorie', 'index');
	}
  
	public function actionThesaurus($render='xml') {

		$fsn_categorie = new Model_Fsn_Categorie();

		$thesaurus_termes = $fsn_categorie->getTermeThesaurus() ;

    if($render == 'xml') {
      $thesaurus_termes_xml = new Yab_File('thesaurus.xml');

      $thesaurus = '<'.'?xml version="1.0" encoding="UTF-8" ?'.'>'."\r" ;
      $thesaurus .= "<phrases>\r" ; 
      foreach($thesaurus_termes as $terme){
        $thesaurus .= " <phrase>".$terme->get('phrase')."</phrase>\r" ;
      }
      $thesaurus .= "</phrases>\r" ; 
      $thesaurus_termes_xml->encode($thesaurus);
      $thesaurus_termes_xml->append($thesaurus);
      
      $this->_response->download($thesaurus_termes_xml);
    
    } else if ($render == 'json' ) {
      return json_encode($thesaurus, JSON_UNESCAPED_SLASHES | JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE) ;
    
    } else {
      return $thesaurus ;
    }
    
    
  }
  
  private function _buildTreeCategorie(array $elements, $element_parent = 'categorie_type') {
    // 'categorie_type' # Racine de l'arborescence des categorie  
      ini_set('xdebug.max_nesting_level', 600);
      ini_set('memory_limit', '256M');
      $branch = array();
     
      foreach ($elements as $key_element => $element) {
        // print $key_element.' '.$element['categorie_type'].' => '.$element['categorie_key'].'<br>' ; 
        if ($element['categorie_type'] == $element_parent ) {           
            if ( $element['categorie_type'] != $element['categorie_key'] ) {
              $children = $this->_buildTreeCategorie($elements, $element['categorie_key']);
              if ($children) {
                $element['sous_categories'] = $children;
              }
            } 
            $branch[] = $element;
        }
      }
  
      return $branch;
    }

	/*
  * Liste des relations potentielles entre les éléments d'une categorie et une autre
  * NON ENCORE définies.  
  */
  private function _getListSansRelations($liste, $relations) {
		
    $referencesNonReliees = is_object($liste) ? $liste->toArray() : $liste ;
    		
		foreach($referencesNonReliees as $i => $v) {
			foreach($relations as $relation) {
				if($v['id'] == $relation['element_a'])
					unset($referencesNonReliees[$i]) ;
			}
		}
		return $referencesNonReliees ;
	}

  private function _getParentsRelations($element_b) {
  
    $fsn_categorie_relation = new Model_Fsn_Categorie_Relation();
    
    $list_relations_parents = $fsn_categorie_relation->getListRelations()->where('element_a = '.$element_b ) ;
		
    $list_ascendants = array ();   
    if(!empty($list_relations_parents) ) {
      foreach($list_relations_parents as $parent ) {
        
        $element_b_parent = $parent->get('element_b') ;
        array_push($list_ascendants, array( 'element_a' => $element_b_parent) );
        $list_grand_parents = $this->_getParentsRelations($element_b_parent) ;
        foreach($list_grand_parents as $grand_parent ){
          $element_a_grand_parent = $grand_parent['element_a'] ;
          array_push($list_ascendants, array( 'element_a' => $element_a_grand_parent)  );
        }
  		}
    }		
		
		
    return $list_ascendants ;
	}    
  private function _calculeKey($str, $charset = 'utf-8') {
		$str = htmlentities ( $str, ENT_NOQUOTES, $charset );
		
		$str = preg_replace ( '#&([A-za-z])(?:acute|cedil|caron|circ|grave|orn|ring|slash|th|tilde|uml);#', '\1', $str );
		$str = preg_replace ( '#&([A-za-z]{2})(?:lig);#', '\1', $str ); // pour les ligatures e.g. '&oelig;'
		$str = preg_replace ( '#&[^;]+;#', '', $str ); // supprime les autres caract�res
		$str = preg_replace ( "#[^a-zA-Z ]#", "", $str );
		
		return str_replace ( " ", "_", strtolower($str) );
	}

  public function _modifRelation($relation_id = null, $modif=null, $mode='edit', $action='save') {

    /* Si il n'y a rien à modifier alors suppression */
    if ( !empty($relation_id) && empty($modif) ) { $action=='delete' ; }
    
    
    if ( empty($relation_id ) && $action=='delete' ) {
      /* Si suppression mais rien indiqué alors rien à supprimer */
      return ;      
    } else if(!empty($relation_id ) ) {
      /* Appel du Model_Fsn_Categorie_Relation */
      $current_categorie_relation = new Model_Fsn_Categorie_Relation($relation_id); 
      $formvalue_relation = $current_categorie_relation->toArray() ;
    } else {
      /* Appel du Model_Fsn_Categorie_Relation */
      $current_categorie_relation = new Model_Fsn_Categorie_Relation();
      $form_relation = new Form_Fsn_Categorie_Relation($current_categorie_relation, $mode);
      $formvalue_relation = $form_relation->getValues();    
    }

   /* Control des champs à modifier */ 
    if(!empty($modif) ) {
      foreach($modif as $key => $value ) {
        if(isset($formvalue_relation[$key]) ) {
          // $form_relation->getElement($key)->set('value',$value);
          $formvalue_relation[$key] = $value ;
        }                
      }
    } 

    if ($action != 'delete') {  $current_categorie_relation->populate($formvalue_relation)->save();    }
    
    /* Historisation de la modification 
    *   APRES une creation / modification
    *   AVANT une suppression    
    */ 
    $formvalue_relation['id'] = $current_categorie_relation->get('id');
    $historisation = new Plugin_Historique() ;
    $historisation->addhistory('Fsn_Categorie_Relation',$mode,$formvalue_relation) ;
  
    if ($action=='delete') {  $current_categorie_relation->delete();    } 
 
    /*
    $this->getSession()->set('flash_title', ''); 
    $this->getSession()->set('flash_status', 'info'); 
    $this->getSession()->set('flash_message', 'la Référence est modifiée');
    */
    return ;
        
  }
}
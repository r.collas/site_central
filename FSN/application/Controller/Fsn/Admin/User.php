<?php

class Controller_Fsn_Admin_User extends Yab_Controller_Action {

	public function actionIndex() {

		$fsn_admin_user = new Model_Fsn_Admin_User();

		$fsn_admin_users = $fsn_admin_user->fetchAll();

		$this->_view->set('fsn_admin_users', $fsn_admin_users);
	}

	public function actionAdd() {

		$fsn_admin_user = new Model_Fsn_Admin_User();

    $mode = 'add';
    
		$form = new Form_Fsn_Admin_User($fsn_admin_user, $mode);

		if($form->isSubmitted() && $form->isValid()) {
		  // suppression du champs du formulaire => non modification de l'enregistrement en base
      $form->remElement('last_login');
      
      // On recree le ldap_info
      /*
      $rawYabObj = $this->_getLdapUser();
      $rawYabObj->set("cn",array($form->getElement('user_login')->get('value'), 'count'=>1));
      $rawYabObj->set("givenname",array("", 'count'=>1));
      $rawYabObj->set("sn",array($form->getElement('user_name')->get('value'), 'count'=>1));
      $CAGroup = new Model_COP_ADMIN_GROUP();
      $copRead = $CAGroup->find(array('group_name' => 'g_coprice_read'));
      $ldapGroups[0] = 'CN='.$copRead['group_label'];

      $rawYabObj->set("memberof",$ldapGroups);
      $ldap_info = base64_encode( serialize($rawYabObj));
      
      $form->getElement('ldap_info')->set('value', $ldap_info);
      */

			$fsn_admin_user->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'User as been added');
			
			$this->forward('Fsn_Admin_User', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionEdit() {

		$fsn_admin_user = new Model_Fsn_Admin_User($this->_request->getParams());

    $mode = 'edit';
    
		$form = new Form_Fsn_Admin_User($fsn_admin_user, $mode);

		if($form->isSubmitted() && $form->isValid()) {
      // suppression du champs du formulaire => non modification de l'enregistrement en base
      $form->remElement('last_login');
      
			$fsn_admin_user->populate($form->getValues())->save();

      $this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'User as been edited');
			
			$this->forward('Fsn_Admin_User', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}
	public function actionShow() {

		$fsn_admin_user = new Model_Fsn_Admin_User($this->_request->getParams());

    $mode = 'show';
    
		$form = new Form_Fsn_Admin_User($fsn_admin_user, $mode);

		if($form->isSubmitted() && $form->isValid()) {
      // suppression du champs du formulaire => non modification de l'enregistrement en base
      $form->remElement('last_login');
      
			$fsn_admin_user->populate($form->getValues())->save();

      $this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'User as been edited');
			
			$this->forward('Fsn_Admin_User', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionDelete() {

		$fsn_admin_user = new Model_Fsn_Admin_User($this->_request->getParams());

		$fsn_admin_user->delete();

		$this->getSession()->set('flash', 'fsn_admin_user as been deleted');

		$this->forward('Fsn_Admin_User', 'index');

	}

}
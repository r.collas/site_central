<?php

class Controller_Fsn_Admin_Usergroup extends Yab_Controller_Action {

	public function actionIndex() {

		$fsn_admin_usergroup = new Model_Fsn_Admin_Usergroup();

		$fsn_admin_usergroups = $fsn_admin_usergroup->fetchAll();

		$this->_view->set('fsn_admin_usergroups', $fsn_admin_usergroups);
	}

	public function actionAdd() {

		$fsn_admin_usergroup = new Model_Fsn_Admin_Usergroup();

		$form = new Form_Fsn_Admin_Usergroup($fsn_admin_usergroup);

		if($form->isSubmitted() && $form->isValid()) {

			$fsn_admin_usergroup->populate($form->getValues())->save();

			$this->getSession()->set('flash', 'fsn_admin_usergroup as been added');

			$this->forward('Fsn_Admin_Usergroup', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionEdit() {

		$fsn_admin_usergroup = new Model_Fsn_Admin_Usergroup($this->_request->getParams());

		$form = new Form_Fsn_Admin_Usergroup($fsn_admin_usergroup);

		if($form->isSubmitted() && $form->isValid()) {

			$fsn_admin_usergroup->populate($form->getValues())->save();

			$this->getSession()->set('flash', 'fsn_admin_usergroup as been edited');

			$this->forward('Fsn_Admin_Usergroup', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionDelete() {

		$fsn_admin_usergroup = new Model_Fsn_Admin_Usergroup($this->_request->getParams());

		$fsn_admin_usergroup->delete();

		$this->getSession()->set('flash', 'fsn_admin_usergroup as been deleted');

		$this->forward('Fsn_Admin_Usergroup', 'index');

	}

}
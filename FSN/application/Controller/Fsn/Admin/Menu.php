<?php

class Controller_Fsn_Admin_Menu extends Yab_Controller_Action {

	public function actionIndex() {

		$fsn_admin_menu = new Model_Fsn_Admin_Menu();

		$fsn_admin_menus = $fsn_admin_menu->fetchAll();

		$this->_view->set('fsn_admin_menus', $fsn_admin_menus);
	}

	public function actionAdd() {

		$fsn_admin_menu = new Model_Fsn_Admin_Menu();

		$form = new Form_Fsn_Admin_Menu($fsn_admin_menu);

		if($form->isSubmitted() && $form->isValid()) {

			$fsn_admin_menu->populate($form->getValues())->save();

			$this->getSession()->set('flash', 'fsn_admin_menu as been added');

			$this->forward('Fsn_Admin_Menu', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionEdit() {

		$fsn_admin_menu = new Model_Fsn_Admin_Menu($this->_request->getParams());

		$form = new Form_Fsn_Admin_Menu($fsn_admin_menu);

		if($form->isSubmitted() && $form->isValid()) {

			$fsn_admin_menu->populate($form->getValues())->save();

			$this->getSession()->set('flash', 'fsn_admin_menu as been edited');

			$this->forward('Fsn_Admin_Menu', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionDelete() {

		$fsn_admin_menu = new Model_Fsn_Admin_Menu($this->_request->getParams());

		$fsn_admin_menu->delete();

		$this->getSession()->set('flash', 'fsn_admin_menu as been deleted');

		$this->forward('Fsn_Admin_Menu', 'index');

	}

}
<?php

class Controller_Analyser_Phasechrono extends Yab_Controller_Action {

	public function actionIndex() {

		$phasechrono = new Model_Phasechrono();

		$phasechronos = $phasechrono->fetchAll();

		$this->_view->set('phasechronos', $phasechronos);
	}

	public function actionAdd() {

		$phasechrono = new Model_Phasechrono();

		$form = new Form_Phasechrono($phasechrono);

		if($form->isSubmitted() && $form->isValid()) {
    
      // Ajout du GUUID 
      $generateGuuid = new Plugin_Guuid() ;
      $guuid = $generateGuuid->GetUUID() ;
      $form->setElement('id', array('type'=>'text', 'value' => $guuid ) ) ;

			$phasechrono->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'phasechrono as been added');

			$this->forward('Phasechrono', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionEdit() {

		$phasechrono = new Model_Phasechrono($this->_request->getParams());

		$form = new Form_Phasechrono($phasechrono);

		if($form->isSubmitted() && $form->isValid()) {

			$phasechrono->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'phasechrono as been edited');

			$this->forward('Phasechrono', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionDelete() {

		$phasechrono = new Model_Phasechrono($this->_request->getParams());

		$phasechrono->delete();

		$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'phasechrono as been deleted');

		$this->forward('Phasechrono', 'index');

	}

}
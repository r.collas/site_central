<?php

class Controller_Profil extends Yab_Controller_Action {

	public function actionIndex() {

		$profil = new Model_Profil();

		$profils = $profil->fetchAll();

		$this->_view->set('profils_index', $profils);
	}

	public function actionAdd() {
		
		// appel fichier internationalisation
		$registry = Yab_Loader::getInstance() -> getRegistry();
		$i18n = $registry -> get('i18n');
		$filter_no_html = new Yab_Filter_NoHtml();
		
		$errors_messages = '';
		
		$profil = new Model_Profil();
		$form = new Form_Profil($profil);
		$formvalue = $form->getValues();
		
		if($form->isSubmitted() && $form->isValid()) {
			try{
				//Trois param($donnees, $field, $fieldMessage, $errors_messages)
				Plugin_Fonctions::novirgule($formvalue['Nomprofil'], 'Nomprofil', 'novirguleinfield', $errors_messages);
				
				function isUnique($idElt, &$formvalue, &$oa, &$id, &$errors_messages, $message){
					$eltvalue = addslashes($formvalue[$idElt]);
					// if(!empty($eltvalue)){
						$req = $oa->fetchAll()->where(addslashes($idElt).' ="'.$eltvalue.'" and id !="'.$id.'" ');
						$nb_row = $req->count();
						if($nb_row > 0){
							$errors_messages[$idElt] = $message;
						}
					// }
				}
				isUnique('Nomprofil', $formvalue, $profil , $id , $errors_messages, $filter_no_html->filter( $i18n -> say('profil_uniciteNom') ));
				
				if(!empty($errors_messages)) throw new Exception();
				
				$profil->populate($form->getValues())->save();
				
				// jfb 2016-04-20 correctif fiche mantis 218 : historisation du profil début
				$id_profil = $profil->get('Id');
				$formvalue['Id'] = $id_profil;
				$historisation = new Plugin_Historique();
				$formvalue = $form->getTypedValues($formvalue);
				$historisation->addhistory('Profil', self::MODE_CREATE, $formvalue);
				// jfb 2016-04-20 correctif fiche mantis 218 : historisation du profil fin
				
				$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'profil as been added');
				$this->forward('Profil', 'index');
			}catch(Exception $e){
				$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'warning'); $this->getSession()->set('flash_message', '');
				$this->getSession()->setFlashErrors($errors_messages);
			}

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionEdit() {
		
		// appel fichier internationalisation
		$registry = Yab_Loader::getInstance() -> getRegistry();
		$i18n = $registry -> get('i18n');
		$filter_no_html = new Yab_Filter_NoHtml();
		
		$errors_messages = '';
		
		$profil = new Model_Profil($this->_request->getParams());
		$form = new Form_Profil($profil);
		$id = $profil->get('Id');
		$formvalue = $form->getValues();

		if($form->isSubmitted() && $form->isValid()) {
			
			try{
				//Trois param($donnees, $field, $fieldMessage, $errors_messages)
				Plugin_Fonctions::novirgule($formvalue['Nomprofil'], 'Nomprofil', 'novirguleinfield', $errors_messages);
				
				function isUnique($idElt, &$formvalue, &$oa, &$id, &$errors_messages, $message){
					$eltvalue = addslashes($formvalue[$idElt]);
					// if(!empty($eltvalue)){
						$req = $oa->fetchAll()->where(addslashes($idElt).' ="'.$eltvalue.'" and id !="'.$id.'" ');
						$nb_row = $req->count();
						if($nb_row > 0){
							$errors_messages[$idElt] = $message;
						}
					// }
				}
				isUnique('Nomprofil', $formvalue, $profil , $id , $errors_messages, $filter_no_html->filter( $i18n -> say('profil_uniciteNom') ));
				
				if(!empty($errors_messages)) throw new Exception();
				
				$profil->populate($form->getValues())->save();
				
				// jfb 2016-04-20 correctif fiche mantis 218 : historisation du profil début
				$id_profil = $profil->get('Id');
				$formvalue['Id'] = $id_profil;
				$historisation = new Plugin_Historique();
				$formvalue = $form->getTypedValues($formvalue);
				$historisation->addhistory('Profil', self::MODE_UPDATE, $formvalue);
				// jfb 2016-04-20 correctif fiche mantis 218 : historisation du profil fin
				
				$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'profil as been edited');
				$this->forward('Profil', 'index');
				
			}catch(Exception $e){
				$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'warning'); $this->getSession()->set('flash_message', '');
				$this->getSession()->setFlashErrors($errors_messages);
			}

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionDelete() {

		$profil = new Model_Profil($this->_request->getParams());
		
		// jfb 2016-04-20 correctif fiche mantis 218 : récupération du formulaire début
		$form = new Form_Profil($profil);
		$formvalue = $form->getValues();
		$id_profil = $profil->get('Id');
		$formvalue['Id'] = $id_profil;
		// jfb 2016-04-20 correctif fiche mantis 218 : récupération du formulaire fin
		
		// jfb 2016-04-18 correctif fiche mantis 99 début
		$message = ''; // init. à blanc message de deletion
		
		try {
			$profil->delete(); // try delete - avant historisation (cf. mantis 218)
				
			// jfb 2016-04-20 correctif fiche mantis 218 : historisation du profil début
			$historisation = new Plugin_Historique();
			$formvalue = $form->getTypedValues($formvalue);
			$historisation->addhistory('Profil', self::MODE_DELETE, $formvalue);
			// jfb 2016-04-20 correctif fiche mantis 218 : historisation du profil fin
			
			$message='profil supprimé';
		} catch (Exception $e) {
			$message = 'Suppression profil impossible';
		}
		
		//$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'profil as been deleted');
		$this->getSession()->set('flash_title', '');
		$this->getSession()->set('flash_status', 'info');
		$this->getSession()->set('flash_message', $message);
		// jfb 2016-04-18 correctif fiche mantis 99 fin
		
		$this->forward('Profil', 'index');

	}

}
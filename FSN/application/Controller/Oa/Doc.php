<?php

class Controller_Oa_Doc extends Yab_Controller_Action {

	public function actionIndex() {

		$oa_doc = new Model_Oa_Doc();

		$oa_docs = $oa_doc->fetchAll();

		$this->_view->set('oa_docs', $oa_docs);
	}

	public function actionAdd() {

		$oa_doc = new Model_Oa_Doc();

		$form = new Form_Oa_Doc($oa_doc);

		if($form->isSubmitted() && $form->isValid()) {

			$oa_doc->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'oa_doc as been added');

			$this->forward('Oa_Doc', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionEdit() {

		$oa_doc = new Model_Oa_Doc($this->_request->getParams());

		$form = new Form_Oa_Doc($oa_doc);

		if($form->isSubmitted() && $form->isValid()) {

			$oa_doc->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'oa_doc as been edited');

			$this->forward('Oa_Doc', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionDelete() {

		$oa_doc = new Model_Oa_Doc($this->_request->getParams());

		$oa_doc->delete();

		$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'oa_doc as been deleted');

		$this->forward('Oa_Doc', 'index');

	}

}
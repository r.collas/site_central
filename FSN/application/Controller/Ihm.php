<?php

class Controller_Ihm extends Yab_Controller_Action {

	public function actionIndex() {

		$ihm = new Model_Ihm();

		$ihms = $ihm->fetchAll();

		$this->_view->set('ihms_index', $ihms);
	}

	public function actionAdd() {

		$ihm = new Model_Ihm();

		$form = new Form_Ihm($ihm);

		if($form->isSubmitted() && $form->isValid()) {

			$ihm->populate($form->getValues())->save();

			$this->getSession()->set('flash', 'ihm as been added');

			$this->forward('Ihm', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionEdit() {

		$ihm = new Model_Ihm($this->_request->getParams());

		$form = new Form_Ihm($ihm);

		if($form->isSubmitted() && $form->isValid()) {

			$ihm->populate($form->getValues())->save();

			$this->getSession()->set('flash', 'ihm as been edited');

			$this->forward('Ihm', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionDelete() {

		$ihm = new Model_Ihm($this->_request->getParams());

		$ihm->delete();

		$this->getSession()->set('flash', 'ihm as been deleted');

		$this->forward('Ihm', 'index');

	}

}
<?php

class Controller_Document extends Yab_Controller_Action {

	public function actionIndex() {

		$document = new Model_Document();

		$documents = $document->fetchAll();

		$this->_view->set('documents_index', $documents);
	}
	
  public function actionCarrousel() {
  
      $document = new Model_Document();
      $documents = $document->fetchAll() ;
  		$this->_view->set('documents', $documents);
    
    /* Filtre par site de fouille */ 
    /*
    $session = $this->getSession(); 
    $sitefouille_id = $session->has('sitefouille_id') ? $session->get('sitefouille_id') : null ;

    if (!empty($sitefouille_id) ) {      
      $document = new Model_Document();
      $documents = $document->fetchAll()->where('sitefouille_id = "'.$sitefouille_id.'" ' ) ;
  		$this->_view->set('documents', $documents);  
    } else {
      $this->getSession()->set('flash_title', 'Documents'); 
      $this->getSession()->set('flash_status', 'warning'); 
      $this->getSession()->set('flash_message', 'Veuillez choisir un site de fouille');
            
      $this->getSession()->set('forward_controller', 'Document');
      $this->getSession()->set('forward_action', 'Carrousel');
      
      $this->forward('Dashboard_Sitefouille', 'List');    
    }
    return ; 
    */        
	}
  
	public function actionAdd() {

		$document = new Model_Document();

		$form = new Form_Document($document);

		if($form->isSubmitted() && $form->isValid()) {
    
      // Ajout du GUUID 
      $generateGuuid = new Plugin_Guuid() ;
      $guuid = $generateGuuid->GetUUID() ;
      $form->setElement('id', array('type'=>'text', 'value' => $guuid ) ) ;

			$document->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); 
      $this->getSession()->set('flash_status', 'info'); 
      $this->getSession()->set('flash_message', 'document as been added');

			$this->forward('Document', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionEdit() {

		$document = new Model_Document($this->_request->getParams());

		$form = new Form_Document($document);

		if($form->isSubmitted() && $form->isValid()) {

			$document->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'document as been edited');

			$this->forward('Document', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionDelete() {

		$document = new Model_Document($this->_request->getParams());

		$document->delete();

		$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'document as been deleted');

		$this->forward('Document', 'index');

	}

}
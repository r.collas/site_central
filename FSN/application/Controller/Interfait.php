<?php

class Controller_Interfait extends Yab_Controller_Action {

	public function actionIndex() {

		$interfait = new Model_Interfait();

		$interfaits = $interfait->fetchAll();

		$this->_view->set('interfaits', $interfaits);
	}
	
	public function actionListFaiFai($layout=false, $fai_id, $v_action=false) {
		
		$this->getLayout()->setEnabled($layout);
		
		$interfait = new Model_Interfait();
		$form_interfait = new Form_Interfait($interfait);
		
		$associatfai_fai = $interfait->getFai_fai()->where('fai_id ="'.$fai_id.'" || fai_id_1 ="'.$fai_id.'"');
				
		$this->_view->set('helper_form_associatfai_fai',$associatfai_fai);
		$this->_view->set('helper_form_interfait',$form_interfait);
		$this->_view->set('helper_model_interfait',$interfait);
		$this->_view->set('v_action', $v_action);
		$this->_view->set('fai_id', $fai_id);
				
		if (!$layout) { $this->_view->setFile('View/interfait/list_fai_fai_simple.html') ; }
	
	}

	public function actionAdd() {

		$interfait = new Model_Interfait();

		$form = new Form_Interfait($interfait);

		if($form->isSubmitted() && $form->isValid()) {

			$interfait->populate($form->getValues())->save();

			$this->getSession()->set('flash', 'interfait as been added');

			$this->forward('Interfait', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionEdit() {

		$interfait = new Model_Interfait($this->_request->getParams());

		$form = new Form_Interfait($interfait);

		if($form->isSubmitted() && $form->isValid()) {

			$interfait->populate($form->getValues())->save();

			$this->getSession()->set('flash', 'interfait as been edited');

			$this->forward('Interfait', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionDelete() {

		$interfait = new Model_Interfait($this->_request->getParams());

		$interfait->delete();

		$this->getSession()->set('flash', 'interfait as been deleted');

		$this->forward('Interfait', 'index');

	}

}
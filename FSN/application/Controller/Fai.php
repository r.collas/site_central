<?php

class Controller_Fai extends Yab_Controller_Action {
		
	public function actionIndex() {
		$fai = new Model_Fai();
		$fais = $fai->getFaiDetail();
		$this->_view->set('fais_index', $fais);
	}
		
	public function actionChangeIdentification() {
		
		$newInterfait = new Model_Interfait();
        $form_interfait = new Form_Interfait($newInterfait);
		
		$newFaiUS = new Model_Fai_Us();
		$param_id = $this->_request->getParams();
		
		if(isset($param_id) && !empty($param_id)){
			$fai = new Model_Fai($this->_request->getParams());
			$courant_sitefouille_id = $fai->get('sitefouille_id');
			$fai_id = $fai->get('id');
			$usesAssociees = $newFaiUS->getFai_us()->where('fai_id = "'.$param_id[0].'" ');
			$faiAssociees = $newInterfait->getFai_fai()->where('fai_id ="'.$param_id[0].'" ');
			
			$this->_view->set('courant_sitefouille_id', $courant_sitefouille_id);	
			$this->_view->set('fai_id', $fai_id);
			$this->_view->set('faitsAssociees', $faiAssociees);	
			$this->_view->set('usesAssociees', $usesAssociees);	
		}
							
		$this->_view->set('helper_model_interfait', $newInterfait);	
		$this->_view->set('helper_form_interfait', $form_interfait);	
		$this->_view->setFile('View/interfait/changeFaiIdentification.html');
	}
	
	public function actionAdd() {

		// appel fichier internationalisation
		$registry = Yab_Loader::getInstance()->getRegistry();
		$i18n = $registry -> get('i18n');
		$filter_no_html = new Yab_Filter_NoHtml();
				
		$mode = self::ACTION_MODE_CREATE ;
		$errors_messages = '';

        $fai = new Model_Fai();
		$form = new Form_Fai($fai);
			
		$newFaiUS = new Model_Fai_Us();
        $form_fai_us = new Form_Fai_Us($newFaiUS);
		
		$newInterfait = new Model_Interfait();
        $form_interfait = new Form_Interfait($newInterfait);
		
		$sitefouille = new Model_Sitefouille();		
		$sitefouilles = $sitefouille->fetchAll() ;
			
        $formvalue = $form->getValues();
		
		$session = Yab_Loader::getInstance()->getSession();
		$courant_sitefouille_id = $session->get('sitefouille_id');
		
		$types = $fai->getTable('Model_Fsn_Categorie')->fetchAll()->where('id= "'.$formvalue['typefai_id'].'" ');
		$nb_row = $types->count();
		
		//Affichage categorie_value by typefai_id
		if($nb_row > 0){
			foreach($types as $type){
				$categ_value = $type['categorie_value'];
			}
		}
		else
			$categ_value = '';
		
		
		if($form->isSubmitted()) {
			
			$categ_value = $formvalue['typefai_id'];
			
			//Recuperation typefai_id by categorie_value
			if($_POST['typefai_id'] !== null && $_POST['typefai_id'] != ''){
				$categories = $fai->getTable('Model_Fsn_Categorie')->fetchAll()->setKey('id')->where('categorie_value="'.$categ_value.'" and categorie_type like "fai%" ');
				$nb_row = $categories->count();
				
				if($nb_row > 0){
					foreach($categories as $typeid => $categorie){
						$formvalue['typefai_id'] = $typeid;
						$form->getElement('typefai_id')->set('value', $typeid);
					}	
				}
				else{
					$errors_messages['typefai_idrelations-fai_us'] = $filter_no_html->filter( $i18n -> say('fai_typeInList') );
				}
			}

			//Recuperation date plancher et plafond du site courant
			$sites = $fai->getSitefouilleByIdSite($courant_sitefouille_id);
			foreach($sites as $site){
				$date_plancher = $site['datation_debut'];
				$date_plafond = $site['datation_fin'];
			}			
			
			// Ajout du GUUID
		    $generateGuuid = new Plugin_Guuid() ;
		    $guuid = $generateGuuid->GetUUID() ;
			
			try{
			
				$formvalue['id'] = $guuid;
				
				// Control champs requis
				function isNumber($idElt, &$formvalue, &$form, &$errors_messages, $message){
					$eltvalue = addslashes($formvalue[$idElt]);
					if(!empty($eltvalue)){
						if($form->getElement($idElt)->getErrors()) $errors_messages[$idElt] = $message;
					}
				}
				function isUnique($idElt, &$formvalue, &$fai, &$errors_messages, $message){
					$eltvalue = addslashes($formvalue[$idElt]);
					if(!empty($eltvalue)){	
						$req = $fai->fetchAll()->where(addslashes($idElt).' ="'.$eltvalue.'" and sitefouille_id ="'.$formvalue['sitefouille_id'].'"');
						$nb_row = $req->count();
						if($nb_row > 0){
							$errors_messages[$idElt] = $message;
						}
					}
				}
				function limiteDatation($idElt, &$formvalue, &$date_plancher, &$date_plafond, &$errors_messages, $message){
					$eltvalue = addslashes($formvalue[$idElt]);
					if(is_numeric($eltvalue) && $eltvalue < $date_plancher || $eltvalue > $date_plafond){
						$errors_messages[$idElt] = $message;
					}
				}
				
				if($form->getElement('identification')->getErrors()){
					$errors_messages['identification'] = $filter_no_html->filter( $i18n -> say('verror_requisAll') );
				}
				isNumber('fin_fourc_hte_estim', $formvalue, $form, $errors_messages, $filter_no_html->filter( $i18n -> say('fai_requisFin_fh_aaaa') ));	
				isNumber('fin_fourc_bas_estim', $formvalue, $form, $errors_messages, $filter_no_html->filter( $i18n -> say('fai_requisFin_fb_aaaa') ));	
				isNumber('deb_fourc_hte_estim', $formvalue, $form, $errors_messages, $filter_no_html->filter( $i18n -> say('fai_requisDebut_fh_aaaa') ));	
				isNumber('deb_fourc_bas_estim', $formvalue, $form, $errors_messages, $filter_no_html->filter( $i18n -> say('fai_requisDebut_fb_aaaa') ));	
				
				isNumber('fin_fourc_hte_certain', $formvalue, $form, $errors_messages, $filter_no_html->filter( $i18n -> say('fai_requisFin_fh_aaaa') ));	
				isNumber('fin_fourc_bas_certain', $formvalue, $form, $errors_messages, $filter_no_html->filter( $i18n -> say('fai_requisFin_fb_aaaa') ));	
				isNumber('deb_fourc_hte_certain', $formvalue, $form, $errors_messages, $filter_no_html->filter( $i18n -> say('fai_requisDebut_fh_aaaa') ));	
				isNumber('deb_fourc_bas_certain', $formvalue, $form, $errors_messages, $filter_no_html->filter( $i18n -> say('fai_requisDebut_fb_aaaa') ));	
				
				isUnique('identification', $formvalue, $fai , $errors_messages, $filter_no_html->filter( $i18n -> say('fai_uniciteIdentification') ));
				
				limiteDatation('fin_fourc_hte_estim', $formvalue, $date_plancher, $date_plafond, $errors_messages, $filter_no_html->filter( $i18n -> say('fai_limiteDatation_finh') ));
				limiteDatation('fin_fourc_bas_estim', $formvalue, $date_plancher, $date_plafond, $errors_messages, $filter_no_html->filter( $i18n -> say('fai_limiteDatation_finb') ));
				limiteDatation('deb_fourc_hte_estim', $formvalue, $date_plancher, $date_plafond, $errors_messages, $filter_no_html->filter( $i18n -> say('fai_limiteDatation_debuth') ));
				limiteDatation('deb_fourc_bas_estim', $formvalue, $date_plancher, $date_plafond, $errors_messages, $filter_no_html->filter( $i18n -> say('fai_limiteDatation_debutb') ));
				
				limiteDatation('fin_fourc_hte_certain', $formvalue, $date_plancher, $date_plafond, $errors_messages, $filter_no_html->filter( $i18n -> say('fai_limiteDatation_finh') ));
				limiteDatation('fin_fourc_bas_certain', $formvalue, $date_plancher, $date_plafond, $errors_messages, $filter_no_html->filter( $i18n -> say('fai_limiteDatation_finb') ));
				limiteDatation('deb_fourc_hte_certain', $formvalue, $date_plancher, $date_plafond, $errors_messages, $filter_no_html->filter( $i18n -> say('fai_limiteDatation_debuth') ));
				limiteDatation('deb_fourc_bas_certain', $formvalue, $date_plancher, $date_plafond, $errors_messages, $filter_no_html->filter( $i18n -> say('fai_limiteDatation_debutb') ));
				
				//Caractères spéciaux interdits: Trois param($donnees, $field, $fieldMessage, $errors_messages)
				Plugin_Fonctions::nospecialcharacter($formvalue['identification'], 'identification', 'fai_nopecialcaractere', $errors_messages);
				
				if(!empty($errors_messages)) throw new Exception();
				
				$fai->populate($formvalue)->save();	
				
				//Debut création arborescence multimédia 						
					foreach($sites as $site){
						$code_sitefouille = $site['nomabrege'];
					}
					
					//Recuperation nomabrege de l'organisme
					$user_session= $this->getSession()->get('session');
					$entiteadmin_id= $user_session['entiteadmin_id'];
					
					$organismes = $fai->getEntiteOrganisme()->where('enti.id="'.$entiteadmin_id.'" ');
					$nb_row = $organismes->count();
					
					if($nb_row > 0){						
						foreach($organismes as $organisme){								
							$arborescence = '../mediatheque/albums/'.$organisme['nomabrege_organisme'].'/fai/'.$code_sitefouille.'/'.$formvalue['identification'];
						}

						if(!is_dir($arborescence)){
							mkdir($arborescence, 0777, true);
						}
					}				
				//Fin création arborescence multimédia
				
				// ajout des relations en bdd
				$historisation = new Plugin_Historique() ;
				$generateGuuid = new Plugin_Guuid();
				$title_relations = ["fai_us" => "_pfai_us_", "interfait" => "_pfai_fai_"];
				$regex_relations = array();
				foreach($title_relations as $table => $title){
					$regex_relations[$table] = "#^([a-z0-9_]+)(".$title.")([0-9]+)$#";
				}

				$relations = array();
				foreach($_POST as $key => $value){
					foreach($regex_relations as $table => $regex) {
						if (preg_match($regex, $key, $res)) {
							$relations[$table][$res[3]][$res[1]] = $value;
						}
					}
				}
				
				foreach($relations as $table => $relationsTable){
										
					foreach($relationsTable as $relation){
						
						if ($table == "fai_us"){
							$nomModel = "Model_Fai_Us";
							$nomForm = "Form_Fai_Us";
							$controller_courant = 'Fai_Us';
						}
						else{
							$nomModel = "Model_Interfait";
							$nomForm = "Form_Interfait";
							$controller_courant = 'Interfait';
						}
						
						$model = new $nomModel();
						$form_courant = new $nomForm($model);
						
						$relation['fai_id'] = $fai->get('id');
						$relation['id'] = $generateGuuid->GetUUID();
						
						$model->populate($relation)->save();
						
						// Historisation des relations						
						unset($relation['id']);
                        foreach($relation as $key => $value)
                            $form_courant->getElement($key)->set('value',$value);
                        $relation = $form_courant->getTypedValues($relation);
                        $relation['id'] = $model->get('id');
						$historisation->addhistory($controller_courant, self::MODE_CREATE, $relation) ;
					}
				}		
					
				// Historisation de la modif
				$id_fai = $fai->get('id');
				$formvalue['id'] = $id_fai ;
				
				$formvalue = $form->getTypedValues($formvalue);
				$historisation->addhistory('Fai', self::MODE_CREATE, $formvalue) ;
				
				$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); 	$this->getSession()->set('flash_message', 'fai as been added');

				$this->forward('Fai', 'index');
			}catch(Exception $e){
				$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'warning'); $this->getSession()->set('flash_message', '');
				$this->getSession()->setFlashErrors($errors_messages);
			}
			
		}

		//$this->_view->set('helper_form', new Yab_Helper_Form($form));
		$this->_view->set('helper_model_fai', $fai);
		$this->_view->set('helper_form', $form);
		$this->_view->set('helper_form_fai_us', $form_fai_us);
		$this->_view->set('helper_model_fai_us', $newFaiUS);
		$this->_view->set('helper_form_interfait', $form_interfait);
		$this->_view->set('helper_model_interfait', $newInterfait);
		$this->_view->set('sitefouilles', $sitefouilles);
		$this->_view->set('courant_sitefouille_id', $courant_sitefouille_id);
		$this->_view->set('categ_value', $categ_value);
	}

	public function actionShow(){
		
		$mode = self::ACTION_MODE_SHOW ;

		$fai = new Model_Fai($this->_request->getParams());
        $form = new Form_Fai($fai);
		$formvalue = $form->getValues();
		
		$id_fai = $fai->get('id');
		$fai_us = new Model_Fai_Us();
		$interfait = new Model_Interfait();
		
		$associatfai_us = $fai_us->getFai_us()->where('fai_id = "'.$id_fai.'" ')->toArray();
		$associatfai_fai = $interfait->getFai_fai()->where('fai_id ="'.$id_fai.'" || fai_id_1 ="'.$id_fai.'"')->toArray();
		
		function getFieldValueById(&$fai, $table, $tablecolum, $eltId, $valsearch, &$retour){
			$req = $fai->getTable($table)->fetchAll()->where(addslashes($tablecolum).'= "'.$fai->get($eltId).'" ');	
			$nb_row = $req->count();
			if($nb_row > 0){
				foreach($req as $row){
					$retour = $row[$valsearch];
				}
			} else $retour = '';
		}
		
		getFieldValueById($fai, 'Model_Sitefouille', 'id', 'sitefouille_id', 'nom', $site_name);
		getFieldValueById($fai, 'Model_Fsn_Categorie', 'id', 'typefai_id', 'categorie_value', $categ_value);	
		getFieldValueById($fai, 'Model_Sitefouille', 'id', 'sitefouille_id', 'nomabrege', $codesite);
				
		//Recuperation nomabrege de l'organisme
		$user_session= $this->getSession()->get('session');
		$entiteadmin_id= $user_session['entiteadmin_id'];
		
		$organismes = $fai->getEntiteOrganisme()->where('enti.id="'.$entiteadmin_id.'" ');
		$nb_row = $organismes->count();
		
		if($nb_row > 0){						
			foreach($organismes as $organisme){			
				$codeorganisme = $organisme['nomabrege_organisme'];
			}
		}
		else 				
			$codeorganisme = "UASD";
		
		//$this->_view->set('helper_form', new Yab_Helper_Form($form));
		$this->_view->set('helper_model_fai', $fai);
		$this->_view->set('helper_form', $form);
		$this->_view->set('site_name', $site_name);
		$this->_view->set('categ_value', $categ_value);
        $this->_view->set('fai_id', $id_fai);
		$this->_view->set('codesite', $codesite);
		$this->_view->set('codeidentification', $formvalue['identification']);
        $this->_view->set('codeorganisme', $codeorganisme);
        $this->_view->set('associatfai_us', $associatfai_us);
        $this->_view->set('associatfai_fai', $associatfai_fai);
	}
	
	public function actionEdit() {
        
		// appel fichier internationalisation
		$registry = Yab_Loader::getInstance()->getRegistry();
		$i18n = $registry -> get('i18n');
		$filter_no_html = new Yab_Filter_NoHtml();
				
		$mode = self::ACTION_MODE_UPDATE ;
		$errors_messages = '';
		
        $fai = new Model_Fai($this->_request->getParams());
		$form = new Form_Fai($fai);
		$id = $fai->get('id');
		
		$newFaiUS = new Model_Fai_Us();
        $form_fai_us = new Form_Fai_Us($newFaiUS);
		$form_fai_us->getElement('fai_id')->set('value', $id);
		
		$newInterfait = new Model_Interfait();
        $form_interfait = new Form_Interfait($newInterfait);
		$form_interfait->getElement('fai_id')->set('value', $id);
		
		$sitefouille = new Model_Sitefouille();		
		$sitefouilles = $sitefouille->fetchAll() ;
		
        $formvalue = $form->getValues();
		
		$courant_sitefouille_id = $fai->get('sitefouille_id');

		$types = $fai->getTable('Model_Fsn_Categorie')->fetchAll()->where('id= "'.$formvalue['typefai_id'].'" ');
		$nb_row = $types->count();
		
		$usesAssociees = $newFaiUS->getFai_us()->where('fai_id = "'.$id.'" ');
		$faitsAssociees = $newInterfait->getFai_fai()->where('fai_id ="'.$id.'" ');

		//Affichage categorie_value by typefai_id
		if($nb_row > 0){
			foreach($types as $type){
				$categ_value = $type['categorie_value'];
			}
		}
		else
			$categ_value = '';

		if($form->isSubmitted()) {
			
			$categ_value = $formvalue['typefai_id'];
			
			//Recuperation typefai_id by categorie_value
			if($_POST['typefai_id'] !== null && $_POST['typefai_id'] != ''){
				$categories = $fai->getTable('Model_Fsn_Categorie')->fetchAll()->setKey('id')->where('categorie_value="'.$categ_value.'" and categorie_type like "fai%" ');
				$nb_row = $categories->count();
				
				if($nb_row > 0){
					foreach($categories as $typeid => $categorie){
						$formvalue['typefai_id'] = $typeid;
						$form->getElement('typefai_id')->set('value', $typeid);
					}	
				}
				else{
					$errors_messages['typefai_idrelations-fai_us'] = $filter_no_html->filter( $i18n -> say('fai_typeInList') );
				}
			}

			//Recupération date plancher et plafond du site courant
			$sites = $fai->getSitefouilleByIdSite($courant_sitefouille_id);
			foreach($sites as $site){
				$date_plancher = $site['datation_debut'];
				$date_plafond = $site['datation_fin'];
			}			
			
			try{		

				// Control champs requis
				function isNumber($idElt, &$formvalue, &$form, &$errors_messages, $message){
					$eltvalue = addslashes($formvalue[$idElt]);
					if(!empty($eltvalue)){
						if($form->getElement($idElt)->getErrors()) $errors_messages[$idElt] = $message;
					}
				}		
				function isUnique($idElt, &$formvalue, &$fai, &$id, &$errors_messages, $message){
					$eltvalue = addslashes($formvalue[$idElt]);
					if(!empty($eltvalue)){		
						$req = $fai->fetchAll()->where(addslashes($idElt).' ="'.$eltvalue.'" and id !="'.$id.'" and sitefouille_id ="'.$formvalue['sitefouille_id'].'" ');
						$nb_row = $req->count();
						if($nb_row > 0){
							$errors_messages[$idElt] = $message;
						}
					}
				}
				function limiteDatation($idElt, &$formvalue, &$date_plancher, &$date_plafond, &$errors_messages, $message){
					$eltvalue = addslashes($formvalue[$idElt]);
					if(is_numeric($eltvalue) && $eltvalue < $date_plancher || $eltvalue > $date_plafond){
						$errors_messages[$idElt] = $message;
					}
				}
				
				if($form->getElement('identification')->getErrors()){
					$errors_messages['identification'] = $filter_no_html->filter( $i18n -> say('verror_requisAll') );
				}
				isNumber('fin_fourc_hte_estim', $formvalue, $form, $errors_messages, $filter_no_html->filter( $i18n -> say('fai_requisFin_fh_aaaa') ));	
				isNumber('fin_fourc_bas_estim', $formvalue, $form, $errors_messages, $filter_no_html->filter( $i18n -> say('fai_requisFin_fb_aaaa') ));	
				isNumber('deb_fourc_hte_estim', $formvalue, $form, $errors_messages, $filter_no_html->filter( $i18n -> say('fai_requisDebut_fh_aaaa') ));	
				isNumber('deb_fourc_bas_estim', $formvalue, $form, $errors_messages, $filter_no_html->filter( $i18n -> say('fai_requisDebut_fb_aaaa') ));	
				
				isNumber('fin_fourc_hte_certain', $formvalue, $form, $errors_messages, $filter_no_html->filter( $i18n -> say('fai_requisFin_fh_aaaa') ));	
				isNumber('fin_fourc_bas_certain', $formvalue, $form, $errors_messages, $filter_no_html->filter( $i18n -> say('fai_requisFin_fb_aaaa') ));	
				isNumber('deb_fourc_hte_certain', $formvalue, $form, $errors_messages, $filter_no_html->filter( $i18n -> say('fai_requisDebut_fh_aaaa') ));	
				isNumber('deb_fourc_bas_certain', $formvalue, $form, $errors_messages, $filter_no_html->filter( $i18n -> say('fai_requisDebut_fb_aaaa') ));	
				
				isUnique('identification', $formvalue, $fai , $id, $errors_messages, $filter_no_html->filter( $i18n -> say('fai_uniciteIdentification') ));
				
				limiteDatation('fin_fourc_hte_estim', $formvalue, $date_plancher, $date_plafond, $errors_messages, $filter_no_html->filter( $i18n -> say('fai_limiteDatation_finh') ));
				limiteDatation('fin_fourc_bas_estim', $formvalue, $date_plancher, $date_plafond, $errors_messages, $filter_no_html->filter( $i18n -> say('fai_limiteDatation_finb') ));
				limiteDatation('deb_fourc_hte_estim', $formvalue, $date_plancher, $date_plafond, $errors_messages, $filter_no_html->filter( $i18n -> say('fai_limiteDatation_debuth') ));
				limiteDatation('deb_fourc_bas_estim', $formvalue, $date_plancher, $date_plafond, $errors_messages, $filter_no_html->filter( $i18n -> say('fai_limiteDatation_debutb') ));
				
				limiteDatation('fin_fourc_hte_certain', $formvalue, $date_plancher, $date_plafond, $errors_messages, $filter_no_html->filter( $i18n -> say('fai_limiteDatation_finh') ));
				limiteDatation('fin_fourc_bas_certain', $formvalue, $date_plancher, $date_plafond, $errors_messages, $filter_no_html->filter( $i18n -> say('fai_limiteDatation_finb') ));
				limiteDatation('deb_fourc_hte_certain', $formvalue, $date_plancher, $date_plafond, $errors_messages, $filter_no_html->filter( $i18n -> say('fai_limiteDatation_debuth') ));
				limiteDatation('deb_fourc_bas_certain', $formvalue, $date_plancher, $date_plafond, $errors_messages, $filter_no_html->filter( $i18n -> say('fai_limiteDatation_debutb') ));
				
				//Caractères spéciaux interdits: Trois param($donnees, $field, $fieldMessage, $errors_messages)
				Plugin_Fonctions::nospecialcharacter($formvalue['identification'], 'identification', 'fai_nopecialcaractere', $errors_messages);					
					
				if(!empty($errors_messages)) throw new Exception();	
				
				$fai->populate($formvalue)->save();	
				
				//Debut création arborescence multimédia
					foreach($sites as $site){
						$code_sitefouille = $site['nomabrege'];
					}
					
					//Recuperation nomabrege de l'organisme					
					$user_session= $this->getSession()->get('session');
					$entiteadmin_id= $user_session['entiteadmin_id'];
					
					$organismes = $fai->getEntiteOrganisme()->where('enti.id="'.$entiteadmin_id.'" ');
					$nb_row = $organismes->count();
					
					if($nb_row > 0){						
						foreach($organismes as $organisme){								
							$arborescence = '../mediatheque/albums/'.$organisme['nomabrege_organisme'].'/fai/'.$code_sitefouille.'/'.$formvalue['identification'];
						}

						if(!is_dir($arborescence)){
							mkdir($arborescence, 0777, true);
						}
					}				
				//Fin création arborescence multimédia
				
				// ajout des relations en bdd
				$historisation = new Plugin_Historique() ;
				$this->deleteRelationsFai($fai->get('id'));				
				$generateGuuid = new Plugin_Guuid();
				
				$title_relations = ["fai_us" => "_pfai_us_", "interfait" => "_pfai_fai_"];
				$regex_relations = array();
				foreach($title_relations as $table => $title){
					$regex_relations[$table] = "#^([a-z0-9_]+)(".$title.")([0-9]+)$#";
				}

				$relations = array();
				foreach($_POST as $key => $value){
					foreach($regex_relations as $table => $regex) {
						if (preg_match($regex, $key, $res)) {
							$relations[$table][$res[3]][$res[1]] = $value;
						}
					}
				}
				foreach($relations as $table => $relationsTable){
										
					foreach($relationsTable as $relation){
						
						if ($table == "fai_us"){
							$nomModel = "Model_Fai_Us";
							$nomForm = "Form_Fai_Us";
							$controller_courant = 'Fai_Us';
							$relation['fai_id'] = $fai->get('id');							
						}
						else{
							$nomModel = "Model_Interfait";
							$nomForm = "Form_Interfait";
							$controller_courant = 'Interfait';
							
							//Si nouvelle relation interfait
							if($relation['fai_id_1'] != $fai->get('id')){							
								$relation['fai_id'] = $fai->get('id');
							}
							else{							
								$interfaits = $newInterfait->fetchAll()->where('fai_id_1 ="'.$fai->get('id').'" ');
								$nb_row = $interfaits->count();
								if($nb_row > 0){
									foreach($interfaits as $interfait)
										$relation['fai_id'] = $interfait['fai_id'];
								}								
								$relation['fai_id_1'] = $fai->get('id');
							}
						}
						
						$model = new $nomModel();	
						$form_courant = new $nomForm($model);
						
						$relation['id'] = $generateGuuid->GetUUID();						
						$model->populate($relation)->save();	
						
						// Historisation des relations						
						unset($relation['id']);
                        foreach($relation as $key => $value)
                            $form_courant->getElement($key)->set('value',$value);
                        $relation = $form_courant->getTypedValues($relation);
                        $relation['id'] = $model->get('id');
						$historisation->addhistory($controller_courant, self::MODE_CREATE, $relation) ;						
					}
				}

				// Historisation de la modif
				$id_fai = $fai->get('id');
				$formvalue['id'] = $id_fai ;
				
				$formvalue = $form->getTypedValues($formvalue);
				$historisation->addhistory('Fai', self::MODE_UPDATE, $formvalue) ;
				
				$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info');	$this->getSession()->set('flash_message', 'fai as been edited');

				$this->forward('Fai', 'index');
			}catch(Exception $e){
				$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'warning'); $this->getSession()->set('flash_message', '');
				$this->getSession()->setFlashErrors($errors_messages);
			}
			
		}

		//$this->_view->set('helper_form_edit', new Yab_Helper_Form($form));
		$this->_view->set('helper_model_fai', $fai);
		$this->_view->set('helper_form', $form);
		$this->_view->set('helper_form_fai_us', $form_fai_us);
		$this->_view->set('helper_model_fai_us', $newFaiUS);
		$this->_view->set('helper_form_interfait', $form_interfait);
		$this->_view->set('helper_model_interfait', $newInterfait);
		$this->_view->set('sitefouilles', $sitefouilles);
		$this->_view->set('courant_sitefouille_id', $courant_sitefouille_id);
		$this->_view->set('categ_value', $categ_value);
		
		$fai_id = $fai->get('id') ;
        $this->_view->set('fai_id', $fai_id);
				
		$this->_view->set('faitsAssociees',$faitsAssociees);
		$this->_view->set('usesAssociees',$usesAssociees);
	}

	private function deleteRelationsFai($fai_id){
        $histo = array();
        $historisation = new Plugin_Historique();
		
        $m_fai_us = new Model_Fai_Us();
        $histo['Fai_Us'] = $m_fai_us->getFai_us()->where('fai_id = "'.$fai_id.'" ')->toArray();
		
		$m_interfait = new Model_Interfait();
        $histo['Interfait'] = $m_interfait->getFai_fai()->where('fai_id = "'.$fai_id.'" || fai_id_1 ="'.$fai_id.'" ')->toArray();
				
        foreach($histo as $name => $relations){
            foreach($relations as $relation){
				if($name == "Interfait"){
					$f_interfait = new Form_Interfait($m_interfait);
					$f_value = $f_interfait->getValues();
					
					//On supprime certains elements des formulaires pour appliquer (getTypedValues)
					unset($relation['fai_1_identification']);					
					unset($relation['fai_identification']);	
					
					unset($f_value['typeinterfait_id']);
					unset($f_value['incertitude']);
					unset($f_value['precisions']);
					
					foreach($relation as $key => $value){
						$f_interfait->getElement($key)->set('value', $value);
					}
					$relation = $f_interfait->getTypedValues($relation);
				}
				$historisation->addhistory($name, self::MODE_DELETE, $relation->getAttributes());
            }
        }

        $m_fai_us->deleteRelations($fai_id);
        $m_interfait->deleteRelations($fai_id);
        $m_interfait->deleteLastRelations($fai_id);
    }
	
	public function actionDelete() {

		$fai = new Model_Fai($this->_request->getParams());
		$form = new Form_Fai($fai);
		$formvalue = $form->getValues();
		
		$formvalue['id'] = $fai->get('id');

// jfb 2016-04-18 correctif fiche mantis 99 début
		$message = ''; // init. à blanc message de deletion
		
		try {
			$fai->delete(); // try delete avant historisation
					
			// historisation début
			$historisation = new Plugin_Historique();
			$formvalue = $form->getTypedValues($formvalue);
        	$historisation->addhistory('Fai', self::MODE_DELETE, $formvalue);
        	// historisation fin

        	$message='fai supprimé';
        } catch (Exception $e) {
       		$message = 'Suppression fai impossible';
       	}
        	 
		//$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info');	$this->getSession()->set('flash_message', 'fai as been deleted');
		$this->getSession()->set('flash_title', '');
		$this->getSession()->set('flash_status', 'info');
		$this->getSession()->set('flash_message', $message);
// jfb 2016-04-18 correctif fiche mantis 99 fin
        	     
		$this->forward('Fai', 'index');

	}

}

<?php

class Controller_Synchronisation extends Yab_Controller_Action {

	// jfb 2016-04-21 correctif fiche mantis 230 début
	//const LABEL_HISTO = "Synchronisation Us";
	const LABEL_HISTO = "Synchronisation";
	// jfb 2016-04-21 correctif fiche mantis 230 fin
	
	public function actionShow($layout = false) {
		if(!isset($_POST['synchro1_id']) || !isset($_POST['synchro2_id']))
			die("erreur à la réception des données");
		$this->getLayout()->setEnabled($layout);
		$msync = new Model_Synchronisation();
		$fsync = new Form_Synchronisation($msync);
		$sync = $msync->getSynchronisation()
			->where("synchro1_id = '".$_POST['synchro1_id']."'")
			->where("synchro2_id = '".$_POST['synchro2_id']."'")
			->toArray();
		$this->_view->set('form_sync', $fsync);
		$this->_view->set('synchronisation', $sync[0]);
	}

	public function actionIndex() {

		$synchronisation = new Model_Synchronisation();

		$synchronisations = $synchronisation->fetchAll();

		$this->_view->set('synchronisations', $synchronisations);
	}

    public function actionListSynchronisation($layout=false, $us_id=null, $v_action=false) {

        $this->getLayout()->setEnabled($layout);
        $synchronisation = new Model_Synchronisation();
		$form_synchronisation = new Form_Synchronisation($synchronisation);
		$us_synchronisations = $synchronisation->ListUsSynchronisation()->where('synchro1_id like ("'.$us_id.'") ' . ' or synchro2_id like ("'.$us_id.'") ' ) ;
		//$us_synchronisations = $synchronisation->fetchAll()->where('synchro1_id = "'.$us_id.'" ' ) ;
		
		
		$this->_view->set('form_synchronisation',$form_synchronisation);
        $this->_view->set('us_synchronisations', $us_synchronisations);
		$this->_view->set('v_action', $v_action);
		$this->_view->set('us_id', $us_id);
		
        if (!$layout) { $this->_view->setFile('View/synchronisation/list_simple.html') ; }
    }

	public function actionAdd() {

		$synchronisation = new Model_Synchronisation();

		$form = new Form_Synchronisation($synchronisation);

		if($form->isSubmitted() && $form->isValid()) {

			$synchronisation->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'synchronisation as been added');

			$this->forward('Synchronisation', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionAddAjax($us_id) {
	    $layout=false ;
	    
        $this->getLayout()->setEnabled($layout);

        $synchronisation = new Model_Synchronisation();

        $form = new Form_Synchronisation($synchronisation);
        
        $form->getElement('synchro1_id')->setValue($us_id) ;

        if($form->isSubmitted() && $form->isValid()) {

            $synchronisation->populate($form->getValues())->save();

            $this->getSession()->set('flash_title', ''); 
            $this->getSession()->set('flash_status', 'info'); 
            $this->getSession()->set('flash_message', 'synchronisation as been added');

            // $this->forward('Synchronisation', 'index');

        }

        $this->_view->set('helper_form', new Yab_Helper_Form($form));

    }

    public function actionEdit() {

		$synchronisation = new Model_Synchronisation($this->_request->getParams());

		$form = new Form_Synchronisation($synchronisation);

		if($form->isSubmitted() && $form->isValid()) {

			$synchronisation->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'synchronisation as been edited');

			$this->forward('Synchronisation', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionDelete() {

		$synchronisation = new Model_Synchronisation($this->_request->getParams());

		$synchronisation->delete();

		$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'synchronisation as been deleted');

		$this->forward('Synchronisation', 'index');

	}

}
<?php

class Controller_Superposition extends Yab_Controller_Action {

	// jfb 2016-04-21 correctif fiche mantis 230 début
	//const LABEL_HISTO = "Superposition Us";
	const LABEL_HISTO = "Superposition";
	// jfb 2016-04-21 correctif fiche mantis 230 fin
	
	public function actionShow($layout = false) {
		if(!isset($_POST['anterieur_id']) || !isset($_POST['posterieur_id']))
			die("erreur à la réception des données");
		$this->getLayout()->setEnabled($layout);
		$msup = new Model_Superposition();
		$fsup = new Form_Superposition($msup);
		$sup = $msup->getAnteroposterite()
			->where('anterieur_id = "'.$_POST['anterieur_id'].'"')
			->where('posterieur_id = "'.$_POST['posterieur_id'].'" ')
			->toArray();
		$this->_view->set('form_sup', $fsup);
		$this->_view->set('superposition', $sup[0]);
	}

    public function actionListAnterieur($layout=false, $us_id, $v_action=false) {
        
        $this->getLayout()->setEnabled($layout);

        $superposition = new Model_Superposition();
		$form_superposition = new Form_Superposition($superposition);

        $us_anterieurs = $superposition->getAnteroposterite()->where('posterieur_id = "'.$us_id.'" ');

		$this->_view->set('form_superposition',$form_superposition);
        $this->_view->set('us_anterieurs', $us_anterieurs);
		$this->_view->set('v_action', $v_action);

		if (!$layout) { $this->_view->setFile('View/superposition/list_anterieur_simple.html') ; }
	}

    public function actionListPosterieur($layout=false, $us_id, $v_action=false) {
        
        $this->getLayout()->setEnabled($layout);

        $superposition = new Model_Superposition();
		$form_superposition = new Form_Superposition($superposition);

        $us_posterieurs = $superposition->getAnteroposterite()->where('anterieur_id = "'.$us_id.'"');

		$this->_view->set('form_superposition',$form_superposition);
        $this->_view->set('us_posterieurs', $us_posterieurs);
		$this->_view->set('v_action', $v_action);
        
        if (!$layout) { $this->_view->setFile('View/superposition/list_posterieur_simple.html') ; }
    }

	public function actionAdd() {

		$superposition = new Model_Superposition();

		$form = new Form_Superposition($superposition);

		if($form->isSubmitted() && $form->isValid()) {

			$superposition->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'superposition as been added');

			$this->forward('Superposition', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

    public function actionAddAjax() {

        $superposition = new Model_Superposition();

        $form = new Form_Superposition($superposition);

        if($form->isSubmitted() && $form->isValid()) {

            $superposition->populate($form->getValues())->save();

            $this->getSession()->set('flash_title', ''); 
            $this->getSession()->set('flash_status', 'info'); 
            $this->getSession()->set('flash_message', 'superposition as been added');            

        }

        $this->_view->set('helper_form', new Yab_Helper_Form($form));

    }
	
	public function actionEdit() {

		$superposition = new Model_Superposition($this->_request->getParams());

		$form = new Form_Superposition($superposition);

		if($form->isSubmitted() && $form->isValid()) {

			$superposition->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'superposition as been edited');

			$this->forward('Superposition', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form) );

	}

	public function actionDelete() {

		$superposition = new Model_Superposition($this->_request->getParams());

		$superposition->delete();

		$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'superposition as been deleted');

		$this->forward('Superposition', 'index');

	}

/*
	public function actionShow() {
		if(!isset($_POST['ant']) || !isset($_POST['post']))
			return null;
		echo("coucou");
		die;
		$this->getLayout()->setEnabled($layout);
		$msup = new Model_Superposition();
		$fsup = new Form_Superposition($msup);
		$sup = $msup->getAnteroposterite()
			->where('anterieur_id = "'.$_POST['ant'].'"')
			->where('posterieur_id = "'.$_POST['post'].'" ');
		$this->_view->set('helper_form', new Yab_Helper_Form($fsup));

	}
*/
}
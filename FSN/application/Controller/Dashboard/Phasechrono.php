<?php

class Controller_Dashboard_Phasechrono extends Yab_Controller_Action {

	public function actionIndex() {

    $session = $this->getSession(); 
    $sitefouille_id = $session->has('sitefouille_id') ? $session->get('sitefouille_id') : null ;

    if (!empty($sitefouille_id) ) {      
    
      // re-initialisation des redirections
      if ( $session->has('forward_controller') ) { $this->getSession()->rem('forward_controller'); }
      if ( $session->has('forward_action') ) { $this->getSession()->rem('forward_action'); }

      $this->forward('Dashboard_Phasechrono', 'List', array('sitefouille_id' => $sitefouille_id) );
    } else {
      $this->getSession()->set('flash_title', 'Suivi de chantiers'); 
      $this->getSession()->set('flash_status', 'warning'); 
      $this->getSession()->set('flash_message', 'Veuillez choisir un site de fouille');
      
      $this->getSession()->set('forward_controller', 'Dashboard_Phasechrono');
      $this->getSession()->set('forward_action', 'List');
      
      $this->forward('Dashboard_Sitefouille', 'List');    
    }
    return ; 
    
	}
	
  public function actionList() {

    $session = $this->getSession(); 
    $sitefouille_id = $session->has('sitefouille_id') ? $session->get('sitefouille_id') : null ;
    
		$phasechrono = new Model_Phasechrono();

		$phasechronos = $phasechrono->fetchAll()->where('sitefouille_id = "'.$sitefouille_id.'" ' ) ;

		$this->_view->set('phasechronos', $phasechronos);
    
	}
  
	public function actionAdd() {

		$phasechrono = new Model_Phasechrono();

		$form = new Form_Phasechrono($phasechrono);

		if($form->isSubmitted() && $form->isValid()) {

			$phasechrono->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'phasechrono as been added');

			$this->forward('Phasechrono', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionEdit() {

		$phasechrono = new Model_Phasechrono($this->_request->getParams());

		$form = new Form_Phasechrono($phasechrono);

		if($form->isSubmitted() && $form->isValid()) {

			$phasechrono->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'phasechrono as been edited');

			$this->forward('Phasechrono', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionDelete() {

		$phasechrono = new Model_Phasechrono($this->_request->getParams());

		$phasechrono->delete();

		$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'phasechrono as been deleted');

		$this->forward('Phasechrono', 'index');

	}

}
<?php

class Controller_Profil_Ihm extends Yab_Controller_Action {

	public function actionIndex() {

		$profil_ihm = new Model_Profil_Ihm();

		$profil_ihms = $profil_ihm->fetchAll();

		$this->_view->set('profil_ihms', $profil_ihms);
	}

	public function actionAdd() {

		$profil_ihm = new Model_Profil_Ihm();

		$form = new Form_Profil_Ihm($profil_ihm);

		if($form->isSubmitted() && $form->isValid()) {

			$profil_ihm->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'profil_ihm as been added');

			$this->forward('Profil_Ihm', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionEdit() {

		$profil_ihm = new Model_Profil_Ihm($this->_request->getParams());

		$form = new Form_Profil_Ihm($profil_ihm);

		if($form->isSubmitted() && $form->isValid()) {

			$profil_ihm->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'profil_ihm as been edited');

			$this->forward('Profil_Ihm', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionDelete() {

		$profil_ihm = new Model_Profil_Ihm($this->_request->getParams());

		$profil_ihm->delete();

		$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'profil_ihm as been deleted');

		$this->forward('Profil_Ihm', 'index');

	}

}
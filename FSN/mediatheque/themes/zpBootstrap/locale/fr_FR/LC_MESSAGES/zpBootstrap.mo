��          T      �       �   L   �         b   '  #   �     �  {   �  C  6  M   z  )   �     �  "   r     �  �   �                                        Check to show a tag cloud in Archive list, with all the tags of the gallery. Check to show some social links. Display a home page, with a slider of 5 random picts, the gallery description and the latest news. Display a link to the Archive list. Latest news Show the EXIF Data on Image page. Remember you have to check EXIFs data you want to show on options>image>information EXIF. Project-Id-Version: zpBootstrap
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2012-04-16 22:34+0100
PO-Revision-Date: 2015-02-16 15:58+0100
Last-Translator: Vincent Bourganel <vincent_bourganel@yahoo.fr>
Language-Team: vincent3569 <vincent_bourganel@yahoo.fr>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-KeywordsList: gettext_th
X-Poedit-Basepath: .
X-Poedit-SourceCharset: utf-8
Language: fr_FR
X-Generator: Poedit 1.7.4
X-Poedit-SearchPath-0: C:\Users\Famille\Documents\_Vincent\Site Internet\Galerie photo\themes\zpBootstrap
 Affiche un nuage de tags dans les Archives, avec tous les tags de la galerie. Affiche des liens vers les sites sociaux. Affiche une page d'accueil, avec un diaporama de 5 images aléatoires, la description de la galerie et la dernière actualité. Affiche un lien vers les Archives. Dernière actualité Affiche les données EXIF sur la page Image. Rappelez-vous que vous devez choisir les données EXIF que vous voulez afficher sur la page options>image>Métadonnées. 
��          T      �       �   L   �         b   '  #   �     �  y   �  C  4  T   x  -   �  z   �     v     �  �   �                                        Check to show a tag cloud in Archive list, with all the tags of the gallery. Check to show some social links. Display a home page, with a slider of 5 random picts, the gallery description and the latest news. Display a link to the Archive list. Latest news Show the EXIF Data on Image page. Remember you have to check EXIFs data you want to show on admin>image>information EXIF. Project-Id-Version: zpBootstrap
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2012-04-16 22:34+0100
PO-Revision-Date: 2015-02-16 15:46+0100
Last-Translator: Vincent Bourganel <vincent_bourganel@yahoo.fr>
Language-Team: vincent3569 <vincent_bourganel@yahoo.fr>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-KeywordsList: gettext_th
X-Poedit-Basepath: .
X-Poedit-SourceCharset: utf-8
Language: de_DE
X-Generator: Poedit 1.7.4
X-Poedit-SearchPath-0: C:\Users\Famille\Documents\_Vincent\Site Internet\Galerie photo\themes\zpBootstrap
 Zeigt eine Schlagwortwolke mit allen Schlagwörtern der Galerie auf der Archiv-Seite Auswählen, um Social-Media-Links anzuzeigen. Zeigt eine Homepage mit einer Diashow mit fünf zufälligen Bildern, die Galerie-Beschreibung und die neusten Nachrichten. Zeigt einen Link zum Archiv. Neustes Zeigt die Exif-Daten auf der Bildseite an. Denken Sie daran, dass Sie die EXIF-Daten, die Sie anzeigen möchten, unter Einstellungen > Bild > EXIF auswählen können. 